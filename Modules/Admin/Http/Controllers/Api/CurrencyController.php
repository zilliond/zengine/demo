<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CurrencyController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return app('zengine')->model('Currency')->get();
    }

    public function rates()
    {
        return app('zengine')->model('CurrencyRate')->get();
    }

    /**
     * Store a newly created Currency in storage.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function store(Request $request)
    {
        $currency = app('zengine')->model('Currency');
        $currency->fill($request->all());
        $currency->save();
        \Debugbar::info($request->get('rates'));

        return [
            'status'    => 'success',
            'message'   => 'Currency saved successfully.',
            'plan'      => $currency
        ];
    }

    public function show($id)
    {
        return app('zengine')->model('Currency')->find($id);
    }

    /**
     * Update the specified Currency in storage.
     *
     * @param  $id
     * @param  Request  $request
     *
     * @return array
     */
    public function update($id, Request $request)
    {
        $currency = app('zengine')->model('Currency')->findOrFail($id);
        $currency->fill($request->all());
        $currency->save();
        $request_rates = $request->get('rates');
        foreach ($request_rates as $target_id => $data) {
            $currency->rates()->updateOrCreate([
                'target_currency_id' => $target_id
            ], $data);
        }

        return [
            'status'    => 'success',
            'message'   => 'Currency updated successfully.',
            'plan'      => $currency
        ];
    }

    /**
     * Remove the specified Currency from storage.
     *
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        $currency = app('zengine')->model('Currency')->findOrFail($id);
        $currency->delete();

        return [
            'status'    => 'success',
            'message'   => 'Currency deleted successfully.',
            'plan'      => $currency
        ];
    }

    public function update_rate($id, \Modules\Core\Services\Contracts\CurrencyUpdaterServiceInterface $currencyService) : array
    {
        $currency = app('zengine')->model('Currency')->findOrFail($id);
        $currency = $currencyService->updateRate($currency);
        return [
            'status'    => 'success',
            'currency'  => $currency
        ];
    }

    public function update_rates(\Modules\Core\Services\Contracts\CurrencyUpdaterServiceInterface $currencyService)
    {
        $currencyService->updateAllRates();
        return [
            'status'        => 'success',
            'currencies'    => app('zengine')->model('Currency')->all()
        ];
    }
}
