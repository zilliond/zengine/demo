<?php
use Modules\Core\Models\Operation;

return [
    'types' => [
        app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => 'Refill',
        app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => 'Withdraw',
        app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => 'Transfer',
        app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => 'Deposit open',
        app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => 'Deposit accrual',
        app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => 'Deposit close',
        app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => 'Referral pay',
    ],
    'statuses' => [
        app('zengine')->modelClass('Operation')::STATUS_CREATED       => 'Created',
        app('zengine')->modelClass('Operation')::STATUS_CANCELED      => 'Canceled',
        app('zengine')->modelClass('Operation')::STATUS_REJECTED      => 'Rejected',
        app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS   => 'In progress',
        app('zengine')->modelClass('Operation')::STATUS_SUCCESS       => 'Success',
    ],
    'status_messages' => [
        'withdraw' => [
            app('zengine')->modelClass('Operation')::STATUS_CREATED       => 'Withdrawal request successfully created',
            app('zengine')->modelClass('Operation')::STATUS_CANCELED      => 'Withdrawal request canceled',
            app('zengine')->modelClass('Operation')::STATUS_REJECTED      => 'The withdrawal request was rejected by the administration',
            app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS   => 'Withdrawal request during processing',
            app('zengine')->modelClass('Operation')::STATUS_SUCCESS       => 'Withdrawal request completed successfully',
        ]
    ]
];
