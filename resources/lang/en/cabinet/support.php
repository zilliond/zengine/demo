<?php

return [
    'tickets' => [
        'title'         => 'Tickets',
        'create_new'    => 'Create new',
        'subject'       => 'Subject',
        'status'        => 'Status',
        'actions'       => 'Actions',
        'new_messages'  => 'New messages :count',
    ],
    'create' => [
        'title'         => 'Create ticket',
        'subject'       => 'Subject',
        'message'       => 'Message',
        'submit'        => 'Send',
    ],
    'ticket' => [
        'message'       => 'Message',
        'submit'        => 'Send',
    ]
];
