<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use DavidBadura\FakerMarkdownGenerator\FakerProvider as MarkdownProvider;
use Faker\Generator as Faker;

$factory->define(app('zengine')->modelClass('Page'), static function (Faker $faker) {
    $faker->addProvider(new \Xvladqt\Faker\LoremFlickrProvider($faker));
    $faker->addProvider(new MarkdownProvider($faker));

    $title = $faker->sentence;
    return [
        'title'       => $title,
        'route'       => \Illuminate\Support\Str::slug($title),
        'description' => \Illuminate\Support\Str::limit($faker->paragraph, 250),
        'keywords'    => implode(',', $faker->words),
        'body'        => $faker->markdown() . "\n\n". MarkdownProvider::markdownInlineImg(),
        'created_at'  => $faker->date('Y-m-d H:i:s'),
        'updated_at'  => $faker->date('Y-m-d H:i:s')
    ];
});
