<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\Core\Exceptions\UserBalancesNotDividedException;
use Modules\Core\Models\Traits\Convert;

/**
 * \Modules\Core\Models\User.
 *
 * @property int                                                                                                       $id
 * @property string                                                                                                    $login
 * @property string                                                                                                    $name
 * @property string                                                                                                    $email
 * @property string                                                                                                    $password
 * @property string|null                                                                                               $pin_code
 * @property string|null                                                                                               $google2fa_secret
 * @property float                                                                                                     $balance
 * @property int                                                                                                       $currency_id
 * @property int                                                                                                       $role
 * @property int|null                                                                                                  $inviter_id
 * @property int                                                                                                       $ref_level
 * @property string|null                                                                                               $referral_percent
 * @property float                                                                                                     $referrals_earned
 * @property string|null                                                                                               $comment
 * @property string|null                                                                                               $last_login_ip
 * @property string|null                                                                                               $country_code
 * @property \Illuminate\Support\Carbon|null                                                                           $email_verified_at
 * @property \Illuminate\Support\Carbon|null                                                                           $last_login_at
 * @property string|null                                                                                               $remember_token
 * @property \Illuminate\Support\Carbon|null                                                                           $created_at
 * @property \Illuminate\Support\Carbon|null                                                                           $updated_at
 * @property \Illuminate\Support\Carbon|null                                                                           $deleted_at
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\AuthLog[]                                            $auth_logs
 * @property int|null                                                                                                  $auth_logs_count
 * @property \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[]                                       $clients
 * @property int|null                                                                                                  $clients_count
 * @property \Modules\Core\Models\Currency                                                                                      $currency
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\Deposit[]                                            $deposits
 * @property int|null                                                                                                  $deposits_count
 * @property mixed                                                                                                     $invite_link
 * @property bool                                                                                                      $is_admin
 * @property mixed                                                                                                     $is_online
 * @property string|null                                                                                               $last_login_country
 * @property string|null                                                                                               $last_login_country_code
 * @property mixed                                                                                                     $last_login_flag
 * @property \Torann\GeoIP\Location                                                                                    $last_login_location
 * @property \Modules\Core\Models\User|null                                                                                     $inviter
 * @property \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property int|null                                                                                                  $notifications_count
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\Operation[]                                          $operations
 * @property int|null                                                                                                  $operations_count
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\User[]                                               $referrals
 * @property int|null                                                                                                  $referrals_count
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\Ticket[]                                             $tickets
 * @property int|null                                                                                                  $tickets_count
 * @property \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[]                                        $tokens
 * @property int|null                                                                                                  $tokens_count
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\Wallet[]                                             $wallets
 * @property int|null                                                                                                  $wallets_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereGoogle2faSecret($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereInviterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereLastLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereLastLoginIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User wherePinCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereRefLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereReferralPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereReferralsEarned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\User withoutTrashed()
 * @mixin \Eloquent
 * @property string $locale
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereLocale($value)
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\UserBalance[]   $balances
 * @property int|null                                                             $balances_count
 * @property \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\SocialAccount[] $social_accounts
 * @property int|null                                                             $social_accounts_count
 * @property \Illuminate\Support\Carbon|null                                      $last_online_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\User whereLastOnlineAt($value)
 * @property string $invested
 * @property string $structure_invested
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInvested($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStructureInvested($value)
 */
class User extends Authenticatable
{
    use Notifiable;
    use Convert;
    use HasApiTokens;
    use SoftDeletes;

    public const ROLE_USER = 0;
    //public const ROLE_MODERATOR = 50;
    public const ROLE_ADMIN = 99;

    protected $fillable = [
        'login', 'name', 'email', 'password', 'balance', 'currency_id', 'role', 'inviter_id', 'ref_level',
        'last_login_at', 'last_login_ip', 'last_online_at', 'country_code', 'comment', 'referral_percent', 'referrals_earned', 'invested', 'structure_invested', 'locale', 'created_at',
    ];

    protected $dates = [
        'created_at', 'updated_at', 'last_login_at', 'last_online_at',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'is_admin', 'is_online',
    ];

    public static $rules = [
    ];

    public function getInviteLinkAttribute()
    {
        return route('home', ['ref' => $this->id]);
    }

    public function getIsAdminAttribute(): bool
    {
        return self::ROLE_ADMIN === $this->role;
    }

    public function inviter()
    {
        return $this->belongsTo(self::class, 'inviter_id', 'id');
    }

    public function referrals()
    {
        return $this->hasMany(self::class, 'inviter_id', 'id');
    }

    public function getReferralsCountAttribute()
    {
        return (int) app('zengine')->getStat('USER_REFERRALS_COUNT_'.$this->id, 0);
    }

    /**
     * @return BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(app('zengine')->modelClass('Currency'))->withDefault([
            'id' => 1, 'name' => 'USD', 'code' => 'USD', 'code_iso' => 'USD', 'symbol' => '$', 'rate' => 1,
        ]);
    }

    public function operations()
    {
        return $this->hasMany(app('zengine')->modelClass('Operation'));
    }

    public function wallets()
    {
        return $this->hasMany(app('zengine')->modelClass('Wallet'), 'user_id', 'id');
    }

    public function tickets()
    {
        return $this->hasMany(app('zengine')->modelClass('Ticket'));
    }

    public function deposits()
    {
        return $this->hasMany(app('zengine')->modelClass('Deposit'));
    }

    public function auth_logs()
    {
        return $this->hasMany(app('zengine')->modelClass('AuthLog'));
    }

    public function social_accounts()
    {
        return $this->hasMany(app('zengine')->modelClass('SocialAccount'));
    }

    /**
     * @throws UserBalancesNotDividedException
     */
    public function balances(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        if ('divided' === setting('balances_type')) {
            return $this->hasMany(app('zengine')->modelClass('UserBalance'));
        }
        throw new UserBalancesNotDividedException();
    }

    public function getIsOnlineAttribute()
    {
        return \Cache::has('user-is-online-'.$this->id);
    }

    /**
     * @return string|null
     */
    public function getLastLoginCountryAttribute()
    {
        $location = $this->last_login_location;

        return isset($location['country']) ? $location['country'] : null;
    }

    /**
     * @return string|null
     */
    public function getLastLoginCountryCodeAttribute()
    {
        $location = $this->last_login_location;

        return isset($location['iso_code']) ? $location['iso_code'] : 'EU';
    }

    public function getLastLoginFlagAttribute()
    {
        $chars = collect(str_split($this->last_login_country_code));
        $chars = $chars->map(function ($char) {
            $code = '&#'.(127397 + ord($char)).';';

            return mb_convert_encoding($code, 'UTF-8', 'HTML-ENTITIES');
        });

        return $chars->implode('');
    }

    /**
     * @return \Torann\GeoIP\Location
     */
    public function getLastLoginLocationAttribute()
    {
        $location = \GeoIP::getLocation($this->last_login_ip);

        return $location;
    }

    /**
     * @param  \Modules\Core\Models\Currency  $to
     * @return float|int
     */
    public function getBalanceInCurrency(Currency $to)
    {
        return $this->convert($this->balance, $this->currency, $to);
    }

    public function unlocked()
    {
        $userService = app('users');

        return !$userService->locked($this);
    }
}
