<?php


namespace Modules\Core\Exceptions;

class BalanceTypeNotFindException extends \Exception
{
    public function __construct($message = "")
    {
        parent::__construct("Balance type not find $message", 500);
    }
}
