<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Services\OperationService;

class OperationController extends Controller
{
    /**
     * Display a listing of the Currency.
     *
     * @param  Request  $request
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(Request $request)
    {
        return app('zengine')->model('Operation')->with([
            'currency',
            'user',
            'payment_system',
            'wallet.payment_system',
        ])->latest()->orderByDesc('id')->when(request('login', null), function (Builder $when) {
            $user = app('zengine')->model('User')->where('login', request('login'))->first();
            if($user) {
                return $when->where('user_id', $user->id);
            }
            return $when;
        })->when(request('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL), function (Builder $when) {
            return $when->where('type', request('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL));
        })->when(request('status'), function (Builder $when) {
            return $when->where('status', request('status'));
        })->currencyAmountFilter(request('min_amount', 0), request('max_amount', 0))->paginate(10);
    }

    public function reject($id)
    {
        $operation = app('zengine')->model('Operation')->findOrFail($id);
        /** @var OperationService $operationService */
        $operationService = app('operations');
        if($operation->status !== app('zengine')->modelClass('Operation')::STATUS_REJECTED) {
            $operationService->typeByOperation($operation)->reject($operation);
            return [
                'status'  => 'success',
                'message' => 'Operation rejected successfully.',
            ];
        }
        return [
            'status'  => 'error',
            'message' => 'Operation rejected error.',
        ];
    }

    public function stats()
    {
        $amounts = app('zengine')->model('Operation')->when(request('login', null), function (Builder $when) {
            $user = app('zengine')->model('User')->where('login', request('login'))->first();
            if($user) {
                return $when->where('user_id', $user->id);
            }
            return $when;
        })->when(request('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL), function (Builder $when) {
            return $when->where('type', request('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL));
        })->when(request('status'), function (Builder $when) {
            return $when->where('status', request('status'));
        })->currencyAmountFilter(request('min_amount', 0), request('max_amount', 0))
            ->groupBy('currency_id')->selectRaw(\DB::raw('sum(amount) as sum, currency_id'))->pluck('sum',
            'currency_id');
        return [
            'amounts' => $amounts,
        ];
    }

    public function success($id)
    {
        $operation = app('zengine')->model('Operation')->findOrFail($id);
        $operationService = app('operations');
        if($operation->status !== app('zengine')->modelClass('Operation')::STATUS_SUCCESS) {
            $operationService->typeByOperation($operation)->success($operation);
            return [
                'status'  => 'success',
                'message' => 'Operation finished successfully.',
            ];
        }
        return [
            'status'  => 'error',
            'message' => 'Operation finished error.',
        ];
    }
}
