<?php


namespace Modules\Core\Models\Traits;

use Modules\Core\Models\Currency;

/**
 * Trait ToCurrencyTrait
 * @mixin \Eloquent
 * @package Modules\Core\Models\Traits
 */
trait Convert
{
    public static $currencyService;
    /**
     * @param  Currency  $to
     * @param  string  $field
     * @param  string  $from_currency_field
     * @return float|int
     *//*
    protected function fieldToCurrency(Currency $to, string $field = 'amount', string $from_currency_field = 'currency')
    {
        if(!$this->currencyService){
            $this->currencyService = app('currencies');
        }
        $from_currency = $this->getAttribute($from_currency_field);
        if(!($from_currency instanceof Currency)){
            throw new \InvalidArgumentException('$from_currency_field is not Currency');
        }
        return $this->currencyService->convert($this->getAttribute($field), $from_currency, $to);
    }*/

    /**
     * @param  float  $amount
     * @param  Currency  $from
     * @param  Currency  $to
     * @return float|int
     */
    public function convert(float $amount, Currency $from, Currency $to)
    {
        if (!self::$currencyService) {
            self::$currencyService = app('currencies');
        }
        return self::$currencyService->convert($amount, $from, $to);
    }
}
