<?php

namespace Modules\Core\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function show(Request $request, $route)
    {
        $page = app('zengine')->model('Page')->where('route', $route)->firstOrFail();
        return view('page', compact('page'));
    }
}
