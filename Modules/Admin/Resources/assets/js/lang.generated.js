export default {
    "en": {
        "ui": {
            "header": {
                "server_time": "Server time:",
                "short_stats": {
                    "button": "Краткая статистика",
                    "title": "Краткая статистика проекта:",
                    "daily_payouts": "Ежедневно выплачивать:",
                    "users_balance": "Денег на счетах пользователей:",
                    "project_left": "Жить проекту:",
                    "deposits_sum": "Сумма депозитов:",
                    "users_online": "Посетителей онлайн:",
                    "withdraws_waiting": "Ожидает выплат:",
                    "users_today": "Посетителей за сутки:",
                    "users_weekly": "Посетителей за неделю:"
                },
                "calendar": {
                    "title": "График",
                    "online": "Онлайн",
                    "offline": "Оффлайн"
                },
                "menu": {
                    "welcome": "Добро пожаловать:",
                    "cabinet": "Кабинет",
                    "logout": "Выйти"
                }
            },
            "menu": {
                "header": "Menu",
                "general_group": "General",
                "dashboard": "Главная админки",
                "refills": "Пополнение счета",
                "withdraws": "Вывод средств",
                "plans": "Инвестиционные планы",
                "deposits": "Депозиты",
                "currencies": "Валюты",
                "payment_systems": "Платежные системы",
                "users_group": "Пользователи",
                "users": "Управление пользователями",
                "users_newsletter": "Рассылка пользователям",
                "referrals_group": "Реффералы",
                "referrals_rating": "Рейтинг рефоводов",
                "referrals_levels": "Реферальные уровни",
                "project_group": "Проект",
                "page_create": "Создать страницу",
                "pages": "Созданные страницы",
                "cheat_statistics": "Накрутка статистики",
                "settings": "Настройки проекта",
                "change_password": "Сменить пароль",
                "news_group": "Новости",
                "posts": "Список новостей",
                "post_add": "Добавить новость",
                "security_group": "Безопасность",
                "server_info": "Информация о сервере",
                "blacklist_ip": "Черный список IP",
                "monitoring_ip": "Мониторинг IP"
            },
            "sidebar": {
                "currency": "Валюта:",
                "add_currency": "Добавить валюту",
                "notice": "Вы можете выбрать валюту, которую хотите чтобы отображась в скобках к любой транзакции",
                "copyright_reserved": "Все права защищены.",
                "copyright_company": "Larahyip Ltd."
            },
            "date_intervals": {
                "daily": "По дням",
                "two_days": "По двум дням",
                "weekly": "По неделям",
                "monthly": "По месяцам"
            },
            "form": {
                "file": "Выбрать файл"
            },
            "days": "дней",
            "people": "чел.",
            "pieces": "шт.",
            "on": "Вкл",
            "off": "Выкл",
            "min": "Мин",
            "max": "Макс",
            "online": "Онлайн",
            "offline": "Оффлайн",
            "search": "Поиск",
            "back": "Назад",
            "currency": "Валюта"
        }
    },
    "ru": {
        "promos": {
            "main": {
                "monitoring": {
                    "title": "Мониторинги",
                    "subtitle": "Расчет по всем мониторингам",
                    "text": "Ведите учет по всем мониторингам, которые вы приобрели"
                },
                "blog": {
                    "title": "Блоги",
                    "subtitle": "Расчет по всем блогам",
                    "text": "Ведите учет по всем блогам, которые вы приобрели"
                },
                "banner": {
                    "title": "Баннерная реклама",
                    "subtitle": "Расчет по каждому баннеру",
                    "text": "Ведите учет по всей баннерной рекламе, которая заказана Ваши"
                },
                "youtube_blog": {
                    "title": "Youtube блогеры",
                    "subtitle": "Расчет по всем блогам",
                    "text": "Ведите учет по всем блогам, которые вы приобрели"
                },
                "title": "Полный учет вашего проекта",
                "subtitle": "В данную колонку будет попадать вся статистика по по добавленным вами категориям",
                "total": "Общий рекламный бюджет:",
                "sort": "Быстрая сортировка:",
                "filters": {
                    "all": "Все",
                    "monitoring": "Мониторинги",
                    "blog": "Блоги",
                    "banner": "Баннерная реклама",
                    "youtube": "Youtube блогеры"
                }
            },
            "pages": {
                "total_budget": "Общий бюджет",
                "payback": "Окупаемость:",
                "search": "Поиск по названию...",
                "table": {
                    "invested": "Вложения:",
                    "income": "Принес:",
                    "profit": "Профит:",
                    "structure": "Структура:",
                    "posted_at": "Дата заказа:",
                    "duration": "Партнерство:",
                    "withdraw": "Всего выведено:",
                    "cost": "Цена:",
                    "profit_help_white": "Профит - это",
                    "profit_help": "цифра, которая может быть как плюсовая так и минусовая, она зависит от цены листинга,\n                    суммы инвестиций в структуре и суммы выводов реферальных и процентов, то есть эта цифра показывает окупил себя блогера или нет."
                },
                "monitoring": {
                    "title": "Мониторинги",
                    "subtitle": "Расчет по всем мониторингам",
                    "three": "Ведите учет по всем блогерам, которых вы заказали",
                    "table_title": "Все мониторинги",
                    "table_subtitle": "Полный список мониторингов которые были заказаны",
                    "total_count": "Всего мониторингов:",
                    "budget": {
                        "first": "Общая сумма вложенная в мониторинги:",
                        "second": "Сумма инвестиций от мониторингов:"
                    },
                    "modal": {
                        "title": "Добавление мониторинга",
                        "name": "Имя блогера:",
                        "cost": "Цена листинга:",
                        "deposit": "Депозит:",
                        "deposit_included": "Депозит включен в листинг:",
                        "posted_at": "Дата размещения:",
                        "user_id": "ID пользователя:",
                        "user": "Пользователь:",
                        "description": "Описание (Заметки, особенности и тд):",
                        "success": "Мониторинг добавлен",
                        "edit_title": "Редактирование мониторинга",
                        "edit_success": "Мониторинг изменен"
                    }
                },
                "blog": {
                    "title": "Блоги",
                    "subtitle": "Расчет по всем блогам",
                    "three": "Ведите учет по всем блогам, которых вы заказали",
                    "table_title": "Все блогеры",
                    "table_subtitle": "Полный список блогов которые были заказаны",
                    "total_count": "Всего блогов:",
                    "budget": {
                        "first": "Общая сумма вложений в блоги:",
                        "second": "Сумма инвестиций от блогов:"
                    },
                    "modal": {
                        "title": "Добавление блогера",
                        "name": "Имя блогера:",
                        "cost": "Цена листинга:",
                        "deposit": "Депозит:",
                        "deposit_included": "Депозит включен в листинг:",
                        "posted_at": "Дата размещения:",
                        "user_id": "ID пользователя:",
                        "user": "Пользователь:",
                        "description": "Описание ( Заметки, особенности и тд):",
                        "success": "Блог добавлен",
                        "edit_title": "Редактирование блогера",
                        "edit_success": "Блогер изменен"
                    }
                },
                "banner": {
                    "title": "Баннерная реклама",
                    "subtitle": "Расчет по всем баннерам",
                    "three": "Ведите учет по всем баннерам, которых вы заказали",
                    "table_title": "Все баннеры",
                    "table_subtitle": "Полный список баннеров которые были заказаны",
                    "total_count": "Всего баннеров:",
                    "budget": {
                        "first": "Общая сумма вложенная в баннеры:",
                        "second": "Сумма инвестиций от баннеров:"
                    },
                    "modal": {
                        "title": "Добавление рекламного баннера",
                        "name": "Название баннера:",
                        "cost": "Цена места:",
                        "link": "Ссылка на баннер:",
                        "posted_at": "Дата заказа:",
                        "ended_at": "Дата окончания:",
                        "user_nick": "Ник аккаунта реф. ссылки:",
                        "user_id": "ID пользователя:",
                        "user": "Пользователь:",
                        "description": "Описание ( Заметки, особенности и тд):",
                        "success": "Баннер добавлен",
                        "edit_title": "Редактирование баннера",
                        "edit_success": "Баннер изменен"
                    }
                },
                "video": {
                    "title": "Youtube блогеры",
                    "subtitle": "Расчет по всем блогерам",
                    "three": "Ведите учет по всем блогерам, которых вы заказали",
                    "table_title": "Все блогеры",
                    "table_subtitle": "Полный список блогеров которые были заказаны",
                    "total_count": "Всего блогеров:",
                    "budget": {
                        "first": "Общая сумма вложенная в блоги:",
                        "second": "Сумма инвестиций от блогеров:"
                    },
                    "modal": {
                        "title": "Добавление блогера",
                        "name": "Имя блогера:",
                        "cost": "Цена листинга:",
                        "deposit": "Депозит:",
                        "deposit_included": "Депозит включен в листинг:",
                        "posted_at": "Дата размещения:",
                        "user_id": "ID пользователя:",
                        "user": "Пользователь:",
                        "description": "Описание ( Заметки, особенности и тд):",
                        "success": "Youtube блог добавлен",
                        "edit_title": "Редактирование Youtube блога",
                        "edit_success": "Youtube блог изменен"
                    }
                }
            }
        },
        "dashboard": {
            "top_plans_first": "Тарифы",
            "top_plans_second": " -  топ 4 тарифа",
            "top_payments_first": "Платежки",
            "top_payments_second": " -  топ 4 платежки",
            "top_balances_first": "Балансы",
            "top_balances_second": " -  топ 4 платежки",
            "operations": {
                "refills": "Пополнения",
                "users": "Пользователи",
                "withdraws": "Выплачено"
            },
            "daily": {
                "title": "Пополнений и выводов за день",
                "refill": "Пополнено:",
                "withdrawn": "Выведено:",
                "profit_daily": "Чистая прибыль за день: ",
                "profit_percent": "Процент прибыли: ",
                "daily": "По дням",
                "last_12_days": "За последние  12 дней"
            },
            "events": {
                "title": "Последние события проекта",
                "sub_title": "в данном поле вы сможете отследить все что происходит в проекте вы сможете",
                "operations": {
                    "refills": "Пополнения",
                    "refill": "Пополнение",
                    "withdraws": "Выводы",
                    "withdraw": "Вывод",
                    "deposits": "Депозиты",
                    "deposit": "Депозит",
                    "registrations": "Регистрации",
                    "registration": "Регистрация",
                    "ref_pays": "Реферальнные",
                    "ref_pay": "Реферальнные"
                }
            },
            "investors": {
                "title": "Инвесторов в проекте:",
                "active": "Активные",
                "total": "Всего партнеров"
            },
            "waiting_refills": "Пополнений в ожидании:",
            "statistics": "Статистика - топ 3 партнеров:",
            "referrals": {
                "title": "Рефферальные отличиления:",
                "active": "Активные",
                "total": "Всего партнеров"
            }
        },
        "payment_systems": {
            "title": "Список платежных систем",
            "add_button_first": "Добавить",
            "add_button_second": "платежную систему",
            "update_button_first": "Обновить",
            "update_button_second": "балансы",
            "form": {
                "create_title": "Добавление платежной системы",
                "update_title": "Редактирование платежной системы",
                "required_help": "- поля обязательные для заполнения",
                "provider": "Основной модуль:",
                "name": "Название:",
                "wallet": "Номер счета:",
                "code": "Аббревиатура:",
                "refill_commission": "Комиссия на ввод:",
                "withdraw_commission": "Комиссия на вывод:",
                "currency": "Валюта:",
                "refill_amount": "Суммы пополнения",
                "withdraw_amount": "Суммы выводов",
                "usage_settings": "Настройки пользования:",
                "usage_types": {
                    "refill": "Пополнения",
                    "withdraw": "Вывод",
                    "wallet": "Кошелек"
                },
                "withdraw_type": "Автовыплаты API:",
                "withdraw_types": {
                    "manual": "Ручные",
                    "semi_auto": "Полуавтоматические",
                    "auto": "Автоматические"
                },
                "status": "Статус:",
                "access_settings": "- Настройки доступа",
                "submit": "Сохранить"
            }
        },
        "validation": {
            "post": []
        },
        "routes": {
            "dashboard": {
                "title": "Главная админки",
                "description": "Страница статистики проекта"
            },
            "operations": {
                "title": "Пополнения / Вывод",
                "description": "Все операции пополнения / вывода"
            },
            "plans": {
                "title": "Инвестиционные планы",
                "description": "Список инвестиционных планов"
            },
            "plans_create": {
                "title": "Инвестиционные планы",
                "description": "Создание инвестиционного плана"
            },
            "plans_edit": {
                "title": "Инвестиционные планы",
                "description": "Редактирование инвестиционного плана"
            },
            "currencies": {
                "title": "Валюты",
                "description": "Список валют"
            },
            "currencies_create": {
                "title": "Валюты",
                "description": "Добавление валюты"
            },
            "currencies_edit": {
                "title": "Валюты",
                "description": "Редактирование валюты"
            },
            "payment_systems": {
                "title": "Платежные системы",
                "description": "Список платежных систем"
            },
            "payment_systems_create": {
                "title": "Платежные системы",
                "description": "Добавление платежной системы"
            },
            "payment_systems_edit": {
                "title": "Платежные системы",
                "description": "Редактирование платежной системы"
            },
            "deposits": {
                "title": "Депозиты",
                "description": "Открытые депозиты проекта"
            },
            "users": {
                "title": "Управление пользователями",
                "description": "Добавляйте и управляйте пользователями"
            },
            "users_referrals": {
                "title": "Рефераллы",
                "description": "Рефераллы пользователя"
            },
            "users_auth_log": {
                "title": "Лог авторизаций",
                "description": "Лог авторизаций пользователя"
            },
            "users_newsletter": {
                "title": "Рассылка пользователям",
                "description": "Рассылка сообщения пользователям"
            },
            "users_edit": {
                "title": "Управление пользователями",
                "description": "Редактирование пользователя"
            },
            "referrals": {
                "title": "Рейтинг рефоводов",
                "description": "Рейтинг рефоводов"
            },
            "referrals_settings": {
                "title": "Реферальные уровни",
                "description": "Реферальные уровни"
            },
            "pages_create": {
                "title": "Создать страницу",
                "description": "Создание статичной страницы"
            },
            "pages": {
                "title": "Созданные страницы",
                "description": "Список статических страниц"
            },
            "pages_edit": {
                "title": "Редактирование страницы",
                "description": "Редактирование статичной страницы"
            },
            "faq_create": {
                "title": "Добавить FAQ",
                "description": "Создание FAQ"
            },
            "faq": {
                "title": "FAQ",
                "description": "FAQ"
            },
            "faq_edit": {
                "title": "Редактирование FAQ",
                "description": "Редактирование FAQ"
            },
            "settings": {
                "title": "Настройки проекта",
                "description": "Полная настройка вашего проекта"
            },
            "settings_fake": {
                "title": "Накрутка статистики",
                "description": "Страница фейковой статистики"
            },
            "posts": {
                "title": "Новости",
                "description": "Список новостей для редактирования"
            },
            "posts_create": {
                "title": "Новости",
                "description": "Страница добавления новостей"
            },
            "posts_edit": {
                "title": "Новости",
                "description": "Редактирование новости"
            },
            "tickets": {
                "title": "Тикеты",
                "description": "Список чаты"
            },
            "tickets_show": {
                "title": "Тикеты",
                "description": "Чат"
            },
            "auth_log": {
                "title": "Мониторинг IP",
                "description": "Мониторинг IP"
            },
            "blacklist": {
                "title": "Черный список IP",
                "description": "Черный список IP"
            },
            "system_info": {
                "title": "Информация о сервере",
                "description": "Информация о сервере"
            },
            "promos": {
                "title": "Ваш учет",
                "description": "Веди свой учет по проекту"
            },
            "promos_type": {
                "title": "Ваш учет",
                "description": "Веди свой учет по проекту"
            },
            "newsletter": {
                "title": "Рассылка ЛС",
                "description": "Рассылка личных сообшений пользователям"
            },
            "analytics": {
                "title": "Аналитика",
                "description": "Аналитика всего проекта"
            },
            "404": {
                "title": "Ошибка",
                "description": "Страницы не существует"
            }
        },
        "analytics": {
            "profit": {
                "title": "Чистый заработок проекта",
                "description": "Чистый заработок высчитывается таким образом:\n                        (Общая сумма инвестиций) - (Общая сумму выведеная с проекта)",
                "today_date": "На теперешнюю  дату :"
            },
            "cash_flow": {
                "title": "Денежный поток проекта",
                "hint": "Данные являются примерными, так как каждую минуту может происходить новый вклад",
                "estimated_tomorrow": "Примерная сумма на вывод завтра:",
                "estimated_week": "Примерная сумма на вывод за неделю:",
                "estimated_month": "Примерная сумма на вывод за месяц:",
                "estimated_half_year": "Примерная сумма на вывод за 6 месяцев:"
            },
            "form": {
                "title": "Выдать статистическую информацию",
                "date_from": "Дата от:",
                "date_to": "Дата до:",
                "day": "День:",
                "month": "Месяц:",
                "year": "Год:",
                "notice": "Статистика ниже появится после введения нужных для вас дат и нажатия на кнопку “Найти”",
                "submit": "Найти"
            },
            "charts": {
                "deposits_sum": "Сумма депозитов:",
                "refills_sum": "Пополнено на сумму:",
                "withdraws_sum": "Выплачено на сумму:",
                "deposits": "Сумма депозитов",
                "refills": "Пополнено на сумму",
                "withdraws": "Выплачено на сумму"
            },
            "stats": {
                "none_enter": "Введите даты, чтобы увидеть",
                "none_enter_link": "статистику...",
                "none_help": "Заполните формы",
                "user_stats": "Пользовательская статистика",
                "users": "Пользователей:",
                "active_users": "Активных пользователей:",
                "active_percent": "Процент вкладов за данный период",
                "active_percent_help": "Данный процент показывает соотношение людей, которые прошли регистрацию и сделали вклад и тех, которые просто зарегистрировались в системе",
                "users_growth": "Прирост пользователей ",
                "users_growth_help": "Данный процент показывает на сколько больше или меньше произведено регистраций новых пользователей по отношению к такому же периоду времени ранее",
                "average_deposit": "Среднией вклад по дням",
                "average_deposit_help": "В данной статистике вы увидите какой средний вклад сделали инвесторы за 1 день",
                "new_users": "Статистика  добавления пользователей",
                "new_users_label": "Новых пользователей",
                "average_deposit_label": "Средний вклад за день"
            }
        },
        "pages": {
            "table": {
                "title": "Список страниц",
                "page": "Страница",
                "route": "Ссылка"
            },
            "form": {
                "title": "Заголовок:",
                "description": "Описание страницы:",
                "keywords": "Ключевые слова:",
                "route": "Путь:",
                "submit": "Сохранить"
            }
        },
        "posts": {
            "table": {
                "title": "Список новостей",
                "date": "Дата",
                "name": "Название",
                "description": "Описание",
                "image": "Картинка"
            },
            "form": {
                "important": "Важная новость:",
                "subject": "Тема новости:",
                "description": "Описание страницы:",
                "keywords": "Ключевые слова:",
                "preview": "Предпросмотр",
                "image_or": "или",
                "image_link": "Введите ссылку на картинку",
                "submit": "Создать новость"
            }
        },
        "operations": {
            "filter": {
                "title": "Поиск платежа:",
                "login": "Логин:",
                "amount_min": "Сумма - от:",
                "amount_max": "Сумма - до:",
                "transaction_id": "Номер транзакции:"
            },
            "total_count": "Общее кол-во транзакций:",
            "total_amount": "Общая сумма транзакций:",
            "table": {
                "title": "Статус транзакции:",
                "date": "Дата",
                "login": "Логин",
                "amount": "Сумма",
                "transaction_id": "Счет",
                "payment_system": "Система",
                "status": "Статус",
                "statuses": {
                    "created": "В процессе",
                    "success": "Выполненые",
                    "in_progress": "В ожидании",
                    "canceled": "Отмененные",
                    "rejected": "Отмененные администрацией",
                    "all": "Все"
                }
            },
            "statuses": {
                "created": "Создана",
                "canceled": "Отменен",
                "rejected": "Отклонен",
                "in_progress": "В процессе",
                "success": "Успешно"
            }
        },
        "ui": {
            "header": {
                "server_time": "Время сервера:",
                "short_stats": {
                    "button": "Краткая статистика",
                    "title": "Краткая статистика проекта:",
                    "daily_payouts": "Ежедневно выплачивать:",
                    "users_balance": "Денег на счетах пользователей:",
                    "project_left": "Жить проекту:",
                    "deposits_sum": "Сумма депозитов:",
                    "users_online": "Посетителей онлайн:",
                    "withdraws_waiting": "Ожидает выплат:",
                    "users_today": "Посетителей за сутки:",
                    "users_weekly": "Посетителей за неделю:"
                },
                "calendar": {
                    "title": "График",
                    "online": "Онлайн",
                    "offline": "Оффлайн"
                },
                "menu": {
                    "welcome": "Добро пожаловать:",
                    "cabinet": "Кабинет",
                    "logout": "Выйти",
                    "menu": "Меню",
                    "profile": "Профиль"
                }
            },
            "menu": {
                "header": "Меню",
                "general_group": "Основное меню",
                "dashboard": "Главная админки",
                "refills": "Пополнение счета",
                "withdraws": "Вывод средств",
                "plans": "Инвестиционные планы",
                "deposits": "Депозиты",
                "currencies": "Валюты",
                "payment_systems": "Платежные системы",
                "promos": "Ваш учет",
                "analytics": "Аналитика",
                "users_group": "Пользователи",
                "users": "Управление пользователями",
                "users_newsletter": "Рассылка пользователям",
                "tickets": "Тикеты",
                "referrals_group": "Реффералы",
                "referrals_rating": "Рейтинг рефоводов",
                "referrals_levels": "Реферальные уровни",
                "project_group": "Проект",
                "page_create": "Создать страницу",
                "pages": "Созданные страницы",
                "faq": "FAQ",
                "cheat_statistics": "Накрутка статистики",
                "settings": "Настройки проекта",
                "change_password": "Сменить пароль",
                "news_group": "Новости",
                "posts": "Список новостей",
                "post_add": "Добавить новость",
                "security_group": "Безопасность",
                "server_info": "Информация о сервере",
                "blacklist_ip": "Черный список IP",
                "monitoring_ip": "Мониторинг IP"
            },
            "sidebar": {
                "currency": "Валюта:",
                "add_currency": "Добавить валюту",
                "notice": "Вы можете выбрать валюту, которую хотите чтобы отображась в скобках к любой транзакции",
                "copyright_reserved": "Все права защищены.",
                "copyright_company": "Larahyip Ltd."
            },
            "date_intervals": {
                "daily": "По дням",
                "two_days": "По двум дням",
                "weekly": "По неделям",
                "monthly": "По месяцам"
            },
            "form": {
                "file": "Выбрать файл"
            },
            "days": "дней",
            "people": "чел.",
            "pieces": "шт.",
            "on": "Вкл",
            "off": "Выкл",
            "min": "Мин",
            "max": "Макс",
            "online": "Онлайн",
            "offline": "Оффлайн",
            "search": "Поиск",
            "choose": "Выбрать",
            "back": "Назад",
            "save": "Сохранить",
            "error": "Ошибка",
            "deleted": "Удалено",
            "success": "Успешно",
            "currency": "Валюта"
        },
        "info": {
            "general": "Общая информация:",
            "server_ip": "IP сервера:",
            "uptime": "Аптайм:",
            "hardware": "Hardware:",
            "cpu": "Процессор:",
            "threads": "Количество потоков:",
            "device_model": "Модель устройства или мат. платы:",
            "virtualization": "Виртуализация:",
            "software": "Software:",
            "arch": "Архитектура:",
            "os": "Операционная система:",
            "distro": "Дистрибутив:",
            "kernel": "Ядро:",
            "php_version": "Версия PHP:",
            "webserver": "Веб сервер:",
            "database_driver": "Драйвер базы данных:",
            "database_version": "Версия базы данных:"
        },
        "faq_items": {
            "title": "FAQ",
            "table": {
                "question": "Вопрос",
                "answer": "Ответ"
            },
            "form": {
                "question": "Вопрос:",
                "answer": "Ответ:",
                "hidden": "Не отображать:",
                "submit": "Сохранить"
            }
        },
        "tickets": {
            "table": {
                "title": "Тикеты",
                "user": "Пользователь",
                "subject": "Тема",
                "last_message": "Последнее сообщение"
            },
            "show": {
                "title": "Тикет",
                "edit": "Редактировать",
                "save": "Сохранить",
                "send": "Отправить"
            }
        },
        "monitoring_ip": {
            "user_log": "Показать лог авторизации пользователя:",
            "ip_log": "Показать лог авторизации по ip:",
            "user_id": "ID пользователя:",
            "ip": "IP:",
            "table": {
                "user": "Пользователь",
                "date": "Дата",
                "ip": "IP",
                "country": "Страна"
            },
            "blacklist": {
                "title": "Добавить в черный список",
                "ip": "IP:",
                "comment": "Комментарий:",
                "submit": "Создать",
                "table": {
                    "date": "Дата добавления",
                    "ip": "IP",
                    "comment": "Комментарий"
                }
            }
        },
        "currencies": {
            "title": "Список валют",
            "add_button_first": "Добавить",
            "add_button_second": "валюту",
            "update_button_first": "Обновить",
            "update_button_second": "курсы валют",
            "form": {
                "create_title": "Создание валюты",
                "update_title": "Редактирование валюты",
                "required_help": "- поля обязательные для заполнения",
                "name": "Название:",
                "symbol": "Символ:",
                "code": "Код:",
                "code_iso": "Код ISO:",
                "code_iso_help": "Код ISO 4217.",
                "rate": "Курс",
                "rate_fixed_help": "Курс для обмена. Поставьте галочку чтобы зафиксировать",
                "auto_update": "Авто-обновление курса:",
                "submit": "Сохранить"
            }
        },
        "settings": {
            "title": "Настройки проекта:",
            "save": "Сохранить",
            "submit": "Сохранить",
            "social_auth": "Авторизация через соц.сети",
            "fake": {
                "total": "Всего:",
                "fake_count": "Фейковых:",
                "hint1": "* Данные цифры будут суммированы с реальными показателями",
                "hint2": "** Вы можете как добавить к сумме вывода средств, так и уменьшить её. Для уменьшения суммы, укажите перед цифрой символ \"-\" (минус)",
                "success": "Статистика обновлена"
            },
            "telegram_bot_key": "Ключ для телеграмм бота (обновляется каждые 5 минут)",
            "fields": {
                "general": {
                    "enabled": {
                        "label": "Работа сайта",
                        "help": "Работа сайта"
                    },
                    "log_ip": {
                        "label": "Отслеживатель IP",
                        "help": "Записывать IP в лог"
                    },
                    "demo_registration": {
                        "label": "Возможности регистрации DEMO",
                        "help": "Возможности регистрации DEMO"
                    },
                    "admin_mail": {
                        "label": "E-mail администратора",
                        "help": "E-mail администратора"
                    },
                    "max_withdraw": {
                        "label": "Максимальная сумма на вывод",
                        "help": "Максимальная сумма на вывод"
                    },
                    "min_withdraw": {
                        "label": "Минимальная сумма на вывод",
                        "help": "Минимальная сумма на вывод"
                    },
                    "withdraws_daily_limit": {
                        "label": "Дневной лимит на вывод",
                        "help": "Дневной лимит на вывод"
                    },
                    "withdraw_api_limit": {
                        "label": "Максимальный вывод через API",
                        "help": "Максимальный вывод через API"
                    },
                    "start_date": {
                        "label": "Дата старта проекта",
                        "help": "Дата старта проекта"
                    },
                    "reinvestment": {
                        "label": "Реинвестирование",
                        "help": "Реинвестирование"
                    },
                    "registration_confirmation": {
                        "label": "Подтверждение регистрации по e-mail",
                        "help": "Подтверждение регистрации по e-mail"
                    },
                    "accrual_enabled": {
                        "label": "Начисление процентов",
                        "help": "Начисление процентов"
                    },
                    "ssl_enabled": {
                        "label": "SSL",
                        "help": "SSL"
                    },
                    "tg_bot_enabled": {
                        "label": "Telegram бот",
                        "help": "Включить Telegram бот"
                    }
                },
                "fake": {
                    "active_users": {
                        "label": "Добавить активных пользователей",
                        "help": "Добавить активных пользователей"
                    },
                    "users": {
                        "label": "Добавить фейковых пользователей",
                        "help": "Добавить фейковых пользователей"
                    },
                    "online_users": {
                        "label": "Добавить пользователей онлайн",
                        "help": "Добавить пользователей онлайн"
                    },
                    "today_deposits_sums": {
                        "label": "Добавить к сумме депозитов сегодня",
                        "help": "Добавить к сумме депозитов сегодня"
                    },
                    "refills_sum": {
                        "label": "Добавить/отнять сумму вывода **",
                        "help": "Добавить/отнять сумму вывода **"
                    },
                    "withdraws_sum": {
                        "label": "Добавить/отнять сумму пополнений **",
                        "help": "Добавить/отнять сумму пополнений **"
                    }
                },
                "users": {
                    "2fa": {
                        "label": "Двухфакторная аутентификация",
                        "help": ""
                    }
                },
                "admin": {
                    "url": {
                        "label": "Префикс для админ-панели",
                        "help": "Префикс для путей в админке(https://demo.local/ваш_префикс/users)"
                    }
                },
                "currencies": {
                    "default": {
                        "label": "Валюта по умолчанию",
                        "help": "Базовая валюта на сайте и у пользователей"
                    }
                },
                "refsys": {
                    "depo_create_percents": {
                        "label": "Реферальные проценты за создание депозита",
                        "help": "Проценты по уровням в формате level1-level2-level3...."
                    },
                    "depo_accruals_percents": {
                        "label": "Реферальные проценты за начисления по депозиту",
                        "help": "Проценты по уровням в формате level1-level2-level3...."
                    }
                },
                "social_auth": {
                    "enabled": "Включена",
                    "provider_enabled": "включен"
                }
            }
        },
        "referrals": {
            "table": {
                "id": "№",
                "login": "Логин:",
                "profit": "Доход:"
            },
            "settings": {
                "title": "Реферальные уровни",
                "total_levels": "Всего уровней:",
                "level": "Уровень:",
                "percent": "Процент:",
                "add": "Добавить уровень",
                "hint": "*После добавления уровня укажите процент",
                "submit": "Сохранить"
            }
        },
        "users": {
            "balances": {
                "total": "Всего денег на балансе у пользователей:",
                "total_after_24h": "Всего денег на балансе у пользователей через 24 часа:"
            },
            "search_form": {
                "title": "Найти пользователя по Логину / ID / e-mail / кошельку",
                "field": "Поиск:",
                "submit": "Искать"
            },
            "create_form": {
                "title": "Создание нового пользователя",
                "total": "Всего пользователей:",
                "login": "Логин:",
                "email": "E-mail:",
                "password": "Пароль:",
                "password_repeat": "Повторите пароль:",
                "submit": "Создать"
            },
            "table": {
                "sort_by": "Сортировать по:",
                "sorts": {
                    "id": "ID",
                    "login": "Логин",
                    "balance": "Баланс",
                    "created_at": "Регистрации",
                    "last_login_at": "Последний вход",
                    "last_login_ip": "Последний IP",
                    "country_code": "Страна",
                    "role": "Статус"
                },
                "id": "ID",
                "login": "Логин",
                "balance": "Баланс",
                "created": "Регистрации",
                "last_login": "Последний вход",
                "last_login_ip": "Последний IP",
                "country": "Страна",
                "role": "Статус",
                "online": "Онлайн",
                "edit": "Редактировать"
            },
            "form": {
                "title": "Редактирование данных пользователя:",
                "password": "Пароль:",
                "password_repeat": "Повторите пароль:",
                "email": "E-mail:",
                "balance": "Баланс:",
                "currency": "Валюта:",
                "wallet": "Счет {code}:",
                "referral_percent": "Реферальный %:",
                "comment": "Комментарий:",
                "submit": "Сохранить"
            },
            "deposits": {
                "title": "Депозиты пользователя:",
                "id": "ID",
                "date": "Дата",
                "amount": "Сумма",
                "plan": "Тарифный план"
            },
            "refills": {
                "title": "Пополнения:",
                "total": "Всего:",
                "id": "ID",
                "date": "Дата",
                "amount": "Сумма",
                "wallet": "Счет",
                "system": "Система"
            },
            "withdraws": {
                "title": "Выводы:",
                "total": "Всего:",
                "id": "ID",
                "date": "Дата",
                "amount": "Сумма",
                "wallet": "Счет",
                "system": "Система"
            },
            "referrals": {
                "title": "Рефералы:",
                "inviter": "Пригласитель:",
                "id": "№",
                "login": "Логин",
                "registered": "Зарегистрирован:",
                "profit": "Доход:"
            },
            "auth_logs": {
                "title": "Авторизации за последние 30 дней:",
                "user": "Пользователь",
                "date": "Дата",
                "ip": "IP",
                "country": "Страна"
            },
            "deposit_form": {
                "text": "Открытие депозита пользователю:",
                "submit": "Открыть депозит"
            },
            "message": {
                "title": "Отправить сообщение пользователю:",
                "subject": "Тема тикета:",
                "from": "Сообщение от (логин):",
                "submit": "Отправить"
            },
            "mail": {
                "title": "Отправить сообщение пользователю на почту:",
                "from": "Сообщение от:",
                "submit": "Отправить"
            },
            "messages": {
                "title": "Сообщения пользователя:",
                "id": "ID",
                "date": "Дата",
                "from": "От",
                "text": "Текст",
                "status": "Статус",
                "edit": "Ред.",
                "status_read": "Прочитано",
                "status_unread": "Не прочитано"
            },
            "tickets": {
                "title": "Тикеты пользователя:",
                "id": "ID",
                "last_message": "Дата последнего сообщения",
                "subject": "Тема",
                "status": "Статус",
                "status_read": "Прочитано",
                "status_unread": "Не прочитано",
                "edit": "Ред."
            },
            "newsletter": {
                "from": "От кого:",
                "subject": "Тема:",
                "submit": "Отправить"
            }
        },
        "deposits": {
            "short_statistics": {
                "title": "Краткая статистика",
                "opened_sum": "Открытых депозитов на сумму:",
                "total_sum": "Всего:",
                "opened_count": "Всего открыто депозитов:"
            },
            "referral_payments": {
                "title": "Рефферальные отличиления:"
            },
            "charge_form": {
                "title": "Начислить проценты по депозитам вручную",
                "percent": "Процент от суммы вклада:",
                "plan": "Тарифный план:",
                "submit": "Начислить",
                "help": "- в течении нескольких секунд будет начислено"
            },
            "table": {
                "title": "Список депозитов",
                "sort_by": "Сортировать по:",
                "sorts": {
                    "id": "ID",
                    "date": "Дата",
                    "login": "Логин",
                    "amount": "Сумма",
                    "plan": "План"
                },
                "id": "ID",
                "date": "Дата",
                "login": "Логин",
                "amount": "Сумма",
                "plan": "План",
                "statistics": {
                    "title": "Статистика по депозиту",
                    "opened_date": "Дата открытия:",
                    "closed_date": "Дата закрытия:"
                }
            }
        },
        "plans": {
            "title": "Действующие тарифные планы",
            "table": {
                "name": "Название:",
                "amount": "Сумма вклада:",
                "percent": "Процент:",
                "duration": "Срок:"
            },
            "percent_type": {
                "hourly": "в час",
                "daily": "в день"
            },
            "duration_type": {
                "hourly": "часов",
                "daily": "дней"
            },
            "create_btn": "Создать новый тариф",
            "create_btn_note": "Чтобы создать новый тарифный план - перейдите на форму создания",
            "form": {
                "create_title": "Создание тарифного плана",
                "edit_title": "Редактирование тарифного плана",
                "required_help": "- поля обязательные для заполнения",
                "duration_label": {
                    "hourly": "Срок (часов)",
                    "daily": "Срок (дней)"
                },
                "name": "Название:",
                "amount": "Суммы вкладов:",
                "currency": "Валюта:",
                "percent": "Процент:",
                "amount_bonus": "Бонус к сумме депозита (%):",
                "balance_bonus": "Бонус на баланс от суммы депозита (%):",
                "not_charge_weekends": "Не начислять в выходные:",
                "can_close": "Возможность досрочного закрытия:",
                "additional_referral_percents": "Дополнительные проценты с рефералов:",
                "is_limited": "Лимитированный тариф:",
                "return_on_close": "Возврат вклада в конце срока:",
                "additional_referral_percent": "Процент начисления с одного реферала:",
                "additional_max_percent": "Максимальный процент начисления:",
                "max_deposits": "Максимальное количество созданных депозитов:",
                "return_percent": "Процент от суммы депозита:",
                "create_submit": "Создать тариф",
                "edit_submit": "Сохранить тариф"
            }
        }
    }
}
