<?php


namespace Modules\Core\CurrencyRateUpdaters;

/**
 * Class BaseCurrencyProvider
 * Base class for currency rates provider
 * @package Modules\Currencies
 */
abstract class BaseCurrencyProvider
{
    /**
     * List of available currencies in ISO 4217 format
     * @return array Empty array if not require
     */
    abstract public static function getAvailableCurrencies() : array;

    /**
     * List of supported currency types (fiat or/and crypto)
     * @return array ex: [Currency::TYPE_FIAT, Currency::TYPE_CRYPTO] (watch types in Currency model)
     */
    abstract public static function getSupportedCurrencyTypes() : array;

    /**
     * Get currency exchange pairs
     *
     * @param  array  $codes Array of ISO 4217 currency codes
     * @param  string $base_code (optional) Force base code (not fetching cross rates pair)
     * @return array ex: ['USD_EUR' => 0.9, 'EUR_USD' => 1.1]
     */
    abstract public function getRates(array $codes, string $base_code = null) : array;

    /**
     * Getting name of Currency Provider
     * @return string
     */
    abstract public static function getName();

    /**
     * Getting alias of Currency Provider
     * @return string
     */
    abstract public static function getAlias();
}
