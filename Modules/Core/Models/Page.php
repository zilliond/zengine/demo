<?php

namespace Modules\Core\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

/**
 * Modules\Core\Models\Page
 *
 * @property int $id
 * @property string $route
 * @property string|null $keywords
 * @property array $title
 * @property array $description
 * @property array $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read array $translations
 * @property-read mixed $url
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Page onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Page withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Page withoutTrashed()
 * @mixin \Eloquent
 */
class Page extends Model
{
    use SoftDeletes, HasTranslations;

    public $table = 'pages';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'title',
        'description',
        'body',
        'route'
    ];

    public $translatable = [
        'title',
        'description',
        'body'
    ];

    protected $appends = ['url'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'title'       => 'string',
        'description' => 'string',
        'keywords'    => 'string',
        'body'        => 'string',
        'route'       => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title'       => 'required',
        'description' => 'required',
        'keywords'    => 'nullable',
        'body'        => 'min:50',
        'route'       => 'required'
    ];

    public function getUrlAttribute()
    {
        return route('page', $this->route);
    }
}
