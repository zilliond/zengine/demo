<?php


namespace Modules\Core\Payments;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Log;
use Modules\Core\Exceptions\OperationException;
use Modules\Core\Exceptions\PaymentSystemException;
use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;
use Modules\Core\Services\OperationService;

abstract class PaymentSystemProvider
{
    /**
     * @var Client
     */
    protected $guzzle;

    /**
     * @var \Modules\Core\Services\OperationService
     */
    protected $operationService;

    /**
     * @var \Modules\Core\Services\CurrencyServiceInterface
     */
    protected $currencyService;

    /**
     * @var array
     */
    protected $credentials = [];

    /**
     * Can api auto withdraw
     *
     * @var bool
     */
    public $can_semi_auto_withdraw = false;

    /**
     * Can semi auto withdraw
     *
     * @var bool
     */
    public $can_auto_withdraw = false;

    public function __construct()
    {
        $this->guzzle = new Client();
        $this->operationService = app('operations');
        $this->currencyService = app('currencies');
    }

    /**
     * Getter for name
     * @return string
     */
    abstract public function getName() : string;

    /**
     * Withdraw types
     *
     * @return array
     */
    public function getTypes() : array
    {
        return [
            'can_manual'    => true,
            'can_semi_auto' => $this->can_semi_auto_withdraw,
            'can_auto'      => $this->can_auto_withdraw,
        ];
    }

    /**
     * @param $credentials
     */
    public function setCredentials(array $credentials)
    {
        $this->credentials = $credentials;
    }

    /**
     * @param  array  $keys
     * @return void
     * @throws PaymentSystemException
     */
    public function checkCredentials(array $keys = []) : void
    {
        if (!is_array($this->credentials) || !count($this->credentials)) {
            Log::channel('payment_systems')->error('PaymentSystem | Credentials not define');
            throw new PaymentSystemException('PaymentSystem | Credentials not define');
        }
        foreach ($keys as $key) {
            if (!(isset($this->credentials[$key]) && $this->credentials[$key])) {
                Log::channel('payment_systems')->error("PaymentSystem | Credential {$key} not define");
                throw new PaymentSystemException("PaymentSystem | Credential {$key} not define");
            }
        }
    }

    /**
     * @param  string  $key
     * @return mixed
     * @throws OperationException
     */
    public function getCredential(string $key)
    {
        $this->checkCredentials([$key]);
        return $this->credentials[$key];
    }

    /**
     * @param  Request  $request
     * @return bool
     * @noinspection PhpUnusedParameterInspection
     */
    public function canHandle(Request $request): bool
    {
        return false;
    }

    abstract public function makeRefill(Operation $operation): Operation;
    abstract public function handle(Request $request);

    /**
     * @return float
     */
    public function getBalance() : float
    {
        return 0.00;
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws OperationException
     * @noinspection PhpUnusedParameterInspection
     */
    public function makeWithdraw(Operation $operation) : Operation
    {
        throw new OperationException('Withdraw don\'t support');
    }

    abstract public function getCredentialsKeys() : array;
    abstract public function getCredentialsFields() : array;
}
