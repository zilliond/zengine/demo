<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Models\UserBalance
 *
 * @property int $id
 * @property int $user_id
 * @property int $payment_system_id
 * @property float $balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Models\PaymentSystem $payment_system
 * @property-read \Modules\Core\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance wherePaymentSystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserBalance whereUserId($value)
 * @mixin \Eloquent
 */
class UserBalance extends Model
{
    protected $fillable = [
        'user_id',
        'payment_system_id',
        'balance'
    ];

    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }

    public function payment_system()
    {
        return $this->belongsTo(app('zengine')->modelClass('PaymentSystem'));
    }
}
