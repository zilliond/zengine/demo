<?php

return [
    'tickets' => [
        'title'         => 'Тикеты',
        'create_new'    => 'Создать новый',
        'subject'       => 'Тема',
        'status'        => 'Статус',
        'actions'       => 'Действия',
        'new_messages'  => 'Новых сообщений :count',
    ],
    'create' => [
        'title'         => 'Создать тикет',
        'subject'       => 'Тема',
        'message'       => 'Сообщение',
        'submit'        => 'Отправить',
    ],
    'ticket' => [
        'message'       => 'Сообщение',
        'submit'        => 'Отправить',
    ]
];
