<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(app('zengine')->modelClass('Page'), 5)->create()->each(function (Page $page) {
            $this->command->info(route('page', $page->route).' page created.');
        });
    }
}
