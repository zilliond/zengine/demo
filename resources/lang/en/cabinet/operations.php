<?php

return [
    'title'             => 'Operations',
    'type'              => 'Type',
    'status'            => 'Status',
    'currency'          => 'Currency',
    'wallet'            => 'Wallet',
    'payment_system'    => 'Payment system',
    'amount'            => 'Amount',
    'deposit'           => 'Deposit',
    'created'           => 'Created',
    'actions'           => 'Actions',
];
