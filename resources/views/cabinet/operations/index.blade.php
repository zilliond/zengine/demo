@extends('layouts.cabinet')
@section('body')
    @php /** @var $operation \Modules\Core\Models\Operation */ @endphp
    @include('flash::message')
    <h1>@lang('cabinet/operations.title')</h1>
    <form type="GET" class="row">
        <div class="form-group col-sm-6 col-md-3">
            {!! Form::label('type', trans('cabinet/operations.type')) !!}
            {!! Form::select('type', [null => ''] + trans('operations.types'), request('type'), ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-6 col-md-3">
            {!! Form::label('status', trans('cabinet/operations.status')) !!}
            {!! Form::select('status', [null => ''] + trans('operations.statuses'), request('status'), ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-6 col-md-3">
            {!! Form::label('currency_id', trans('cabinet/operations.currency')) !!}
            {!! Form::select('currency_id', [null => ''] + app('zengine')->model('Currency')->pluck('code_iso', 'id')->toArray(), request('currency_id'), ['class' => 'form-control']) !!}
        </div>
        @if(setting('balances_type') === 'divided')
            <div class="form-group col-sm-6 col-md-3">
                {!! Form::label('wallet_id', trans('cabinet/operations.wallet')) !!}
                {!! Form::select('wallet_id', [null => ''] + $wallets->pluck('payment_system.name', 'id')->toArray(), request('wallet_id'), ['class' => 'form-control']) !!}
            </div>
        @elseif(setting('balances_type') === 'shared')
            <div class="form-group col-sm-6 col-md-3">
                {!! Form::label('payment_system_id', trans('cabinet/operations.payment_system')) !!}
                {!! Form::select('payment_system_id', [null => ''] + app('zengine')->model('PaymentSystem')->pluck('name', 'id')->toArray(), request('payment_system_id'), ['class' => 'form-control']) !!}
            </div>
        @endif
        <div class="col-sm-6" style="margin-bottom: 1rem">
            <button type="submit" class="btn btn-primary">@lang('cabinet/ui.search')</button>
            <br>
        </div>
    </form>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">@lang('cabinet/operations.type')</th>
            <th scope="col">@lang('cabinet/operations.status')</th>
            <th scope="col">@lang('cabinet/operations.amount')</th>
            @if(setting('balances_type') === 'divided')
                <th scope="col">@lang('cabinet/operations.wallet')</th>
            @elseif(setting('balances_type') === 'shared')
                <th scope="col">@lang('cabinet/operations.payment_system')</th>
            @endif
            <th scope="col">@lang('cabinet/operations.deposit')</th>
            <th scope="col">@lang('cabinet/operations.created')</th>
            <th scope="col">@lang('cabinet/operations.actions')</th>
        </tr>
        </thead>
        <tbody>
        @php
            $currencyService = app('currencies');
            $defaultCurrency = $currencyService->getDefaultCurrency(); //TODO: Use User Currency
        @endphp
        @php /* @var $serivce \Modules\Core\Services\OperationService */ @endphp
        @foreach($operations as $operation)
            @php /* @var $operation \Modules\Core\Models\Operation */ @endphp
            <tr>
                <th scope="row">{{ $operation->id }}</th>
                <td>{{ trans('operations.types.'.$operation->type) }}</td>
                <td>
                    @if(in_array($operation->status, [app('zengine')->modelClass('Operation')::STATUS_CREATED, app('zengine')->modelClass('Operation')::STATUS_SUCCESS, app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS], true))
                    <span class="text-success">
                        {{ trans('operations.statuses.'.$operation->status) }}
                    </span>
                    @else
                    <span class="text-danger">
                        {{ trans('operations.statuses.'.$operation->status) }}
                    </span>
                    @endif
                </td>
                <td>
                    {{ $operation->amount }} {{ $operation->currency->symbol }}
                    @if($operation->currency_id !== $defaultCurrency->id) ( {{ $operation->getAmountInCurrency($defaultCurrency) }} {{ $defaultCurrency->symbol }} ) @endif
                </td>
                @if(setting('balances_type') === 'divided')
                    <td>{{ $operation->wallet->payment_system->name }}</td>
                @elseif(setting('balances_type') === 'shared')
                    <td>{{ optional($operation->payment_system)->name }}</td>
                @endif
                <td>
                    @if(strpos($operation->type, 'deposit_') === 0)
                        <a href="{{ route('cabinet.deposits') }}">{{ $operation->target_id }}</a>
                    @endif
                </td>
                <td>{{ optional($operation->created_at)->toDateTimeString() }}</td>
                <td>
                    @if($service->typeByOperation($operation)->canCancel($operation))
                        {!! Form::open(['route' => ['cabinet.operations.cancel', $operation], 'method' => 'post']) !!}
                            {!! Form::button('<i class="fas fa-times"></i>',
                                ['type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        {!! Form::close() !!}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $operations->appends(request()->query())->links() }}
@endsection
