<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Modules\Core\Models\User;

class RequestLocale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (array_key_exists($request->get('locale'), config('app.supportedLocales'))) {
            \Session::put('app.locale', $request->get('locale'));
            /* @noinspection NotOptimalIfConditionsInspection */
            /** @var User $user */
            if (\Auth::check() && $user = \Auth::user()) {
                $user->locale = $request->get('locale');
                $user->save();
            }
        } elseif (\Auth::check() && array_key_exists(\Auth::user()->locale, config('app.supportedLocales'))) {
            \Session::put('app.locale', \Auth::user()->locale);
        }
        if (!\Session::has('app.locale')) {
            \Session::put('app.locale', config('app.locale', 'en'));
        }
        app()->setLocale(\Session::get('app.locale'));

        return $next($request);
    }
}
