<?php

use Modules\Admin\Http\Controllers\Api\AnalyticsController;
use Modules\Admin\Http\Controllers\Api\CurrencyController;
use Modules\Admin\Http\Controllers\Api\DashboardController;
use Modules\Admin\Http\Controllers\Api\DepositController;
use Modules\Admin\Http\Controllers\Api\FaqController;
use Modules\Admin\Http\Controllers\Api\IpController;
use Modules\Admin\Http\Controllers\Api\LocaleController;
use Modules\Admin\Http\Controllers\Api\OperationController;
use Modules\Admin\Http\Controllers\Api\PageController;
use Modules\Admin\Http\Controllers\Api\PaymentSystemController;
use Modules\Admin\Http\Controllers\Api\PlanController;
use Modules\Admin\Http\Controllers\Api\PostController;
use Modules\Admin\Http\Controllers\Api\PromoController;
use Modules\Admin\Http\Controllers\Api\ReferralController;
use Modules\Admin\Http\Controllers\Api\SettingController;
use Modules\Admin\Http\Controllers\Api\TicketController;
use Modules\Admin\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'auth:api'], function () {
    Route::get('dashboard', [DashboardController::class, 'index']);
    Route::get('short_stats', [DashboardController::class, 'short_stats']);
    Route::get('user', [DashboardController::class, 'user']);

    Route::get('locale', [LocaleController::class, 'locale']);
    Route::post('locale', [LocaleController::class, 'setLocale']);
    Route::get('locale/supported', [LocaleController::class, 'supported']);

    Route::get('plans', [PlanController::class, 'index']);
    Route::post('plans', [PlanController::class, 'store']);
    Route::get('plans/{id}', [PlanController::class, 'show']);
    Route::post('plans/{id}', [PlanController::class, 'update']);
    Route::post('plans/{id}/visible', [PlanController::class, 'toggleVisible']);
    Route::post('plans/{id}/delete', [PlanController::class, 'destroy']);

    Route::get('deposits', [DepositController::class, 'index']);
    Route::post('deposits', [DepositController::class, 'store']);
    Route::get('deposits/stats', [DepositController::class, 'stats']);
    Route::get('deposits/refs', [DepositController::class, 'refs']);
    Route::post('deposits/customAccrual', [DepositController::class, 'customAccrual']);

    Route::get('currencies', [CurrencyController::class, 'index']);
    Route::post('currencies', [CurrencyController::class, 'store']);
    Route::get('currencies/rates', [CurrencyController::class, 'rates']);
    Route::get('currencies/update_rates', [CurrencyController::class, 'update_rates']);
    Route::get('currencies/{id}', [CurrencyController::class, 'show']);
    Route::post('currencies/{id}', [CurrencyController::class, 'update']);
    Route::post('currencies/{id}/delete', [CurrencyController::class, 'destroy']);
    Route::get('currencies/{id}/update_rate', [CurrencyController::class, 'update_rate']);

    Route::get('payment_systems', [PaymentSystemController::class, 'index']);
    Route::get('payment_systems/providers', [PaymentSystemController::class, 'providers']);
    Route::get('payment_systems/balances', [PaymentSystemController::class, 'balances']);
    Route::post('payment_systems', [PaymentSystemController::class, 'store']);
    Route::get('payment_systems/{id}', [PaymentSystemController::class, 'show']);
    Route::post('payment_systems/{id}', [PaymentSystemController::class, 'update']);
    Route::post('payment_systems/{id}/delete', [PaymentSystemController::class, 'destroy']);
    Route::get('payment_systems/{id}/balance', [PaymentSystemController::class, 'balance']);

    Route::get('users', [UserController::class, 'index']);
    Route::post('users', [UserController::class, 'create']);
    Route::get('users/count', [UserController::class, 'count']);
    Route::get('users/balances', [UserController::class, 'balances']);
    Route::get('users/search', [UserController::class, 'search']);
    Route::get('users/{id}', [UserController::class, 'show']);
    Route::post('users/{id}', [UserController::class, 'update']);
    Route::get('users/{id}/tickets', [UserController::class, 'tickets']);
    Route::post('users/{id}/mail', [UserController::class, 'mail']);
    Route::post('users/{id}/send_message', [UserController::class, 'sendMessage']);
    Route::get('users/{id}/short_deposits', [UserController::class, 'shortDeposits']);
    Route::get('users/{id}/short_refills', [UserController::class, 'shortRefills']);
    Route::get('users/{id}/short_withdraws', [UserController::class, 'shortWithdraws']);
    Route::get('users/{id}/short_referrals', [UserController::class, 'shortReferrals']);
    Route::get('users/{id}/short_logs', [UserController::class, 'shortAuthLogs']);
    Route::get('users/{id}/short_messages', [UserController::class, 'shortMessages']);
    Route::get('users/{id}/short_tickets', [UserController::class, 'shortTickets']);
    Route::get('users/{id}/referrals', [UserController::class, 'referrals']);
    Route::post('users/{id}/delete', [UserController::class, 'delete']);
    Route::post('users/{id}/role', [UserController::class, 'setRole']);
    Route::get('users/{id}/payment_systems', [UserController::class, 'payment_systems']);
    Route::get('users/{id}/wallets', [UserController::class, 'wallets']);
    Route::post('users/{id}/wallets', [UserController::class, 'storeWallets']);

    Route::get('referrals', [ReferralController::class, 'index']);
    Route::get('referrals/settings', [ReferralController::class, 'settings']);
    Route::post('referrals/settings', [ReferralController::class, 'updateSettings']);

    Route::get('auth_log', [IpController::class, 'auth_log']);
    Route::get('blacklist', [IpController::class, 'blacklist']);
    Route::post('blacklist', [IpController::class, 'storeBlacklistEntry']);
    Route::post('blacklist/{id}', [IpController::class, 'updateBlacklistEntry']);
    Route::post('blacklist/{id}/delete', [IpController::class, 'deleteBlacklistEntry']);

    Route::post('messages/{message}/delete', [UserController::class, 'deleteMessage']);
    Route::post('messages', [UserController::class, 'sendMessageAll']);

    Route::get('pages', [PageController::class, 'index']);
    Route::post('pages', [PageController::class, 'store']);
    Route::get('pages/{id}', [PageController::class, 'show']);
    Route::post('pages/{id}', [PageController::class, 'update']);
    Route::post('pages/{id}/delete', [PageController::class, 'delete']);

    Route::get('posts', [PostController::class, 'index']);
    Route::post('posts', [PostController::class, 'store']);
    Route::get('posts/stats', [PostController::class, 'stats']);
    Route::get('posts/{id}', [PostController::class, 'show']);
    Route::post('posts/{id}', [PostController::class, 'update']);
    Route::post('posts/{id}/toggleHidden', [PostController::class, 'toggle_hidden']);
    Route::post('posts/{id}/delete', [PostController::class, 'delete']);

    Route::get('settings/fake', [SettingController::class, 'fake']);
    Route::post('settings/fake', [SettingController::class, 'updateFake']);

    Route::get('settings/general', [SettingController::class, 'general']);
    Route::post('settings/general', [SettingController::class, 'updateGeneral']);

    Route::get('settings/social', [SettingController::class, 'social']);
    Route::post('settings/social', [SettingController::class, 'updateSocial']);

    Route::get('system/info', [SettingController::class, 'system_info']);

    Route::get('operations', [OperationController::class, 'index']);
    Route::get('operations/stats', [OperationController::class, 'stats']);
    Route::post('operations/{id}/success', [OperationController::class, 'success']);
    Route::post('operations/{id}/reject', [OperationController::class, 'reject']);

    Route::get('promos', [PromoController::class, 'index']);
    Route::get('promos/stats', [PromoController::class, 'stats']);
    Route::get('promos/{id}', [PromoController::class, 'show']);
    Route::post('promos/{id}', [PromoController::class, 'update']);
    Route::post('promos/{id}/delete', [PromoController::class, 'delete']);
    Route::post('promos', [PromoController::class, 'store']);

    Route::post('analytics', [AnalyticsController::class, 'index']);
    Route::get('analytics/stats', [AnalyticsController::class, 'stats']);

    Route::get('faq_items', [FaqController::class, 'index']);
    Route::post('faq_items', [FaqController::class, 'store']);
    Route::get('faq_items/{id}', [FaqController::class, 'show']);
    Route::get('faq_items/{id}/toggleHidden', [FaqController::class, 'toggle_hidden']);
    Route::post('faq_items/{id}', [FaqController::class, 'update']);
    Route::post('faq_items/{id}/delete', [FaqController::class, 'destroy']);

    Route::get('tickets', [TicketController::class, 'index']);
    Route::post('tickets', [TicketController::class, 'store']);
    Route::post('tickets/message', [TicketController::class, 'message']);
    Route::get('tickets/{id}', [TicketController::class, 'show']);
    Route::post('tickets/{id}/delete', [TicketController::class, 'destroy']);
});
