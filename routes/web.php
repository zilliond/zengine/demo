<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Cabinet\CabinetController;
use App\Http\Controllers\Cabinet\DepositController;
use App\Http\Controllers\Cabinet\OperationController;
use App\Http\Controllers\Cabinet\SecurityController;
use App\Http\Controllers\Cabinet\TicketController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;

app('zengine')->webRoutes();
if (setting('general.ssl_enabled')) {
    URL::forceScheme('https');
}

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('faq', [HomeController::class, 'faq'])->name('faq');

Route::group(['prefix' => 'news', 'as' => 'news'], static function () {
    Route::get('/', [NewsController::class, 'index']);
    Route::get('{post}', [NewsController::class, 'show'])->name('.show');
    Route::get('{post}/watched', [NewsController::class, 'watched'])->name('.watched');
});

Route::group([
    'prefix'        => 'cabinet',
    'as'            => 'cabinet.',
    'middleware'    => 'auth'
], function () {
    Route::get('/', [CabinetController::class, 'index'])->name('index');
    Route::get('/referrals', [CabinetController::class, 'referrals'])->name('referrals');
    Route::get('/profile', [CabinetController::class, 'profile'])->name('profile');
    Route::post('/profile', [CabinetController::class, 'storeProfile'])->name('profile.store');
    Route::post('/wallets', [CabinetController::class, 'storeWallets'])->name('wallets.store');

    Route::get('/security', [SecurityController::class, 'index'])->name('security');
    Route::post('/security/password', [SecurityController::class, 'changePassword'])->name('security.password');
    Route::post('/security/2fa/enable', [SecurityController::class, 'enable2fa'])->name('security.2fa.enable');
    Route::post('/security/2fa/disable', [SecurityController::class, 'disable2fa'])->name('security.2fa.disable');
    Route::post('/security/pin/enable', [SecurityController::class, 'enablePin'])->name('security.pin.enable');
    Route::post('/security/pin/disable', [SecurityController::class, 'disablePin'])->name('security.pin.disable');
    Route::post('/security/pin/unlock', [SecurityController::class, 'unlock'])->name('security.pin.unlock');
    Route::get('/security/pin/lock', [SecurityController::class, 'lock'])->name('security.pin.lock');

    Route::get('/deposits', [DepositController::class, 'index'])->name('deposits');
    Route::get('/invest', [DepositController::class, 'create'])->name('deposits.create');
    Route::post('/invest', [DepositController::class, 'store'])->name('deposits.store');
    Route::get('/deposits/{deposit}/close', [DepositController::class, 'close'])->name('deposits.close');

    Route::get('/operations', [OperationController::class, 'index'])->name('operations');
    Route::post('/operations/{operation}/cancel', [OperationController::class, 'cancel'])->name('operations.cancel');

    Route::get('/refill/{status?}', [OperationController::class, 'refill'])->name('refill');
    Route::post('/refill', [OperationController::class, 'storeRefill'])->name('refill.store');

    Route::get('/withdraw/{status?}', [OperationController::class, 'withdraw'])->name('withdraw');
    Route::post('/withdraw', [OperationController::class, 'storeWithdraw'])->name('withdraw.store');

    Route::get('/transfer', [OperationController::class, 'transfer'])->name('transfer');
    Route::post('/transfer', [OperationController::class, 'storeTransfer'])->name('transfer.store');

    Route::resource('tickets', TicketController::class);
    Route::post('tickets/{ticket}/message', [TicketController::class, 'message'])->name('tickets.message');
    Route::post('tickets/{ticket}/status', [TicketController::class, 'status'])->name('tickets.status');
    Route::get('tickets/{ticket}/read', [TicketController::class, 'read'])->name('tickets.read');
});
