<?php

namespace Modules\Core\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class SetInviterCookie
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        /* @var $response Response */
        if (!$request->hasCookie('inviter') && $request->query('ref') && $user = app('zengine')->model('User')->find($request->query('ref'))) {
            $response->cookie('inviter', $user->id, 525600);
        }

        return $response;
    }
}
