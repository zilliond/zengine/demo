<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Models\PaymentSystem;
use Modules\Core\Payments\PaymentSystemProvider;
use Modules\Core\Services\OperationService;

class PaymentSystemController extends Controller
{
    /**
     * @var OperationService
     */
    private $operationService;

    public function __construct(OperationService $operationService)
    {
        $this->operationService = $operationService;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function providers()
    {
        return collect($this->operationService->getProviders())->map(static function (PaymentSystemProvider $paymentSystemProvider) {
            return [
                'name'           => $paymentSystemProvider->getName(),
                'fields'         => $paymentSystemProvider->getCredentialsFields(),
                'withdraw_types' => $paymentSystemProvider->getTypes()
            ];
        });
    }

    /**
     * Display a listing of the PaymentSystem.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $paymentSystems = app('zengine')->model('PaymentSystem')->with(['currency'])->get();
        return $paymentSystems->map(function (PaymentSystem $paymentSystem) {
            $data = $paymentSystem->toArray();
            $data['credentials'] = $paymentSystem->credentials;
            return $data;
        });
    }

    /**
     * @param $id
     * @return array
     */
    public function show($id)
    {
        $paymentSystem = app('zengine')->model('PaymentSystem')->find($id);
        $data = $paymentSystem->toArray();
        $data['credentials'] = $paymentSystem->credentials;
        return $data;
    }

    /**
     * Store a newly created PaymentSystem in storage.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function store(Request $request)
    {
        $paymentSystem = app('zengine')->model('PaymentSystem');
        $paymentSystem->fill($request->all());
        $paymentSystem->save();

        return [
            'status'            => 'success',
            'message'           => 'PaymentSystem saved successfully.',
            'payment_system'    => $paymentSystem
        ];
    }

    /**
     * Update the specified PaymentSystem in storage.
     *
     * @param $id
     * @param  Request  $request
     *
     * @return array
     */
    public function update($id, Request $request)
    {
        $paymentSystem = app('zengine')->model('PaymentSystem')->find($id);
        $paymentSystem->fill($request->all());
        $paymentSystem->save();

        return [
            'status'            => 'success',
            'message'           => 'PaymentSystem updated successfully.',
            'payment_system'    => $paymentSystem
        ];
    }

    /**
     * Remove the specified PaymentSystem from storage.
     *
     * @param  $id
     *
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        $paymentSystem = app('zengine')->model('PaymentSystem')->find($id);
        $paymentSystem->delete();

        return [
            'status'            => 'success',
            'message'           => 'PaymentSystem deleted successfully.',
            'payment_system'    => $paymentSystem
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function balance($id)
    {
        $paymentSystem = app('zengine')->model('PaymentSystem')->find($id);
        $provider = $paymentSystem->getProvider();
        if (is_array($paymentSystem->credentials) && count($paymentSystem->credentials)) {
            $provider->setCredentials($paymentSystem->credentials);
        }
        if ($provider) {
            $balance = $provider->getBalance();
            if ($balance) {
                $paymentSystem->balance = $balance;
                $paymentSystem->save();
                return [
                    'status'    => 'success',
                    'balance'   => $balance
                ];
            }
        }
        return [
            'status'    => 'error',
            'balance'   => 0.00
        ];
    }

    /**
     * @return array
     */
    public function balances()
    {
        $payment_systems = app('zengine')->model('PaymentSystem')->get()->keyBy('id');
        $errors = 0;
        $success = 0;
        $balances = $payment_systems->map(function (PaymentSystem $paymentSystem) {
            return $this->balance($paymentSystem);
        });
        foreach ($balances as $result) {
            $result['status'] === 'success' ? $success++ : $errors++;
        }
        $balances = $balances->map(function ($result) {
            return $result['balance'];
        });
        return [
            'errors'    => $errors,
            'success'   => $success,
            'balances'  => $balances
        ];
    }
}
