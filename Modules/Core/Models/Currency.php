<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Core\Models\Currency
 *
 * @property int $id
 * @property string $name
 * @property string $symbol
 * @property string $code
 * @property string $code_iso
 * @property float $rate
 * @property int $format_precision
 * @property bool $auto_update_rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Currency onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereAutoUpdateRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereCodeIso($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereFormatPrecision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereSymbol($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Currency withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Currency withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\CurrencyRate[] $rates
 * @property-read int|null $rates_count
 * @property string $type
 * @property string|null $update_provider
 * @property-read mixed $can_update_rate
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Currency whereUpdateProvider($value)
 */
class Currency extends Model
{
    use SoftDeletes;

    public $table = 'currencies';

    protected $dates = ['deleted_at'];

    const TYPE_FIAT = 'fiat';
    const TYPE_CRYPTO = 'crypto';

    public $fillable = [
        'name',
        'symbol',
        'code',
        'code_iso',
        'type',
        'auto_update_rate',
        'format_precision',
        'update_provider',
    ];

    protected $appends = [
        'can_update_rate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'name'              => 'string',
        'symbol'            => 'string',
        'code'              => 'string',
        'code_iso'          => 'string',
        'type'              => 'string',
        'auto_update_rate'  => 'boolean',
        'update_provider'   => 'string',
    ];

    public function getCanUpdateRateAttribute()
    {
        return $this->update_provider && app('currency-updater')->hasProvider($this->update_provider);
    }

    public function rates()
    {
        return $this->hasMany(app('zengine')->modelClass('CurrencyRate'), 'currency_id');
    }
}
