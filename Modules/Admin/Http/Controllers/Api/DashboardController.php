<?php


namespace Modules\Admin\Http\Controllers\Api;

use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\Operation;
use Modules\Core\Models\User;
use Modules\Core\Models\Wallet;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;

class DashboardController extends Controller
{
    /**
     * @var CurrencyServiceInterface
     */
    private $currencyService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->currencyService = app('currencies');
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return array
     */
    public function index()
    {
        $stats = [];
        $stats['plans'] = $this->plansStat();
        $stats['payments'] = $this->paymentsStat();
        $stats['balances'] = $this->balances();
        $stats['balances_type'] = setting('balances_type');
        $stats['time'] = $this->timeStats();
        $stats['daily'] = $this->dailyStats();
        $stats['investors'] = $this->investors();
        $stats['events'] = $this->lastEvents();
        $stats['waiting_refills'] = $this->waitingOperations(app('zengine')->modelClass('Operation')::TYPE_USER_REFILL);
        $stats['top_refs'] = $this->topRefs();
        $stats['time_refs'] = $this->timeRefs();
        return $stats;
    }

    public function user()
    {
        return \Auth::user();
    }

    protected function plansStat()
    {
        $plans = collect();
        $deposits = app('zengine')->model('Deposit')->with(['plan', 'currency'])->where('left_accruals', '>', 0)->get();
        foreach ($deposits as $deposit) {
            if (!isset($plans[$deposit->plan->name])) {
                $plans[$deposit->plan->name] = 0;
            }
            $plans[$deposit->plan->name] += $this->currencyService->convertToDefault($deposit->amount, $deposit->currency);
        }
        $others = $plans->sortByDesc(static function ($v) {
            return $v;
        })->splice(4);
        $data = $plans;
        $data[''] = $others->sum() ?: 0;
        return $data;
    }

    protected function paymentsStat()
    {
        $payments_operations = app('zengine')->model('Operation')->with(['payment_system', 'currency'])->where(function (Builder $query) {
            return $query->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)->orWhere('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW);
        })->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->whereNotNull('payment_system_id')->get();
        if ($payments_operations->count()) {
            $payments = collect();
            foreach ($payments_operations as $payment_operation) {
                $amount = $this->currencyService->convertToDefault($payment_operation->amount, $payment_operation->currency);
                $payments[$payment_operation->payment_system->name] = $payments->has($payment_operation->payment_system->name) ?
                    $payments[$payment_operation->payment_system->name] + $amount :
                    $amount;
            }
            $payments = $payments->sortByDesc(function ($v) {
                return $v;
            });
            $others = $payments->splice(3);
            $payments[''] = $others->sum();
        } else {
            $payments = app('zengine')->model('PaymentSystem')->limit(3)->pluck('balance', 'name')->map(function ($v) {
                return 0;
            });
            $payments[''] = 1;
        }
        return $payments;
    }

    protected function balances()
    {
        $wallets = app('zengine')->model('Wallet')->with('payment_system.currency')->get();
        if ($wallets->count()) {
            $balances = collect();
            foreach ($wallets as $wallet) {
                /** @var Wallet $wallet */
                $balance = $this->currencyService->convertToDefault($wallet->balance, $wallet->payment_system->currency);
                if (!$balances->has($wallet->payment_system->name)) {
                    $balances[$wallet->payment_system->name] = 0;
                }
                $balances[$wallet->payment_system->name] += $balance;
            }
        } else {
            $balances = app('zengine')->model('PaymentSystem')->get()->keyBy('name');
            $balances->map(static function ($v) {
                return 0;
            });
        }
        $balances = $balances->sortByDesc(static function ($v) {
            return $v;
        });
        $others = $balances->splice(4);
        $data = $balances;
        $data[''] = $others->sum() ?: 0;
        return $data;
    }

    protected function timeStats()
    {
        $stats = [];
        $start_date = now()->subMonths(7);
        $operations = app('zengine')->model('Operation')->with('currency')->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->where('created_at', '>', $start_date)
            ->get();
        $stats['refills'] = $this->timeOperations($operations);
        $operations = app('zengine')->model('Operation')->with('currency')->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->where('created_at', '>', $start_date)
            ->get();
        $stats['withdraws'] = $this->timeOperations($operations);
        $users = app('zengine')->model('User')->where('created_at', '>', $start_date)->get();
        $stats['users'] = $this->timeUsers($users);
        return $stats;
    }

    public function timeOperations(Collection $all_operations)
    {
        $stats = [
            'days'     => collect(),
            'two_days' => collect(),
            'weeks'    => collect(),
            'months'   => collect(),
        ];
        //$start_date
        for ($i = 0; $i < 7; $i++) {
            $start_date = now()->subDays($i)->startOfDay();
            $end_date = now()->subDays($i)->endOfDay();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $start_date->toDateString();
            $stats['days'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['days'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['days'] = $stats['days']->reverse();
        for ($i = 1; $i <= 7; $i++) {
            $start_date = now()->subDays($i * 2)->startOfDay();
            $end_date = now()->subDays($i * 2 - 2)->endOfDay();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $end_date->toDateString();
            $stats['two_days'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['two_days'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['two_days'] = $stats['two_days']->reverse();
        for ($i = 0; $i < 7; $i++) {
            $start_date = now()->subWeeks($i)->startOfWeek();
            $end_date = now()->subWeeks($i)->endOfWeek();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $end_date->toDateString();
            $stats['weeks'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['weeks'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['weeks'] = $stats['weeks']->reverse();
        for ($i = 0; $i < 7; $i++) {
            $start_date = now()->subMonths($i)->startOfMonth();
            $end_date = now()->subMonths($i)->endOfMonth();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $start_date->toDateString();
            $stats['months'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['months'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['months'] = $stats['months']->reverse();

        return $stats;
    }

    public function timeUsers(Collection $users)
    {
        $stats = [
            'days'     => collect(),
            'two_days' => collect(),
            'weeks'    => collect(),
            'months'   => collect(),
        ];
        //$start_date
        for ($i = 0; $i < 7; $i++) {
            $start_date = now()->subDays($i)->startOfDay();
            $end_date = now()->subDays($i)->endOfDay();
            $current = $users->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $start_date->toDateString();
            $stats['days'][$key] = $current->count();
        }
        $stats['days'] = $stats['days']->reverse();
        for ($i = 1; $i <= 7; $i++) {
            $start_date = now()->subDays($i * 2)->startOfDay();
            $end_date = now()->subDays($i * 2 - 2)->endOfDay();
            $current = $users->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $end_date->toDateString();
            $stats['two_days'][$key] = $current->count();
        }
        $stats['two_days'] = $stats['two_days']->reverse();
        for ($i = 0; $i < 7; $i++) {
            $start_date = now()->subWeeks($i)->startOfWeek();
            $end_date = now()->subWeeks($i)->endOfWeek();
            $current = $users->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $end_date->toDateString();
            $stats['weeks'][$key] = $current->count();
        }
        $stats['weeks'] = $stats['weeks']->reverse();
        for ($i = 0; $i < 7; $i++) {
            $start_date = now()->subMonths($i)->startOfMonth();
            $end_date = now()->subMonths($i)->endOfMonth();
            $current = $users->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $start_date->toDateString();
            $stats['months'][$key] = $current->count();
        }
        $stats['months'] = $stats['months']->reverse();

        return $stats;
    }

    public function dailyStats()
    {
        $start_date = now()->subDays(10);
        $operations = app('zengine')->model('Operation')->with('currency')->whereIn('type', [app('zengine')->modelClass('Operation')::TYPE_USER_REFILL, app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW])
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->where('created_at', '>', $start_date)
            ->get();
        $days = collect();
        for ($i = 0; $i < 10; $i++) {
            $daily = [
                'refills'           => 0,
                'withdraws'         => 0,
                'profit'            => 0,
                'profit_percent'    => 0,
            ];
            $start_date = now()->subDays($i)->startOfDay();
            $end_date = now()->subDays($i)->endOfDay();
            $current_refill = $operations->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $daily['refills'] = $current_refill->reduce(function ($carry, Operation $operation) {
                return $carry + round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            }) ?? 0;
            $current_withdraw = $operations->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $daily['withdraws'] = $current_withdraw->reduce(function ($carry, Operation $operation) {
                return $carry + round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            }) ?? 0;
            $daily['profit'] = $daily['refills'] - $daily['withdraws'];
            if ($daily['withdraws'] == 0 && $daily['refills'] == 0) {
                $daily['profit_percent'] = 0;
            } elseif ($daily['withdraws'] == 0) {
                $daily['profit_percent'] = 100;
            } elseif ($daily['refills'] == 0) {
                $daily['profit_percent'] = -100;
            } else {
                $daily['profit_percent'] = ($daily['refills'] / $daily['withdraws'] - 1) * 100;
            }

            $days->push($daily);
        }
        return $days;
    }

    public function investors()
    {
        return [
            'active' => app('zengine')->model('User')->whereHas('deposits')->count(),
            'total'  => app('zengine')->model('User')->count()
        ];
    }

    public function lastEvents()
    {
        $refills = app('zengine')->model('Operation')->with('currency')->latest()->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->limit(5)->get();
        $withdraws = app('zengine')->model('Operation')->with('currency')->latest()->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->limit(5)->get();
        $deposits = app('zengine')->model('Deposit')->with('currency')->latest()->limit(5)->get();
        $users = app('zengine')->model('User')->latest()->limit(5)->get();
        $referrals = app('zengine')->model('Operation')->with('currency')->latest()->where('type', app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY)->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->limit(5)->get();
        $events = collect()->merge($refills)->merge($withdraws)->merge($deposits)->merge($users)->merge($referrals)->sortByDesc('created_at');
        $events = $events->map(function ($event) {
            if ($event instanceof Operation) {
                if ($event->type === app('zengine')->modelClass('Operation')::TYPE_USER_REFILL) {
                    $event->event_type = 'refills';
                } elseif ($event->type === app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY) {
                    $event->event_type = 'ref_pays';
                } elseif ($event->type === app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW) {
                    $event->event_type = 'withdraws';
                }
            } elseif ($event instanceof Deposit) {
                $event->event_type = 'deposits';
            } elseif ($event instanceof User) {
                $event->event_type = 'registrations';
            }
            return $event;
        });
        return $events->values();
    }

    public function waitingOperations($type)
    {
        $currencyService = $this->currencyService;
        $waiting_operations = app('zengine')->model('Operation')->with('currency')
            ->select(['currency_id', 'amount'])
            ->where('type', $type)
            ->whereIn('status', [app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS, app('zengine')->modelClass('Operation')::STATUS_CREATED])->get();
        return $waiting_operations->reduce(static function ($carry, Operation $operation) use ($currencyService) {
            return $carry + ($operation->currency_id === $currencyService->getDefaultCurrency()->id ? $operation->amount : $currencyService->convertToDefault($operation->amount, $operation->currency));
        }, 0);
    }

    public function topRefs()
    {
        return app('zengine')->model('User')->with('currency')
            ->orderByDesc('referrals_earned')
            ->limit(3)
            ->get()->map(static function (Model $model) {
                return $model->append('referrals_count');
            });
    }

    public function timeRefs()
    {
        $start_date = now()->subMonths(7);
        $operations = app('zengine')->model('Operation')->with('currency')->where('type', app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->where('created_at', '>', $start_date)
            ->get();
        $all_operations = app('zengine')->model('Operation')->with('currency')->where('type', app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->get();

        $operations = $this->timeOperations($operations);
        $operations['total'] = $all_operations->reduce(function ($carry, Operation $operation) {
            return $carry + round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
        }) ?? 0;
        return $operations;
    }

    protected function estimated_accruals(Carbon $to)
    {
        $currencies = app('currencies');
        $deposits = app('zengine')->model('Deposit')->with('plan')->where('left_accruals', '>', 0)->get();
        $diff_days = $to->diffInDays(now(), 1);
        $diff_hours = $to->diffInHours(now(), 1);
        $estimated = 0;
        foreach ($deposits as $deposit) {
            /** @var Deposit $deposit */
            if ($deposit->plan->type === app('zengine')->modelClass('Plan')::TYPE_DAILY) {
                $accrual = ($deposit->amount / 100) * $deposit->plan->percent;
                $estimated += $currencies->convertToDefault($accrual * min($deposit->left_accruals, $diff_days), $deposit->currency);
            } elseif ($deposit->plan->type === app('zengine')->modelClass('Plan')::TYPE_HOURLY) {
                $accrual = ($deposit->amount / 100) * $deposit->plan->percent;
                $estimated += $currencies->convertToDefault($accrual * min($deposit->left_accruals, $diff_hours), $deposit->currency);
            }
        }
        return $estimated;
    }

    public function short_stats()
    {
        $currencyService = $this->currencyService;
        $stats = [];
        $stats['daily_accruals'] = round($this->estimated_accruals(now()->addDay()), 2);
        $stats['users_balances'] = round(app('zengine')->model('User')->with('currency')->select(['currency_id', 'balance'])->get()->reduce(static function ($carry, User $user) use ($currencyService) {
            return $carry + ($user->currency_id === $currencyService->getDefaultCurrency()->id ? $user->balance : $currencyService->convertToDefault($user->balance, $user->currency));
        }, 0), 2);
        $stats['deposits_sum'] = round(app('zengine')->model('Deposit')->with('currency')->select(['currency_id', 'amount'])->get()->reduce(static function ($carry, Deposit $deposit) use ($currencyService) {
            return $carry + ($deposit->currency_id === $currencyService->getDefaultCurrency()->id ? $deposit->amount : $currencyService->convertToDefault($deposit->amount, $deposit->currency));
        }, 0), 2);
        $stats['online_users'] = (int) Cache::get('online-users', 0);
        $stats['waiting_withdraws'] = round($this->waitingOperations(app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW), 2);
        $stats['day_online'] = app('zengine')->model('User')->where('last_online_at', '>=', now()->startOfDay())->count();
        $stats['week_online'] = app('zengine')->model('User')->where('last_online_at', '>=', now()->subWeek())->count();
        $operations = app('zengine')->model('Operation')->with('currency')->select(['currency_id', 'amount'])
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->get();
        $refills = $operations->reduce(static function ($carry, Operation $operation) use ($currencyService) {
            return $carry + ($operation->currency_id === $currencyService->getDefaultCurrency()->id ? $operation->amount : $currencyService->convertToDefault($operation->amount, $operation->currency));
        }, 0);
        $operations = app('zengine')->model('Operation')->with('currency')->select(['currency_id', 'amount'])
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
            ->whereIn('status', [
                app('zengine')->modelClass('Operation')::STATUS_SUCCESS,
                app('zengine')->modelClass('Operation')::STATUS_CREATED,
                app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS
            ])->get();
        $withdraws = $operations->reduce(static function ($carry, Operation $operation) use ($currencyService) {
            return $carry + ($operation->currency_id === $currencyService->getDefaultCurrency()->id ? $operation->amount : $currencyService->convertToDefault($operation->amount, $operation->currency));
        }, 0);
        $stats['left_to_live'] = (int) (($refills - $withdraws) / ($stats['daily_accruals'] ?: 1));

        return $stats;
    }
}
