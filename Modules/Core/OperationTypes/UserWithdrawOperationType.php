<?php


namespace Modules\Core\OperationTypes;


use Modules\Core\Models\Operation;
use Modules\Core\Payments\ManualPaymentProvider;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;
use Modules\Core\Services\Contracts\UserServiceInterface;

class UserWithdrawOperationType extends OperationType
{
    public function canCreate(Operation $operation) : bool
    {
        return $operation->payment_system_id;
    }

    public function canCancel(Operation $operation) : bool
    {
        return app('zengine')->modelClass('Operation')::STATUS_CREATED === $operation->status;
    }

    public function canReject(Operation $operation) : bool
    {
        return app('zengine')->modelClass('Operation')::STATUS_CREATED === $operation->status;
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws \Modules\Core\Exceptions\BalanceException
     * @throws \Modules\Core\Exceptions\BalanceTypeNotFindException
     * @throws \Modules\Core\Exceptions\CanPayWalletException
     * @throws \Modules\Core\Exceptions\OperationException
     * @throws \Modules\Core\Exceptions\OperationWalletInvalidException
     * @throws \Modules\Core\Exceptions\UserBalancesNotDividedException
     */
    public function create(Operation $operation) : Operation
    {
        $operation = parent::create($operation);

        if( !( isset($operation->payment_system_data['force']) && $operation->payment_system_data['force'] ) ){
            app(UserServiceInterface::class)->canPayFrom($operation->user, $operation->amount, $operation->currency, $operation->wallet);
            app(UserServiceInterface::class)->takeMoney($operation);
        }

        /* @var $paymentProvider ManualPaymentProvider */
        $paymentProvider = $operation->payment_system->getProvider();
        if (is_array($operation->payment_system->credentials) && count($operation->payment_system->credentials)) {
            $paymentProvider->setCredentials($operation->payment_system->credentials);
        }

        return $paymentProvider->makeWithdraw($operation);
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws \Modules\Core\Exceptions\BalanceTypeNotFindException
     * @throws \Modules\Core\Exceptions\OperationWalletInvalidException
     */
    public function cancel(Operation $operation) : Operation
    {
        app(UserServiceInterface::class)->putMoney($operation);
        return parent::cancel($operation);
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws \Modules\Core\Exceptions\BalanceTypeNotFindException
     * @throws \Modules\Core\Exceptions\OperationWalletInvalidException
     */
    public function reject(Operation $operation) : Operation
    {
        app(UserServiceInterface::class)->putMoney($operation);
        return parent::reject($operation);
    }

    public function success(Operation $operation) : Operation
    {
        $sum = app(CurrencyServiceInterface::class)->convertToDefault($operation->amount, $operation->currency) + (float) app('zengine')->getStat('WITHDRAWS_SUCCESS_SUM', 0.00) ;
        app('zengine')->setStat('WITHDRAWS_SUCCESS_SUM', $sum);
        return parent::success($operation);
    }
}
