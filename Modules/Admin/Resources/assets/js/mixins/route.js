export default {
    methods: {
        route(name, params = {}){
            return {name: name, params: params};
        }
    }
}
