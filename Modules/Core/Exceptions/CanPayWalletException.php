<?php


namespace Modules\Core\Exceptions;

class CanPayWalletException extends \Exception
{
    public function __construct($message = "CanPay Wallet null or invalid")
    {
        parent::__construct($message, 500);
    }
}
