@extends('layouts.cabinet')

@section('body')
    @include('flash::message')

    <div class="clearfix"></div>
    <h1>@lang('cabinet/withdraw.title')</h1>
    @if(request('status', $status))
        @if(request('status', $status) === 'success')
            <div class="alert alert-success" role="alert">
                @lang('cabinet/withdraw.alerts.success')
            </div>
        @elseif(request('status', $status) === 'error')
            <div class="alert alert-danger" role="alert">
                @lang('cabinet/withdraw.alerts.error')
            </div>
        @elseif(request('status', $status) === 'in_progress')
            <div class="alert alert-danger" role="alert">
                @lang('cabinet/withdraw.alerts.in_progress')
            </div>
        @endif
    @endif
    @can('unlocked')
        <form action="{{ route('cabinet.withdraw.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="amount_field">@lang('cabinet/withdraw.amount')</label>
                <input type="number" class="form-control" name="amount" id="amount_field" step="0.00001">
            </div>
            <div class="form-group">
                <label for="currency_id_field">@lang('cabinet/withdraw.currency')</label>
                <select name="currency_id" id="currency_id_field" class="form-control">
                    @foreach($currencies as $currency)
                        <option value="{{ $currency->id }}" @if($currency->id === (int) old('currency_id'))selected @endif>{{ $currency->code_iso }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="payment_system_field">@lang('cabinet/withdraw.payment_system')</label>
                <select name="payment_system" id="payment_system_field" class="form-control">
                    @foreach($payment_systems as $payment_system)
                        @php
                            /** @var \Modules\Core\Models\PaymentSystem $payment_system **/
                        @endphp
                        <option value="{{ $payment_system->id }}" @if($payment_system->id === (int) old('payment_system'))selected @endif
                            @if($payment_system->need_wallet && (!isset($wallets[$payment_system->id]) || !$wallets[$payment_system->id]->wallet))
                                disabled
                            @elseif(setting('balances_type') === 'divided' && (!isset($wallets[$payment_system->id]) || $wallets[$payment_system->id]->balance == 0))
                                disabled
                            @endif
                        >
                            {{ $payment_system->name }} @if(isset($wallets[$payment_system->id]) && setting('balances_type') === 'divided') {{ $wallets[$payment_system->id]->balance }} @endif</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-primary" type="submit">@lang('cabinet/withdraw.submit')</button>
        </form>
    @else
        <div class="alert alert-danger" role="alert">
            @lang('ui.account_locked')
        </div>
        <button class="btn btn-primary" type="button" @click="unlock_modal = true">@lang('ui.unlock')</button>
    @endcan
@endsection
