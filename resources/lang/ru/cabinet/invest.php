<?php

return [
    'title'     => 'Инвестировать',
    'form'      => [
        'currency'  => 'Валюта',
        'amount'    => 'Сумма',
        'wallet'    => 'Кошелек',
        'submit'    => 'Инвестировать',
    ],
    'plan' => [
        'amount'                        => 'Сумма',
        'percent'                       => 'Процент',
        'charge_weekends'               => 'Начисления в выходные',
        'limit'                         => 'Лимит',
        'additional_referral_percent'   => 'Дополнительный процент рефераллам',
    ]
];
