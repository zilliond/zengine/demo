<?php

use Modules\Core\Models\Operation;

return [
    'filter' => [
        'title'             => 'Поиск платежа:',
        'login'             => 'Логин:',
        'amount_min'        => 'Сумма - от:',
        'amount_max'        => 'Сумма - до:',
        'transaction_id'    => 'Номер транзакции:',
    ],
    'total_count'           => 'Общее кол-во транзакций:',
    'total_amount'          => 'Общая сумма транзакций:',
    'table'                 => [
        'title'             => 'Статус транзакции:',
        'date'              => 'Дата',
        'login'             => 'Логин',
        'amount'            => 'Сумма',
        'transaction_id'    => 'Счет',
        'payment_system'    => 'Система',
        'status'            => 'Статус',
        'statuses'          => [
            'created'       => 'В процессе',
            'success'       => 'Выполненые',
            'in_progress'   => 'В ожидании',
            'canceled'      => 'Отмененные',
            'rejected'      => 'Отмененные администрацией',
            'all'           => 'Все',
        ]
    ],
    'statuses' => [
        app('zengine')->modelClass('Operation')::STATUS_CREATED       => 'Создана',
        app('zengine')->modelClass('Operation')::STATUS_CANCELED      => 'Отменен',
        app('zengine')->modelClass('Operation')::STATUS_REJECTED      => 'Отклонен',
        app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS   => 'В процессе',
        app('zengine')->modelClass('Operation')::STATUS_SUCCESS       => 'Успешно',
    ],
];
