const state = {
    user: {
        id: window.system.user.id
    },
    online: true
};

const getters = {
    user: state => state.user,
    user_id: state => state.user.id,
    user_login: state => state.user.login,
    online: state => state.online,

    axios: (state, getters, rootState, rootGetters) => rootGetters.axios
};

const actions = {
    setUser({commit}, user) {
        commit('SET_USER', user)
    },
    setAuthToken({commit, dispatch}, token) {
        dispatch('setAuthToken', token, {root: true});
    },
    toggleOnline({commit}){
        console.log('TOGGLE_ONLINE');
        commit('TOGGLE_ONLINE');
    },
    setOnline({commit}, value){
        console.log('SET_ONLINE '+value);
        commit('SET_ONLINE', value);
    },
    setUserLocale({commit}, value){
        console.log('SET_USER_LOCALE', value);
        commit('SET_USER_LOCALE', value);
    },
    getUser({commit, getters, dispatch, rootGetters}){
        getters.axios
            .get('/user')
            .then(resp => resp.data)
            .then(user => {
                commit('SET_USER', user);
                if(!rootGetters.locale){
                    dispatch('setLocale', user.locale, {root: true});
                }
            });
    }
};

const mutations = {
    SET_USER(state, user){
        state.user = user;
    },
    TOGGLE_ONLINE(state){
        state.online = !state.online;
    },
    SET_ONLINE(state, value){
        state.online = value;
    },
    SET_USER_LOCALE(state, locale){
        state.user.locale = locale;
    }
};

const auth = {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};

export default auth;
