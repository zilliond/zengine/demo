<?php

return [
    'title'         => 'Deposits',
    'plan'          => 'Plan',
    'wallet'        => 'Wallet',
    'amount'        => 'Amount',
    'accruals'      => 'Accruals',
    'accrued'       => 'Accrued',
    'status'        => 'Status',
    'next_accrual'  => 'Next accrual',
    'last_accrual'  => 'Last accrual',
];
