<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="https://getbootstrap.com/docs/4.1/assets/img/favicons/favicon.ico">
    <title>@section('title')Larahyip @show</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/starter-template/">

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    @yield('css')
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="/">Larahyip</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                <a class="nav-link" href="/">@lang('ui.menu.home') <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{ Request::is('cabinet') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('cabinet.index') }}">@lang('ui.menu.cabinet')</a>
            </li>
            <li class="nav-item {{ Request::is('news') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('news') }}">@lang('ui.menu.news')</a>
            </li>
            <li class="nav-item {{ Request::is('faq') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('faq') }}">@lang('ui.menu.faq')</a>
            </li>
        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ config('app.supportedLocales.'. app()->getLocale() . '.name') }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    @foreach(config('app.supportedLocales') as $code => $locale)
                        <a class="dropdown-item @if(app()->getLocale() == $code) active @endif"
                           href="?locale={{ $code }}">
                            {{ $locale['name'] ?? $code }}
                        </a>
                    @endforeach
                </div>
            </li>
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">@lang('ui.menu.login')</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">@lang('ui.menu.register')</a>
                    </li>
                @endif
            @else

                <li class="nav-item">
                    <span class="nav-link">
                        {{ Auth::user()->balance }}{{ Auth::user()->currency->symbol }}
                    </span>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @can('admin_panel')
                            <a class="dropdown-item" href="{{ route('admin.spa') }}">
                                @lang('ui.menu.dashboard')
                            </a>
                        @endif
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            @lang('ui.menu.logout')
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">@csrf</form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</nav>

<main role="main" class="@section('containerClass')container @show" id="main">

    @yield('content')

</main>

<footer class="footer">
    <div class="container">
        <a href="//freekassa.ru/"><img src="//www.free-kassa.ru/img/fk_btn/9.png" title="Приём оплаты на сайте картами"></a>
        <a href="https://www.fkwallet.ru"><img src="https://www.fkwallet.ru/assets/2017/images/btns/icon_wallet1.png"
                                               title="Прием криптовалют"></a>
    </div>
</footer>
<script>
    window.system = {!!
        json_encode([
            'user_id' => \Auth::id() ?? 0,
            'csrfToken' => csrf_token(),
            'sockets' => setting('sockets'),
            'locale' => App::getLocale()
        ]) !!};
</script>
<script src="{{ mix('js/app.js') }}"></script>
@yield('js')
</body>
</html>
