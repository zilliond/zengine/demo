<?php

use Modules\Core\Models\Deposit;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepositsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('amount', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'));
            $table->decimal('profit', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('plan_id');
            $table->unsignedBigInteger('user_id');
            if (setting('balances_type') === 'shared') {
                $table->unsignedBigInteger('wallet_id')->nullable();
            } elseif (setting('balances_type') === 'divided') {
                $table->unsignedBigInteger('wallet_id');
            }
            $table->string('status')->default(app('zengine')->modelClass('Deposit')::STATUS_OPEN);
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('wallet_id')->references('id')->on('wallets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deposits');
    }
}
