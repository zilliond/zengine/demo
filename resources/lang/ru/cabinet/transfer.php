<?php

return [
    'title'             => 'Перевести',
    'receiver_login'    => 'Логин получателя',
    'amount'            => 'Сумма',
    'currency'          => 'Валюта',
    'wallet'            => 'Кошелек',
    'submit'            => 'Перевести',
];
