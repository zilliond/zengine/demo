@extends('layouts.cabinet')

@section('body')
    <h1>@lang('cabinet/invest.title')</h1>
    <invest-form
        action_url="{{ route('cabinet.deposits.store') }}" redirect_url="{{ route('cabinet.deposits') }}"
        balance_type="{{ setting('balances_type') }}" :wallets="{{ $wallets ? json_encode($wallets) : '' }}"
        :plans='{{ json_encode($plans) }}' :currencies='{{ json_encode($currencies) }}' :old='{{ json_encode(old('form')) }}'></invest-form>
@endsection
