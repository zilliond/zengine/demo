<?php

return [
    'change_password' => [
        'title'                     => 'Смена пароля',
        'old_password'              => 'Старый пароль',
        'new_password'              => 'Новый пароль',
        'new_password_confirmation' => 'Повторите новый пароль',
    ],
    '2fa' => [
        'title'             => 'Двухфакторная аутентификация',
        'code'              => 'Код',
        'not_support_qr'    => 'Если ваше 2FA мобильное приложение не поддеживает QR, введите следующий номер: <code>:SECRET</code>',
    ],
    'pin' => [
        'title'             => 'PIN код',
        'code'              => 'Код',
    ]
];
