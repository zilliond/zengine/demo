<?php


namespace Modules\Core\Services;

use Modules\Core\Services\Contracts\SocialAuthServiceInterface;
use Socialite;
use SocialiteProviders\Manager\Config;

class SocialAuthService implements SocialAuthServiceInterface
{
    /**
     * @param $provider
     * @return mixed
     */
    public function driver($provider)
    {
        $provider_config = $this->getConfig($provider);
        $config = new Config(
            $provider_config['client_id'],
            $provider_config['client_secret'],
            $provider_config['redirect']
        );
        return Socialite::with($provider)->setConfig($config);
    }

    public function getAvailableProviders() : array
    {
        return config('settings.social_providers');
    }

    public function getActiveProviders() : array
    {
        $available = collect($this->getAvailableProviders());
        return $available->filter(static function ($provider) {
            return (bool) setting("social_auth.{$provider}_enabled");
        })->toArray();
    }

    protected function getConfig($provider) : array
    {
        $setting_prefix = "social_auth.{$provider}_";
        return [
            'client_id'     => setting($setting_prefix . 'client_id'),
            'identifier'    => setting($setting_prefix . 'client_id'),
            'client_secret' => setting($setting_prefix . 'client_secret'),
            'secret'        => setting($setting_prefix . 'client_secret'),
            'redirect'      => $this->getRedirectUrl($provider),
            'callback_uri'  => $this->getRedirectUrl($provider),
        ];
    }

    protected function getRedirectUrl($provider) : string
    {
        return \App::make('url')->to("/login/{$provider}/callback");
    }
}
