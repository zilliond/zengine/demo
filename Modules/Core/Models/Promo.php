<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Promo
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property float $cost
 * @property float $deposit
 * @property \Illuminate\Support\Carbon|null $posted_at
 * @property \Illuminate\Support\Carbon|null $ended_at
 * @property string|null $link
 * @property string|null $user_nick
 * @property int|null $user_id
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $invested
 * @property-read mixed $profit
 * @property-read mixed $profit_percent
 * @property-read \Modules\Core\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereDeposit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo wherePostedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Promo whereUserNick($value)
 * @mixin \Eloquent
 * @property-read mixed $income
 * @property-read mixed $duration_days
 * @property-read mixed $structure
 * @property-read float $withdraws
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Promo type($type = '')
 * @property int $deposit_included
 * @method static Builder|Promo whereDepositIncluded($value)
 */
class Promo extends Model
{
    public const TYPE_MONITORING = 'monitoring';
    public const TYPE_BLOG = 'blog';
    public const TYPE_BANNER = 'banner';
    public const TYPE_VIDEO = 'video';

    protected $fillable = [
        'type',
        'name',
        'cost',
        'deposit',
        'deposit_included',
        'posted_at',
        'ended_at',
        'link',
        'user_nick',
        'user_id',
        'description',
    ];

    protected $casts = [
        'posted_at' => 'datetime',
        'ended_at'  => 'datetime',
    ];

    protected $appends = [
        'invested',
        'income',
        'profit',
        'profit_percent',
        'withdraws',
        'structure',
        'duration_days',
    ];

    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }

    /**
     * @return float
     */
    public function getInvestedAttribute() : float
    {
        return $this->deposit_included ? (float) $this->cost : (float) $this->cost + $this->deposit;
    }
    /**
     * @return float
     */
    public function getIncomeAttribute() : float
    {
        if ($this->user) {
            $currencyService = app('currencies');
            $users = app('zengine')->model('Referral')->where('user_id', $this->user_id)->pluck('referral_id')->push($this->user_id);
            return app('zengine')->model('Operation')->with('currency')->whereIn('user_id', $users)
                ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
                ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
                ->select(['currency_id', 'amount'])
                ->get()
                ->reduce(static function ($carry, Operation $operation) use ($currencyService) {
                    return $carry + $currencyService->convertToDefault($operation->amount, $operation->currency);
                }, 0);
        } else {
            return 0.00;
        }
    }

    /**
     * @return float
     */
    public function getWithdrawsAttribute() : float
    {
        if ($this->user) {
            $currencyService = app('currencies');
            $users = app('zengine')->model('Referral')->where('user_id', $this->user_id)->pluck('referral_id')->push($this->user_id);
            return app('zengine')->model('Operation')->with('currency')->whereIn('user_id', $users)
                ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
                ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
                ->select(['currency_id', 'amount'])
                ->get()
                ->reduce(static function ($carry, Operation $operation) use ($currencyService) {
                    return $carry + $currencyService->convertToDefault($operation->amount, $operation->currency);
                }, 0);
        } else {
            return 0.00;
        }
    }


    public function getDurationDaysAttribute() : int
    {
        if ($this->ended_at && $this->ended_at->lessThanOrEqualTo(now())) {
            return $this->ended_at->diffInDays($this->posted_at);
        } else {
            return now()->diffInDays($this->posted_at);
        }
    }

    public function getStructureAttribute() : int
    {
        if ($this->user) {
            return 1 + app('zengine')->model('Referral')->where('user_id', $this->user_id)->count();
        } else {
            return 0;
        }
    }

    /**
     * @return float
     */
    public function getProfitAttribute() : float
    {
        return (float) $this->income - $this->invested - $this->withdraws;
    }

    /**
     * @return float
     */
    public function getProfitPercentAttribute() : float
    {
        return round($this->invested !== 0.00 ? (float) ($this->profit / $this->invested) * 100 : 100, 2);
    }

    public function scopeType($query, $type = '')
    {
        return $query->when($type, function (Builder $builder) use ($type) {
            return $builder->where('type', $type);
        });
    }
}
