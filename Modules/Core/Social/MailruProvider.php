<?php


namespace Modules\Core\Social;

use SocialiteProviders\Manager\OAuth2\AbstractProvider;
use SocialiteProviders\Manager\OAuth2\User;

class MailruProvider extends AbstractProvider
{
    /**
     * Unique Provider Identifier.
     */
    const IDENTIFIER = 'MAILRU';

    /**
     * {@inheritdoc}
     */
    protected $scopes = ['userinfo'];

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://connect.mail.ru/oauth/authorize', $state);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return 'https://connect.mail.ru/oauth/token';
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        $sign = md5("app_id={$this->clientId}method=users.getInfosecure=1session_key={$token}{$this->clientSecret}");
        $params = [
            'method'       => 'users.getInfo',
            'secure'       => '1',
            'app_id'       => $this->clientId,
            'session_key'  => $token,
            'sig'          => $sign
        ];
        $params = http_build_query($params);

        $response = $this->getHttpClient()->get('http://www.appsmail.ru/platform/api?'.$params);

        $resp = json_decode($response->getBody(), true);
        \Debugbar::info($resp);
        return $resp;
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        $user = $user[0];
        $parts = explode("@", $user['email']);
        $mail_username = $parts[0];
        $nickname = isset($user['nick']) && $user['nick'] ? $user['nick'] : $mail_username;
        return (new User())->setRaw($user[0])->map([
            'id'       => $user['uid'],
            'nickname' => $nickname,
            'name'     => $user['first_name'] . ' ' . $user['last_name'],
            'email'    => $user['email'],
            'avatar'   => $user['pic'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return array_merge(parent::getTokenFields($code), [
            'grant_type' => 'authorization_code',
        ]);
    }
}
