<?php

namespace Modules\Admin\Http\Controllers;

use Modules\Core\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function spa()
    {
        $token = \Auth::user()->createToken('admin_api');

        return view('admin::spa', [
            'auth_token' => $token->accessToken,
        ]);
    }
}
