<?php

namespace Modules\Admin\Console;

use Modules\Core\Models\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class MakeAdmin extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:make-admin {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make user role admin.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = app('zengine')->model('User')->findOrFail($this->argument('user'));
        if ($user->role === app('zengine')->modelClass('User')::ROLE_ADMIN) {
            $this->error('User is already admin');
            return false;
        }
        $user->role = app('zengine')->modelClass('User')::ROLE_ADMIN;
        $user->save();
        $this->info('Done!');
        return true;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['user', InputArgument::REQUIRED, 'User id.'],
        ];
    }
}
