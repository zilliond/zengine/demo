<?php


namespace Modules\Core\Exceptions;

class PaymentSystemException extends \Exception
{
    public function __construct($message = "Payment System error")
    {
        parent::__construct($message, 500);
    }
}
