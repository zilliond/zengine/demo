@if(setting('users.2fa') !== 'disabled')
    <h1>@lang('cabinet/security.2fa.title')</h1>
    @if($user->google2fa_secret)
        {!! Form::open(['route' => 'cabinet.security.2fa.disable', 'method' => 'POST']) !!}
        <div class="form-group">
            {!! Form::label('code', trans('cabinet/security.2fa.code')) !!}
            {!! Form::text('code', null, ['class' => 'form-control '.( $errors->has('old_password') ? ' code' : '' )]) !!}
        </div>
        <button class="btn btn-primary" type="submit">@lang('cabinet/ui.disable')</button>
        {!! Form::close() !!}
    @else
        {!! Form::open(['route' => 'cabinet.security.2fa.enable', 'method' => 'POST']) !!}
        <img src="{{ $image_2fa }}" class="img-fluid rounded mx-auto d-block" alt="QR code">
        <span>@lang('cabinet/security.2fa.not_support_qr', ['secret' => $secret_2fa])</span>
        {!! Form::hidden('secret', $secret_2fa) !!}
        <div class="form-group">
            {!! Form::label('code', trans('cabinet/security.2fa.code')) !!}
            {!! Form::text('code', null, ['class' => 'form-control '.( $errors->has('old_password') ? ' is-invalid' : '' )]) !!}
        </div>
        <button class="btn btn-primary" type="submit">@lang('cabinet/ui.enable')</button>
        {!! Form::close() !!}
    @endif
@endif
