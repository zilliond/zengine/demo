<?php

return [
    'title'             => 'Операции',
    'type'              => 'Тип',
    'status'            => 'Статус',
    'currency'          => 'Валюта',
    'wallet'            => 'Кошелек',
    'payment_system'    => 'Платежная система',
    'amount'            => 'Сумма',
    'deposit'           => 'Депозит',
    'created'           => 'Создано',
    'actions'           => 'Действия',
];
