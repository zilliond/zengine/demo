<?php

use Modules\Core\Http\Controllers\Auth\ForgotPasswordController;
use Modules\Core\Http\Controllers\Auth\LoginController;
use Modules\Core\Http\Controllers\Auth\RegisterController;
use Modules\Core\Http\Controllers\Auth\ResetPasswordController;
use Modules\Core\Http\Controllers\Auth\SocialController;
use Modules\Core\Http\Controllers\Auth\VerificationController;
use Modules\Core\Http\Controllers\PageController;
use Modules\Core\Http\Controllers\ZEngineController;

if (setting('general.ssl_enabled')) {
    URL::forceScheme('https');
}
// Authentication Routes...
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('register', [RegisterController::class, 'register']);

Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.update');

Route::get('email/verify', [VerificationController::class, 'show'])->name('verification.notice');
Route::get('email/verify/{id}', [VerificationController::class, 'verify'])->name('verification.verify');
Route::get('email/resend', [VerificationController::class, 'resend'])->name('verification.resend');

Route::get('login/{provider}', [SocialController::class, 'redirect'])->name('social_login');
Route::get('login/{provider}/callback', [SocialController::class, 'callback']);
Route::get('login/{provider}/logout', [SocialController::class, 'logout']);

Route::any('/paymentSystems/handle', [ZEngineController::class, 'ipn_handle'])->name('payment_systems.ipn');

Route::get('pages/{route}', [PageController::class, 'show'])->name('page');
