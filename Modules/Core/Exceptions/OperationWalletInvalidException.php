<?php


namespace Modules\Core\Exceptions;

class OperationWalletInvalidException extends \Exception
{
    public function __construct($operationId)
    {
        parent::__construct("User wallet in operation - $operationId not set or invalid", 500);
    }
}
