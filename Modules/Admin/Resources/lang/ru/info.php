<?php

return [
    'general'           => 'Общая информация:',
    'server_ip'         => 'IP сервера:',
    'uptime'            => 'Аптайм:',
    'hardware'          => 'Hardware:',
    'cpu'               => 'Процессор:',
    'threads'           => 'Количество потоков:',
    'device_model'      => 'Модель устройства или мат. платы:',
    'virtualization'    => 'Виртуализация:',
    'software'          => 'Software:',
    'arch'              => 'Архитектура:',
    'os'                => 'Операционная система:',
    'distro'            => 'Дистрибутив:',
    'kernel'            => 'Ядро:',
    'php_version'       => 'Версия PHP:',
    'webserver'         => 'Веб сервер:',
    'database_driver'   => 'Драйвер базы данных:',
    'database_version'  => 'Версия базы данных:',
];
