<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\AuthLog.
 *
 * @property int                             $id
 * @property int                             $user_id
 * @property string                          $ip
 * @property string                          $country_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Modules\Core\Models\User       $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AuthLog whereUserId($value)
 * @mixin \Eloquent
 */
class AuthLog extends Model
{
    protected $fillable = [
        'user_id', 'ip', 'country_code',
    ];

    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }
}
