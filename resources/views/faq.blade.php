@extends('layouts.app')

@section('title', 'FAQ')

@section('content')
    <div>
        <h2>FAQ</h2>
        <div class="accordion" id="faq">
            @foreach($faq_items as $item)
            <div class="card">
                <div class="card-header" id="faq_item_{{ $item->id }}">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#faq_item_{{ $item->id }}_collapse" aria-expanded="true" aria-controls="faq_item_{{ $item->id }}_collapse">
                            {{ $item->question }}
                        </button>
                    </h2>
                </div>

                <div id="faq_item_{{ $item->id }}_collapse" class="collapse" aria-labelledby="faq_item_{{ $item->id }}" data-parent="#faq">
                    <div class="card-body">
                        @markdown($item->answer)
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection
