@extends('layouts.app')

@section('title', $page->title)

@section('content')
	@markdown($page->body)
@endsection
