<?php

return [
    'title'             => 'Вывести',
    'amount'            => 'Сумма',
    'currency'          => 'Валюта',
    'payment_system'    => 'Платежная система',
    'submit'            => 'Вывести',
    'alerts'            => [
        'success'       => 'Успешно выведено',
        'error'         => 'Ошибка при выводе',
        'in_progress'   => 'Вывод обрабатывается',
    ],
];
