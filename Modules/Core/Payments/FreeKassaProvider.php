<?php


namespace Modules\Core\Payments;

use Illuminate\Http\Request;
use Log;
use Modules\Core\Exceptions;
use Modules\Core\Exceptions\OperationException;
use Modules\Core\Exceptions\PaymentSystemException;
use Modules\Core\Models\Operation;

class FreeKassaProvider extends PaymentSystemProvider
{

    /**
     * @inheritDoc
     */
    public function getName() : string
    {
        return 'FreeKassa';
    }

    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['merchant_id', 'merchant_secret']);
        $merchant = $this->getCredential('merchant_id');
        $secret = $this->getCredential('merchant_secret');
        $sign = md5($merchant.':'.$operation->amount.':'.$secret.':'.$operation->id);
        $form_data = [
            'm'     => $merchant, // Merchant id
            'oa'    => $operation->amount, // Operation amount
            'o'     => $operation->id, // Order id
            's'     => $sign, // sign
            'em'    => $operation->user->email,
        ];
        $redirect_url = 'https://www.free-kassa.ru/merchant/cash.php?'.http_build_query($form_data);
        \Log::channel('payment_systems')->info("FreeKassaProvider|makeRefill|Operation {$operation->id}: payment_url: $redirect_url");
        $operation->payment_system_data = [
            'm_amount'      => $operation->amount,
            'sign'          => $sign,
            'type'          => 'redirect',
            'redirect_url'  => $redirect_url
        ];
        $operation->save();

        return $operation;
    }

    public function canHandle(Request $request) : bool
    {
        return $request->filled('MERCHANT_ID') && $request->filled('MERCHANT_ORDER_ID');
    }

    public function handle(Request $request)
    {
        if (!in_array($request->ip(), ['136.243.38.147', '136.243.38.149', '136.243.38.150', '136.243.38.151', '136.243.38.189', '136.243.38.108'])) {
            \Log::info('FreeKassaProvider| request ip mismatch');
            throw new PaymentSystemException('FreeKassaProvider| request ip mismatch');
        }
        $this->checkCredentials(['merchant_id', 'merchant_secret_2']);
        $merchant = $this->getCredential('merchant_id');
        $secret = $this->getCredential('merchant_secret_2');
        $sign = md5($merchant.':'.$request->get('AMOUNT').':'.$secret.':'.$request->get('MERCHANT_ORDER_ID'));
        $operation = app('zengine')->model('Operation')->findOrFail($request->get('MERCHANT_ORDER_ID'));
        if (!$operation) {
            \Log::channel('payment_systems')->info("FreeKassaProvider|Handler|Operation {$request->get('MERCHANT_ORDER_ID')} not find - request", ['request' => $request->all()]);
            throw new PaymentSystemException('FreeKassaProvider| Operation not find');
        }
        if ($sign === $request->get('SIGN')) {
            \Log::channel('payment_systems')->info("FreeKassaProvider|Handler|Operation {$request->get('MERCHANT_ORDER_ID')} success");
            $this->operationService->typeByOperation($operation)->success($operation);
            return;
        }
        \Log::channel('payment_systems')->info("FreeKassaProvider| sign mismatch |Operation {$request->get('MERCHANT_ORDER_ID')}", ['request' => $request->all()]);
        throw new PaymentSystemException('FreeKassaProvider| sign mismatch');
    }

    /**
     * @return float
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws PaymentSystemException
     */
    public function getBalance() : float
    {
        $this->checkCredentials(['merchant_id', 'merchant_secret_2']);
        $merchant = $this->getCredential('merchant_id');
        $secret = $this->getCredential('merchant_secret_2');
        $sign = md5($merchant.$secret);
        $response = $this->guzzle->request('GET', 'https://www.free-kassa.ru/api.php', [ 'query' => [
            'action'        => 'get_balance',
            'type'          => 'json',
            'merchant_id'   => $merchant,
            's'             => $sign
        ]]);
        $result = json_decode((string) $response->getBody(), true);
        if (is_array($result) && $result['data']) {
            return $result['data']['balance'];
        }
        return 0;
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws OperationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws Exceptions\BalanceException
     */
    public function makeWithdraw(Operation $operation) : Operation
    {
        $this->checkCredentials(['merchant_id', 'merchant_secret_2', 'wallet_id', 'wallet_key']);
        $merchant = $this->getCredential('merchant_id');
        $secret = $this->getCredential('merchant_secret_2');
        $sign = md5($merchant.$secret);
        if (!isset($operation->payment_system_data['target'])) {
            throw new OperationException('PerfectMoneyProvider|makeWithdraw|Not set target account');
        }
        $response = $this->guzzle->request('GET', 'https://www.free-kassa.ru/api.php', [ 'query' => [
            'action'        => 'payment',
            'currency'      => 'fkw',
            'amount'        => $operation->amount,
            'merchant_id'   => $merchant,
            's'             => $sign
        ]]);
        $payment = json_decode((string) $response->getBody(), true);
        if ($payment['type'] === 'error') {
            throw new PaymentSystemException("Operation {$operation->id}|Withdraw error : {$payment['desc']}");
        }

        $wallet_id = $this->getCredential('wallet_id');
        $wallet_key = $this->getCredential('wallet_key');
        $currencyService = app('currencies');
        if ($operation->currency->code_iso === 'RUB') {
            $amount_rub = number_format($operation->amount, 2, '.', '');
        } else {
            $rub = app('zengine')->model('Currency')->where('code_iso', 'RUB')->orWhere('code', 'RUB')->firstOrFail();
            $amount_rub = number_format($currencyService->convert($operation->amount, $operation->currency, $rub), 2, '.', '');
        }
        $sign = md5(implode('', [
            $wallet_id,
            '133',
            $amount_rub,
            $operation->payment_system_data['target'],
            $wallet_key
        ]));
        //$sign = md5($wallet_id.':133:'.$amount_rub.':'.$operation->payment_system_data['target'].':'.$wallet_key);
        $response = $this->guzzle->request('POST', 'https://www.fkwallet.ru/api_v1.php', [
            'form_params' => [
                'wallet_id'     => $wallet_id,
                'purse'         => $operation->payment_system_data['target'],
                'amount'        => $amount_rub,
                'desc'          => 'Withdraw',
                'currency'      => '133',
                'sign'          => $sign,
                'action'        => 'cashout',
            ]
        ]);
        $result = json_decode((string) $response->getBody(), true);
        \Debugbar::info($result);
        if ($result['status'] === 'error') {
            throw new PaymentSystemException("Operation {$operation->id}|Withdraw error : {$result['desc']}");
        }
        $payment_id = $result['data']['payment_id'];
        $operation->payment_system_data += ['transaction_id' => $payment_id];
        $sign = md5($wallet_id.$payment_id.$wallet_key);
        $response = $this->guzzle->request('POST', 'https://www.fkwallet.ru/api_v1.php', [
            'form_params' => [
                'wallet_id'     => $wallet_id,
                'payment_id'    => $payment_id,
                'sign'          => $sign,
                'action'        => 'get_payment_status',
            ]
        ]);
        $result = json_decode((string) $response->getBody(), true);
        if (is_array($result) && is_array($result['data'])) {
            if ($result['data']['status'] === 'Completed') {
                $operation = $this->operationService->typeByOperation($operation)->success($operation);
            } elseif ($result['data']['status'] === 'In process') {
                $operation = $this->operationService->typeByOperation($operation)->inProgress($operation);
            }
        }
        Log::channel('payment_systems')->info("FreeKassaProvider|makeWithdraw|Operation {$operation->id} error", ['body' => $result]);

        $operation->save();
        return $operation;
    }

    public function getCredentialsKeys() : array
    {
        return [
            'merchant_id',
            'merchant_secret',
            'merchant_secret_2',
            'wallet_id',
            'wallet_key',
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'merchant_id'       => 'Merchant id',
            'merchant_secret'   => 'Merchant secret',
            'merchant_secret_2' => 'Merchant secret 2',
            'wallet_id'         => 'Wallet id',
            'wallet_key'        => 'Wallet key',
        ];
    }
}
