<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccrualsDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->dateTime('next_accrual_at')->nullable()->after('user_id');
            $table->dateTime('last_accrual_at')->nullable()->after('user_id');
            $table->unsignedSmallInteger('left_accruals')->default(0)->after('user_id');
            $table->unsignedSmallInteger('passed_accruals')->default(0)->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deposits', function (Blueprint $table) {
            $table->dropColumn('left_accruals');
            $table->dropColumn('passed_accruals');
            $table->dropColumn('last_accrual_at');
            $table->dropColumn('next_accrual_at');
        });
    }
}
