<?php

namespace Modules\Core\Services\Contracts;

interface SocialAuthServiceInterface
{
    /**
     * @param $provider
     * @return mixed
     */
    public function driver($provider);

    public function getAvailableProviders() : array;

    public function getActiveProviders() : array;
}
