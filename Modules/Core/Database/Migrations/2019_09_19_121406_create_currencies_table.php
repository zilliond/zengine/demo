<?php

use Modules\Core\Models\Currency;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCurrenciesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('symbol');
            $table->string('code');
            $table->string('code_iso')->index();
            $table->string('type')->default(app('zengine')->modelClass('Currency')::TYPE_FIAT);
            $table->unsignedSmallInteger('format_precision')->default(2);
            $table->boolean('auto_update_rate')->default(0);
            $table->string('update_provider')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        app('zengine')->model('Currency')->create([
            'name'              => 'Dollar USD',
            'symbol'            => '$',
            'code'              => 'usd',
            'code_iso'          => 'USD',
            'rate'              => 1,
            'update_provider'   => 'openexchangerates_free'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currencies');
    }
}
