<?php

return [
    'dashboard' => [
        'title'         => 'Главная админки',
        'description'   => 'Страница статистики проекта',
    ],
    'operations' => [
        'title'         => 'Пополнения / Вывод',
        'description'   => 'Все операции пополнения / вывода',
    ],
    'plans' => [
        'title'         => 'Инвестиционные планы',
        'description'   => 'Список инвестиционных планов',
    ],
    'plans_create' => [
        'title'         => 'Инвестиционные планы',
        'description'   => 'Создание инвестиционного плана',
    ],
    'plans_edit' => [
        'title'         => 'Инвестиционные планы',
        'description'   => 'Редактирование инвестиционного плана',
    ],
    'currencies' => [
        'title'         => 'Валюты',
        'description'   => 'Список валют',
    ],
    'currencies_create' => [
        'title'         => 'Валюты',
        'description'   => 'Добавление валюты',
    ],
    'currencies_edit' => [
        'title'         => 'Валюты',
        'description'   => 'Редактирование валюты',
    ],
    'payment_systems' => [
        'title'         => 'Платежные системы',
        'description'   => 'Список платежных систем',
    ],
    'payment_systems_create' => [
        'title'         => 'Платежные системы',
        'description'   => 'Добавление платежной системы',
    ],
    'payment_systems_edit' => [
        'title'         => 'Платежные системы',
        'description'   => 'Редактирование платежной системы',
    ],
    'deposits' => [
        'title'         => 'Депозиты',
        'description'   => 'Открытые депозиты проекта',
    ],
    'users' => [
        'title'         => 'Управление пользователями',
        'description'   => 'Добавляйте и управляйте пользователями',
    ],
    'users_referrals' => [
        'title'         => 'Рефераллы',
        'description'   => 'Рефераллы пользователя',
    ],
    'users_auth_log' => [
        'title'         => 'Лог авторизаций',
        'description'   => 'Лог авторизаций пользователя',
    ],
    'users_newsletter' => [
        'title'         => 'Рассылка пользователям',
        'description'   => 'Рассылка сообщения пользователям',
    ],
    'users_edit' => [
        'title'         => 'Управление пользователями',
        'description'   => 'Редактирование пользователя',
    ],
    'referrals' => [
        'title'         => 'Рейтинг рефоводов',
        'description'   => 'Рейтинг рефоводов',
    ],
    'referrals_settings' => [
        'title'         => 'Реферальные уровни',
        'description'   => 'Реферальные уровни',
    ],
    'pages_create' => [
        'title'         => 'Создать страницу',
        'description'   => 'Создание статичной страницы',
    ],
    'pages' => [
        'title'         => 'Созданные страницы',
        'description'   => 'Список статических страниц',
    ],
    'pages_edit' => [
        'title'         => 'Редактирование страницы',
        'description'   => 'Редактирование статичной страницы',
    ],
    'faq_create' => [
        'title'         => 'Добавить FAQ',
        'description'   => 'Создание FAQ',
    ],
    'faq' => [
        'title'         => 'FAQ',
        'description'   => 'FAQ',
    ],
    'faq_edit' => [
        'title'         => 'Редактирование FAQ',
        'description'   => 'Редактирование FAQ',
    ],
    'settings' => [
        'title'         => 'Настройки проекта',
        'description'   => 'Полная настройка вашего проекта',
    ],
    'settings_fake' => [
        'title'         => 'Накрутка статистики',
        'description'   => 'Страница фейковой статистики',
    ],
    'posts' => [
        'title'         => 'Новости',
        'description'   => 'Список новостей для редактирования',
    ],
    'posts_create' => [
        'title'         => 'Новости',
        'description'   => 'Страница добавления новостей',
    ],
    'posts_edit' => [
        'title'         => 'Новости',
        'description'   => 'Редактирование новости',
    ],
    'tickets' => [
        'title'         => 'Тикеты',
        'description'   => 'Список чаты',
    ],
    'tickets_show' => [
        'title'         => 'Тикеты',
        'description'   => 'Чат',
    ],
    'auth_log' => [
        'title'         => 'Мониторинг IP',
        'description'   => 'Мониторинг IP',
    ],
    'blacklist' => [
        'title'         => 'Черный список IP',
        'description'   => 'Черный список IP',
    ],
    'system_info' => [
        'title'         => 'Информация о сервере',
        'description'   => 'Информация о сервере',
    ],
    'promos' => [
        'title'         => 'Ваш учет',
        'description'   => 'Веди свой учет по проекту',
    ],
    'promos_type' => [
        'title'         => 'Ваш учет',
        'description'   => 'Веди свой учет по проекту',
    ],
    'newsletter' => [
        'title'         => 'Рассылка ЛС',
        'description'   => 'Рассылка личных сообшений пользователям',
    ],
    'analytics' => [
        'title'         => 'Аналитика',
        'description'   => 'Аналитика всего проекта',
    ],
    '404' => [
        'title'         => 'Ошибка',
        'description'   => 'Страницы не существует',
    ],
];
