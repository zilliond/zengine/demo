#!/bin/sh

cd /app

# Need to edit
echo 'current env'
echo $1
if [ "$1" = "demo" ]
then
    echo "init demo"
    cp .env.demo .env
else
    echo 'not stage'
fi

mkdir -p bootstrap/cache storage/framework/sessions storage/framework/views storage/framework/cache

composer install --prefer-source
php artisan cache:clear
php artisan config:clear
php artisan storage:link

chmod +x wait-for-it.sh
./wait-for-it.sh -t 60 mysql:3306
php artisan migrate:fresh --seed --force
php artisan passport:install
