<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Admin\Http\Requests\Api\CreateDepositRequest;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;

class DepositController extends Controller
{
    /**
     * @var \Modules\Core\CurrencyRateUpdaters\CurrencyServiceInterface
     */
    private $currencyService;

    public function __construct()
    {
        $this->currencyService = app('currencies');
    }

    public function index()
    {
        return app('zengine')->model('Deposit')->with(['user', 'currency', 'plan'])
            ->when($sort = request('sort_by', 'id'), function (Builder $q) use ($sort) {
                switch ($sort) {
                    case 'date':
                        $q->orderByDesc('created_at');
                        break;
                    case 'login':
                        $q->orderByDesc('user_id');
                        break;
                    case 'amount':
                        $q->orderByDesc('amount');
                        break;
                    case 'plan':
                        $q->orderByDesc('plan_id');
                        break;
                    default:
                        $q->orderByDesc('id');
                }
            })
            ->paginate(10);
    }

    /**
     * @param  CreateDepositRequest  $request
     * @return array
     * @throws \Modules\Core\Exceptions\BalanceException
     * @throws \Modules\Core\Exceptions\DepositException
     */
    public function store(CreateDepositRequest $request)
    {
        $deposit = app('zengine')->model('Deposit')->create($request->all());
        $depositService = app('deposits');
        $depositService->create($deposit, true);
        return [
            'status' => 'success'
        ];
    }

    public function customAccrual(Request $request)
    {
        $plan = app('zengine')->model('Plan')->findOrFail($request->get('plan_id'));
        $percent = (float) $request->get('percent');
        $depositService = app('Modules\Core\Services\DepositService');
        return $depositService->adminPlanAccrual($plan, $percent);
    }

    public function stats()
    {
        $deposits = app('zengine')->model('Deposit')->with('currency')->get(['status', 'amount', 'currency_id']);
        $opened_sum = $deposits
            ->where('status', app('zengine')->modelClass('Deposit')::STATUS_OPEN)
            ->reduce(function ($sum, $deposit) {
                /** @var Deposit $deposit */
                return round($sum + $this->currencyService->convertToDefault($deposit->amount, $deposit->currency), 2);
            }, 0);
        $sum = $deposits
            ->reduce(function ($sum, $deposit) {
                /** @var Deposit $deposit */
                return round($sum + $this->currencyService->convertToDefault($deposit->amount, $deposit->currency), 2);
            }, 0);
        $opened_count = $deposits->where('status', app('zengine')->modelClass('Deposit')::STATUS_OPEN)->count();
        return [
            'opened_sum'    => $opened_sum,
            'sum'           => $sum,
            'opened_count'  => $opened_count
        ];
    }

    public function refs()
    {
        $start_date = now()->subMonths(7);
        $operations = app('zengine')->model('Operation')->with('currency')->where('type', app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->where('created_at', '>', $start_date)
            ->get();
        $all_operations = app('zengine')->model('Operation')->with('currency')->where('type', app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->get();

        $operations = $this->timeOperations($operations);
        $operations['total'] = $all_operations->reduce(function ($carry, Operation $operation) {
            return $carry + round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
        }) ?? 0;
        return $operations;
    }

    protected function timeOperations(Collection $all_operations, $iterations = 7)
    {
        $stats = [
            'days'     => collect(),
            'two_days' => collect(),
            'weeks'    => collect(),
            'months'   => collect(),
        ];
        //$start_date
        for ($i = 0; $i < 14; $i++) {
            $start_date = now()->subDays($i)->startOfDay();
            $end_date = now()->subDays($i)->endOfDay();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $start_date->toDateString();
            $stats['days'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['days'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['days'] = $stats['days']->reverse();
        for ($i = 1; $i <= 14; $i++) {
            $start_date = now()->subDays($i * 2)->startOfDay();
            $end_date = now()->subDays($i * 2 - 2)->endOfDay();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $end_date->toDateString();
            $stats['two_days'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['two_days'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['two_days'] = $stats['two_days']->reverse();
        for ($i = 0; $i < 14; $i++) {
            $start_date = now()->subWeeks($i)->startOfWeek();
            $end_date = now()->subWeeks($i)->endOfWeek();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $end_date->toDateString();
            $stats['weeks'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['weeks'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['weeks'] = $stats['weeks']->reverse();
        for ($i = 0; $i < 14; $i++) {
            $start_date = now()->subMonths($i)->startOfMonth();
            $end_date = now()->subMonths($i)->endOfMonth();
            $current = $all_operations->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
            $key = $start_date->toDateString();
            $stats['months'][$key] = 0;
            $current->map(function (Operation $operation) use (&$stats, $key) {
                $stats['months'][$key] += round($this->currencyService->convertToDefault($operation->amount, $operation->currency), 2);
            });
        }
        $stats['months'] = $stats['months']->reverse();

        return $stats;
    }
}
