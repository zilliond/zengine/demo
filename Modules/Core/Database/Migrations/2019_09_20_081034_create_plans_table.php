<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('min_amount');
            $table->decimal('max_amount');
            $table->unsignedInteger('currency_id');
            $table->decimal('percent');
            $table->string('type')->default(app('zengine')->modelClass('Plan')::TYPE_DAILY);
            $table->integer('duration')->default(1);
            $table->decimal('amount_bonus')->default(0);
            $table->decimal('balance_bonus')->default(0);
            $table->boolean('not_charge_weekends')->default(0);
            $table->decimal('additional_referral_percent')->nullable();
            $table->boolean('is_limited')->default(0);
            $table->integer('max_deposits')->nullable();
            $table->boolean('can_close')->default(0);
            $table->decimal('return_percent')->nullable();
            $table->boolean('is_hidden')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plans');
    }
}
