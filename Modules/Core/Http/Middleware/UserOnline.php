<?php

namespace Modules\Core\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Modules\Core\Models\User;

class UserOnline
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Auth::check()) {
            /** @var User $user */
            $user = \Auth::user();
            $expiresAt = Carbon::now()->addMinutes(15);
            if (\Cache::missing('user-is-online-'.$user->id)) {
                \Cache::put('user-is-online-'.$user->id, true, $expiresAt);
                $user->last_online_at = now();
                $user->save();
            }
        }

        return $next($request);
    }
}
