<?php


namespace Modules\Core\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\Operation;

class OperationNotificationEvent implements ShouldBroadcast
{
    use SerializesModels, InteractsWithSockets;

    /**
     * @var Operation
     *
     */
    public $operation;

    /**
     * Create a new event instance.
     *
     * @param  Operation  $operation
     */
    public function __construct(Operation $operation)
    {
        $this->operation = $operation;
        $this->operation->loadMissing(['currency', 'payment_system', 'target']);
    }

    protected function getTranslateParams()
    {
        return [
            'amount'    => $this->operation->amount,
            'currency'  => $this->operation->currency->symbol,
            'payment'   => optional($this->operation->payment_system)->name,
            'user'      => $this->operation->target instanceof Operation ? $this->operation->target->user->name : '',
            'deposit'   => $this->operation->target instanceof Deposit ? $this->operation->target->id : '',
            'left'      => $this->operation->target instanceof Deposit ? $this->operation->target->left_accruals : '',
        ];
    }

    public function getNotificationStatus()
    {
        switch ($this->operation->status) {
            case app('zengine')->modelClass('Operation')::STATUS_SUCCESS:
                return 'success';
            case app('zengine')->modelClass('Operation')::STATUS_CREATED:
                if ($this->operation->type === app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN) {
                    return 'success';
                }
                return 'primary';
            case app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS:
                return 'primary';
            case app('zengine')->modelClass('Operation')::STATUS_REJECTED:
            case app('zengine')->modelClass('Operation')::STATUS_CANCELED:
                return 'error';
        }
        return 'primary';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|\Illuminate\Broadcasting\Channel[]
     */
    public function broadcastOn()
    {
        // TODO: Implement broadcastOn() method.
    }
}
