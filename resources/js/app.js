require('./bootstrap');
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import LaravelEcho from './plugins/LaravelEcho'
import Toasted from 'vue-toasted';

Vue.use(VeeValidate);

window.Pusher = require('pusher-js');
Vue.use(new LaravelEcho({
	broadcaster: 'pusher',
	key: window.system.sockets.key,
	cluster: window.system.sockets.cluster,
}));
Vue.use(Toasted);

window.Vue = Vue;

Vue.component('invest-form', require('./components/Cabinet/InvestForm').default);
Vue.component('unlock-modal', require('./components/Cabinet/UnlockPin').default);


import VueI18n from 'vue-i18n'
Vue.use(VueI18n);
import messages from './lang.generated';
const i18n = new VueI18n({
    locale: window.system.locale, // set locale
    messages, // set locale messages
});

const app = new Vue({
    i18n,
    el: '#main',
    data(){
        return {
            system: window.system,
            unlock_modal: false,
        }
    },
    created: function (){
        console.log(this.system.user_id);
        if(this.$laravelEcho && this.system.user_id){
            this.$laravelEcho.private(`user.operations.${this.system.user_id}`)
                .listen('.operation.created', this.operationNotification)
                .listen('.operation.status-updated', this.operationNotification);
        }
    },
    methods: {
        operationNotification(e){
            console.log(e);
            if(e.has_message){
                this.$toasted.show(e.message, {
                    theme: `toasted-status-${e.style}`,
                    position: "bottom-right",
                    duration : 5000,
                    action: {
                        text: 'Close',
                        onClick : (event, toastObject) => {
                            toastObject.goAway(event);
                        }
                    }
                });
            }
        }
    }
});
