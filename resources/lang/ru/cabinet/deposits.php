<?php

return [
    'title'         => 'Депозиты',
    'plan'          => 'План',
    'wallet'        => 'Кошелек',
    'amount'        => 'Сумма',
    'accruals'      => 'Начислений',
    'accrued'       => 'Начислено',
    'status'        => 'Статус',
    'next_accrual'  => 'Следующее начисление',
    'last_accrual'  => 'Последнее начисление',
];
