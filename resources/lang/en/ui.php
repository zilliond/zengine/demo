<?php

return [
    'registration'  => 'Registration',
    'sign_up'       => 'Sign up',
    'menu'          => [
        'home'      => 'Home',
        'cabinet'   => 'Cabinet',
        'news'      => 'News',
        'faq'       => 'FAQ',
        'dashboard' => 'Admin-panel',
        'login'     => 'Sign in',
        'register'  => 'Sign up',
        'logout'    => 'Logout',
    ],
    'yes'          => 'Yes',
    'no'           => 'No',
    'days'         => 'Days',
    'hours'        => 'Hours',
];
