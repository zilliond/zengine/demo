<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\SocialAccount
 *
 * @property int $id
 * @property int $user_id
 * @property string $provider_user_id
 * @property string $provider
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Models\User $user
 * @method static Builder|\App\SocialAccount newModelQuery()
 * @method static Builder|\App\SocialAccount newQuery()
 * @method static Builder|\App\SocialAccount query()
 * @method static Builder|\App\SocialAccount whereCreatedAt($value)
 * @method static Builder|\App\SocialAccount whereId($value)
 * @method static Builder|\App\SocialAccount whereProvider($value)
 * @method static Builder|\App\SocialAccount whereProviderUserId($value)
 * @method static Builder|\App\SocialAccount whereUpdatedAt($value)
 * @method static Builder|\App\SocialAccount whereUserId($value)
 * @mixin \Eloquent
 */
class SocialAccount extends Model
{
    protected $fillable = [
        'user_id',
        'provider_user_id',
        'provider',
    ];

    public function user() : BelongsTo
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }
}
