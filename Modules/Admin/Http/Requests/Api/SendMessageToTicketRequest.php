<?php


namespace Modules\Admin\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageToTicketRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_id'     => 'nullable|integer|exists:tickets,id',
            'ticket'        => 'nullable|string',
            'message'       => 'required|string|min:1',
            'from'          => 'required|string|exists:users,login',
        ];
    }
}
