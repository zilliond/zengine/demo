<?php
/** @noinspection PhpMultipleClassesDeclarationsInOneFile */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection AutoloadingIssuesInspection */
/** @noinspection PhpIllegalPsrClassPathInspection */

// @formatter:off

namespace Modules\Core{

    use Illuminate\Database\Eloquent\Model;

    /**
     * Class Zengine
     * @package zillion/zengine-core
     *
     * @property array $models  Array [modelMame => modelClass]
     * @method bool checkLicence() Check licence exist && valid && active
     * @method Model model(string $name) Get model by config file zengine.php
     * @method string modelClass(string $name) Get model by config file zengine.php
     * @method Zengine useModel(string $name, string|object $object) Change class for model on runtime
     * @method string version() Get current engine version (based on core)
     * @method void apiRoutes() Load engine modules api routes, need to use in routes/api.php before your routes
     * @method void webRoutes() Load engine modules web routes, need to use in routes/web.php before your routes
     * @method mixed getStat(string $key, $default = null) Get cached stat value with default value
     * @method Zengine setStat(string $key, $value) Set stat value and flush cache by this key
     */
    class ZEngine
    {
    }
}

namespace Modules\Core\Services{

    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Http\Request;
    use Modules\Core\Models\Currency;
    use Modules\Core\Models\Deposit;
    use Modules\Core\Models\Operation;
    use Modules\Core\Models\Plan;
    use Modules\Core\Models\User;
    use Modules\Core\Models\Wallet;
    use Modules\Core\OperationTypes\OperationType;
    use Modules\Core\Payments\PaymentSystemProvider;

    /**
     * Class CurrencyService
     * @package zillion/zengine-core
     * In this class, the returned models are used from the config file config/zengine.php
     *
     * @method Currency getDefaultCurrency() return default Currency
     * @method Currency getSystemCurrency() return default Currency
     * @method Collection getCurrencies() return all Currency with CurrencyRate relations
     * @method float convertByCode(float $amount, string $code_from, string $code_to) convert amount using a ISO 4217 code
     * @method float convert(float $amount, Currency $from, Currency $to) Convert amount using a Currency object or inherited class object
     * @method float convertToDefault(float $amount, Currency $from) Convert amount from Currency object or inherited class object
     * @method float convertToSystem(float $amount, Currency $from) Convert amount from Currency object or inherited class object
     */
    class CurrencyService
    {


    }

    /**
     * Class DepositService
     * @package zillion/zengine-core
     * In this class, the returned models are used from the config file config/zengine.php
     *
     * @method Deposit create(Deposit $deposit, $force = false) Creating deposit process, checking funds, debiting (if $force is not false), deposit bonus !!IMPORTANT MANUAL USE AFTER CREATE DEPOSIT
     * @method Deposit initAccruals(Deposit $deposit) It is called automatically when creating a deposit (Creating event), sets the number of accruals and the date of the next accrual
     * @method void processAccrual(Deposit $deposit) Processes charges(check, make accrual, set next accrual date, close if end) (use database transaction)
     * @method Collection adminPlanAccrual(Plan $plan, float $percent) Make a manual charge for all deposits of the plan
     * @method Deposit close(Deposit $deposit) Close the deposit, and return the funds (if configured in the plan)
     */
    class DepositService
    {

    }

    /**
     * Class OperationService
     * @package zillion/zengine-core
     * In this class, the returned models are used from the config file config/zengine.php
     *
     * @property string[] $payment_providers array<key, provider class>
     * @method array getPaymentProviders() Get payment providers keys ex. [payeer, perfect_money, ...]
     * @method OperationService setProvider(string $alias, string $providerClass) Add or change payment provider, $providerClass - classname of provider (ex. \Modules\Core\Payments\PayeerProvider). Return self
     * @method PaymentSystemProvider getProvider(string $alias) Return provider by alias
     * @method array getProvidersFields() Return field for all providers. Return array<string, array<key, label>>
     * @method void handle(Request $request) Handle payment systems response(default: Instant payment notifications). Check canHandle of every active payment system PaymentProvider, and if true check call handle method of PaymentProvider.
     *
     * @method OperationService setOperationType(string $type, string $class) Add or change operation type (class example:  \Modules\Core\OperationTypes\UserRefillOperationType). Return self
     * @method OperationType getOperationType(string $type) Return operation type by type (ex. \Modules\Core\OperationTypes\UserRefillOperationType)
     * @method OperationType typeByOperation(Operation $operation) Proxy to operationType
     * @method array parsePercentsString($string) Convert percents strings(ex. 5-3-1), return [5.00, 3.00, 1.00]
     * @method string preparePercents(array $percents) Convert percents array [5.00, 3.00, 1.00], return strings(ex. 5-3-1)
     */
    class OperationService
    {

    }

    /**
     * Class UserService
     * @package zillion/zengine-core
     * In this class, the returned models are used from the config file config/zengine.php
     *
     * @method void onCreate(User $user) User created processing, ex: create referral for inviter
     * @method void createReferral(User $user, User $inviter, int $level = 1) Recursive create Referral, down-to-up
     * @method int onlineUsers() Currently online users
     * @method void putMoney(Operation $operation, User $user = null, Wallet $wallet = null) Put funds to user, based on operation, using Operation amount and currency. Wallet use on balance type divided. If $user or $wallet not set, use operation->user && operation->wallet
     * @method void takeMoney(Operation $operation) Take funds from user or wallet, based on operation, using Operation amount, currency, user, wallet(balance type divided).
     * @method bool canPayFromBalance(User $user, float $amount, Currency $currency = null) Check pay ability(funds enough), if $currency - null, use user currency.
     * @method bool canPayFromWallet(User $user, float $amount, Currency $currency = null, Wallet $wallet = null) Check pay ability(funds enough), if $currency - null, use user currency. $wallet need and required if balance type divided
     * @method bool canPayFrom(User $user, float $amount, Currency $currency = null, Wallet $wallet = null) Proxy method for canPayFromBalance and canPayFromWallet, based on balance type
     * @method float getReferralEarned(User $user, User $referral) Calculate referral profit
     * @method \Illuminate\Support\Collection getFlattenReferrals(User $user) Flatten of 3 levels user referrals
     */
    class UserService
    {

    }
}
