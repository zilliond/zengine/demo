@extends('layouts.cabinet')

@section('body')
    @php /** @var $user \Modules\Core\Models\User */ @endphp
    <div class="clearfix"></div>
        @if($user->referrals)
            <div class="col-sm-12">
                <h1>@lang('cabinet/profile.referrals.title')</h1>
                <div class="form-group">
                    {!! Form::label('invite_link', trans('cabinet/profile.form.invite_link')) !!}
                    {!! Form::text('invite_link', $user->invite_link, ['class' => 'form-control', 'readonly']) !!}
                </div>
                @php($inviter = $user->inviter)
                @if($inviter)
                    <div class="form-group">
                        {!! Form::label('inviter', 'Вас пригласил') !!}
                        {!! Form::text('inviter', "{$inviter->login}({$inviter->name})", ['class' => 'form-control', 'readonly']) !!}
                    </div>
                @endif
                <table class="table">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">@lang('cabinet/profile.referrals.login')</th>
                        <th scope="col">@lang('cabinet/profile.referrals.name')</th>
                        <th scope="col">@lang('cabinet/profile.referrals.earned')</th>
                        <th scope="col">@lang('cabinet/profile.referrals.level')</th>
                    </tr>
                    @php($referrals = $service->getFlattenReferrals($user))
                    @foreach($referrals as $referral)
                        <tr>
                            <td>{{ $referral->id }}</td>
                            <td>{{ $referral->login }}</td>
                            <td>{{ $referral->name }}</td>
                            <td>{{ $service->getReferralEarned($user, $referral) }} {{ $user->currency->symbol }}</td>
                            <td>{{ $referral->ref_level - $user->ref_level }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endif
    </div>
@endsection
@section('css')
    <style>
        .cabinet-profile .col-sm-12{
            margin-bottom: 1rem;
        }
    </style>
@endsection
