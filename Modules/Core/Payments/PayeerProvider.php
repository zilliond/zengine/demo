<?php


namespace Modules\Core\Payments;

use Illuminate\Http\Request;
use Log;
use Modules\Core\Exceptions;
use Modules\Core\Exceptions\OperationException;
use Modules\Core\Models\Operation;
use RuntimeException;

class PayeerProvider extends PaymentSystemProvider
{
    /* @inheritDoc */
    public $can_auto_withdraw = true;

    /* @inheritDoc */
    public $can_semi_auto_withdraw = true;

    /** @inheritDoc */
    public function getName() : string
    {
        return 'Payeer';
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws \Exception
     */
    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['shop_id', 'secret']);
        $m_amount = number_format($operation->amount, 2, '.', '');
        $m_desc = base64_encode('Пополнение баланса');
        $arHash = [
            'm_shop' => $this->getCredential('shop_id'), 'm_orderid' => $operation->id, 'm_amount' => $m_amount,
            'm_curr' => $operation->currency->code_iso, 'm_desc' => $m_desc, 'm_key' => $this->getCredential('secret')
        ];
        $sign = strtoupper(hash('sha256', implode(':', $arHash)));
        $request_data = $arHash;
        unset($request_data['m_key']);
        $request_data['m_sign'] = $sign;
        $redirect_url = 'https://payeer.com/merchant/?'.http_build_query($request_data);
        Log::channel('payment_systems')->info("PayeerProvider|makeRefill|Operation {$operation->id}: payment_url: $redirect_url");
        $operation->payment_system_data = [
            'm_amount' => $m_amount, 'sign' => $sign, 'type' => 'redirect', 'redirect_url' => $redirect_url
        ];
        $operation->save();
        return $operation;
    }

    /**
     * @param  Request  $request
     * @return bool
     */
    public function canHandle(Request $request) : bool
    {
        return $request->filled('m_operation_id') && $request->filled('m_sign');
    }

    /**
     * @param  Request  $request
     * @throws Exceptions\OperationException
     * @throws Exceptions\BalanceException
     */
    public function handle(Request $request)
    {
        if (!in_array($request->ip(), ['185.71.65.92', '185.71.65.189', '149.202.17.210'])) {
            throw new RuntimeException('PayeerProvider| request ip mismatch');
        };
        $arHash = $request->only([
            'm_operation_id',
            'm_operation_ps',
            'm_operation_date',
            'm_operation_pay_date',
            'm_shop',
            'm_orderid',
            'm_amount',
            'm_curr',
            'm_desc',
            'm_status',
            'm_params'
        ]);
        $operation = app('zengine')->model('Operation')->find((int) $request->input('m_orderid'));
        if (!$operation) {
            Log::channel('payment_systems')->info("PayeerProvider|Handler|Operation {$request->get('m_orderid')} not find - request", ['request' => $request->all()]);
            ob_end_clean();
            exit($request->get('m_orderid').'|error');
        }
        $arHash[] = $this->getCredential('secret');
        $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));
        if ($request->get('m_sign') === $sign_hash && $request->get('m_status') === 'success') {
            Log::channel('payment_systems')->info("PayeerProvider|Handler|Operation {$request->get('m_orderid')} success");
            $this->operationService->typeByOperation($operation)->success($operation);
            ob_end_clean();
            exit($request->get('m_orderid').'|success');
        }
        Log::channel('payment_systems')->info("PayeerProvider|Handler|Operation {$request->get('m_orderid')} error - request", ['request' => $request->all()]);
        ob_end_clean();
        exit($request->get('m_orderid').'|error');
    }

    /**
     * @return CPayeer
     * @throws OperationException
     */
    protected function apiLogin()
    {
        $this->checkCredentials(['account', 'api_id', 'api_key']);
        $payeer = new CPayeer($this->getCredential('account'), $this->getCredential('api_id'), $this->getCredential('api_key'));
        if (!$payeer->isAuth()) {
            Log::channel('payment_systems')->error("PayeerProvider|apiLogin|Auth Error", ['errors' => $payeer->getErrors()]);
            throw new OperationException('PayeerProvider|apiLogin|Auth Error');
        }
        return $payeer;
    }

    /**
     * @return float
     * @throws OperationException
     */
    public function getBalance() : float
    {
        return 0.00;
        $payeer = $this->apiLogin();
        $response = $payeer->getBalance();
        if (isset($response['balance'])) {
            if (isset($response['balance']['USD'])) {
                if (isset($response['balance']['USD']['DOSTUPNO'])) {
                    return (float) $response['balance']['USD']['DOSTUPNO'];
                } else {
                    Log::channel('payment_systems')->error('PayeerProvider|getBalance|Response error $response[\'balance\'][\'USD\'][\'DOSTUPNO\'] not define', ['response' => $response]);
                }
            } else {
                Log::channel('payment_systems')->error('PayeerProvider|getBalance|Response error $response[\'balance\'][\'USD\'] not define', ['response' => $response]);
            }
        } else {
            Log::channel('payment_systems')->error('PayeerProvider|getBalance|Response error $response[\'balance\'] not define', ['response' => $response]);
        }

        return 0.00;
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws Exceptions\BalanceException
     * @throws OperationException
     */
    public function makeWithdraw(Operation $operation) : Operation
    {
        $payeer = $this->apiLogin();
        if (!isset($operation->payment_system_data['target'])) {
            throw new OperationException('PayeerProvider|makeWithdraw|Not set target account');
        }
        $arTransfer = $payeer->transfer([
            'curIn'   => $operation->currency->code_iso,
            'sum'     => $operation->amount,
            'curOut'  => $operation->currency->code_iso,
            'to'      => $operation->payment_system_data['target'],
            'comment' => 'Withdraw from '.config('app.name'),
        ]);
        $operation->payment_system_data += [
            'transfer' => $arTransfer
        ];
        if (!empty($arTransfer['errors'])) {
            Log::channel('payment_systems')->error('PayeerProvider|makeWithdraw|Withdraw error', ['operation' => $operation, 'transfer' => $arTransfer]);
        }
        if (isset($arTransfer['historyId']) && $arTransfer['historyId']) {
            $operation->payment_system_data['transaction_id'] = $arTransfer['historyId'];
            $this->operationService->typeByOperation($operation)->success($operation);
        } else {
            $this->operationService->typeByOperation($operation)->inProgress($operation);
        }
        $operation->save();
        return $operation;
    }

    public function getCredentialsKeys() : array
    {
        return [
            'shop_id',
            'secret',
            'account',
            'api_id',
            'api_key',
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'shop_id'   => 'ID магазина',
            'secret'    => 'Приватный ключ магазина',
            'account'   => 'Номер аккаунта',
            'api_id'    => 'API ID',
            'api_key'   => 'API key',
        ];
    }
}
