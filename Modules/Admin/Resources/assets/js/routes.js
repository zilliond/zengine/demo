import Dashboard from "./pages/Dashboard";
import Plans from "./pages/Plans";
import PlanCreate from "./pages/PlanCreate";
import PlanEdit from "./pages/PlanEdit";
import Deposits from "./pages/Deposits";
import Operations from "./pages/Operations";
import Currencies from "./pages/Currencies";
import CurrencyCreate from "./pages/CurrencyCreate";
import CurrencyEdit from "./pages/CurrencyEdit";
import PaymentSystems from "./pages/PaymentSystems";
import PaymentSystemCreate from "./pages/PaymentSystemCreate";
import PaymentSystemEdit from "./pages/PaymentSystemEdit";
import Users from "./pages/Users";
import Newsletter from "./pages/Newsletter";
import Promos from "./pages/Promos";
import UserReferrals from "./pages/UserReferrals";
import UserAuthLog from "./pages/UserAuthLog";
import User from "./pages/User";
import Referrals from "./pages/Referrals";
import ReferralsSettings from "./pages/ReferralsSettings";
import Pages from "./pages/Pages";
import PageCreate from "./pages/PageCreate";
import PageEdit from "./pages/PageEdit";
import StatisticsFake from "./pages/StatisticsFake";
import Settings from "./pages/Settings";
import Posts from "./pages/Posts";
import PostCreate from "./pages/PostCreate";
import PostEdit from "./pages/PostEdit";
import AuthLog from "./pages/AuthLog";
import Blacklist from "./pages/Blacklist";
import SystemInfo from "./pages/SystemInfo";
import PromosInternal from "./pages/PromosInternal";
import ProjectAnalytics from "./pages/ProjectAnalytics";
import Faq from "./pages/Faq";
import FaqCreate from "./pages/FaqCreate";
import FaqEdit from "./pages/FaqEdit";
import Tickets from "./pages/Tickets";
import TicketCreate from "./pages/TicketCreate";
import TicketShow from "./pages/TicketShow";

const ViewRouter = {
    name: 'ViewRouter',
    template: `
        <router-view/>
`
};

export default [
    {
        path: '/',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            headerLogo: '/img/sprite.svg#real-estate',
        }
    },
    {
        path: '/operations/:type?',
        name: 'operations',
        component: Operations,
        props: true,
        meta: {
            headerLogo: '/img/sprite.svg#transaction',
        }
    },
    {
        path: '/plans',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'plans',
                component: Plans,
                meta: {
                    headerLogo: '/img/sprite.svg#portfolio-of-business',
                },
            },
            {
                path: 'create',
                name: 'plans_create',
                component: PlanCreate,
                meta: {
                    headerLogo: '/img/sprite.svg#portfolio-of-business',
                },
            },
            {
                path: ':id/edit',
                name: 'plans_edit',
                component: PlanEdit,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#portfolio-of-business',
                },
            },
        ]
    },
    {
        path: '/currencies',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'currencies',
                component: Currencies,
                meta: {
                    headerLogo: '/img/sprite.svg#coin',
                },
            },
            {
                path: 'create',
                name: 'currencies_create',
                component: CurrencyCreate,
                meta: {
                    headerLogo: '/img/sprite.svg#coin',
                },
            },
            {
                path: ':id/edit',
                name: 'currencies_edit',
                component: CurrencyEdit,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#coin',
                },
            },
        ]
    },
    {
        path: '/payment_systems',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'payment_systems',
                component: PaymentSystems,
                meta: {
                    headerLogo: '/img/sprite.svg#coin',
                },
            },
            {
                path: 'create',
                name: 'payment_systems_create',
                component: PaymentSystemCreate,
                meta: {
                    headerLogo: '/img/sprite.svg#coin',
                },
            },
            {
                path: ':id/edit',
                name: 'payment_systems_edit',
                component: PaymentSystemEdit,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#coin',
                },
            },
        ]
    },
    {
        path: '/deposits',
        name: 'deposits',
        component: Deposits,
        meta: {
            headerLogo: '/img/sprite.svg#coin-stack',
        }
    },
    {
        path: '/users',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'users',
                component: Users,
                meta: {
                    headerLogo: '/img/sprite.svg#rating',
                },
            },
            {
                path: ':id/referrals',
                name: 'users_referrals',
                component: UserReferrals,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#rating',
                },
            },
            {
                path: ':id/auth_log',
                name: 'users_auth_log',
                component: UserAuthLog,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#rating',
                },
            },
            {
                path: 'newsletter',
                name: 'users_newsletter',
                component: Newsletter,
                meta: {
                    headerLogo: '/img/sprite.svg#sent',
                },
            },
            {
                path: ':id',
                name: 'users_edit',
                component: User,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#rating',
                },
            },
        ]
    },
    {
        path: '/referrals',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'referrals',
                component: Referrals,
                meta: {
                    headerLogo: '/img/sprite.svg#rating',
                },
            },
            {
                path: 'settings',
                name: 'referrals_settings',
                component: ReferralsSettings,
                meta: {
                    headerLogo: '/img/sprite.svg#hand-shake',
                },
            },
        ]
    },
    {
        path: '/pages',
        component: ViewRouter,
        children: [
            {
                path: 'create',
                name: 'pages_create',
                component: PageCreate,
                meta: {
                    headerLogo: '/img/sprite.svg#add-circular-button',
                },
            },
            {
                path: '',
                name: 'pages',
                component: Pages,
                meta: {
                    headerLogo: '/img/sprite.svg#confirm',
                },
            },
            {
                path: ':id',
                name: 'pages_edit',
                component: PageEdit,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#confirm',
                },
            },
        ]
    },
    {
        path: '/faq',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'faq',
                component: Faq,
                meta: {
                    headerLogo: '/img/sprite.svg#confirm',
                },
            },
            {
                path: 'create',
                name: 'faq_create',
                component: FaqCreate,
                meta: {
                    headerLogo: '/img/sprite.svg#add-circular-button',
                },
            },
            {
                path: ':id',
                name: 'faq_edit',
                component: FaqEdit,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#confirm',
                },
            },
        ]
    },
    {
        path: '/settings',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'settings',
                component: Settings,
                meta: {
                    headerLogo: '/img/sprite.svg#settings-cog',
                },
            },
            {
                path: 'fake',
                name: 'settings_fake',
                component: StatisticsFake,
                meta: {
                    headerLogo: '/img/sprite.svg#bar-chart',
                },
            },
        ]
    },
    {
        path: '/news',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'posts',
                component: Posts,
                meta: {
                    headerLogo: '/img/sprite.svg#edit',
                },
            },
            {
                path: 'create',
                name: 'posts_create',
                component: PostCreate,
                meta: {
                    headerLogo: '/img/sprite.svg#add',
                },
            },
            {
                path: ':id',
                name: 'posts_edit',
                component: PostEdit,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#edit',
                },
            },
        ]
    },
    {
        path: '/auth_log',
        name: 'auth_log',
        component: AuthLog,
        meta: {
            headerLogo: '/img/sprite.svg#musica-searcher',
        },
    },
    {
        path: '/blacklist',
        name: 'blacklist',
        component: Blacklist,
        meta: {
            headerLogo: '/img/sprite.svg#cancel',
        },
    },
    {
        path: '/info',
        name: 'system_info',
        component: SystemInfo,
        meta: {
            headerLogo: '/img/sprite.svg#information',
        },
    },
    {
        path: '/promos',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'promos',
                component: Promos,
                meta: {
                    headerLogo: '/img/sprite.svg#transaction',
                },
            },
            {
                path: ':type',
                name: 'promos_type',
                component: PromosInternal,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#transaction',
                },
            },
        ]
    },
    {
        path: '/tickets',
        component: ViewRouter,
        children: [
            {
                path: '',
                name: 'tickets',
                component: Tickets,
                meta: {
                    headerLogo: '/img/sprite.svg#confirm',
                },
            },
            {
                path: 'create',
                name: 'tickets_create',
                component: TicketCreate,
                meta: {
                    headerLogo: '/img/sprite.svg#add-circular-button',
                },
            },
            {
                path: ':id',
                name: 'tickets_show',
                component: TicketShow,
                props: true,
                meta: {
                    headerLogo: '/img/sprite.svg#confirm',
                },
            },
        ]
    },
    {
        path: '/analytics',
        name: 'analytics',
        component: ProjectAnalytics,
        meta: {
            headerLogo: '/img/sprite.svg#settings-cog',
        },
    },
    {
        path: '*',
        name: '404',
        meta: {
            headerLogo: '/img/sprite.svg#real-estate',
        }
    }
];
