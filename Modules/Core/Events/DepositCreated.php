<?php

namespace Modules\Core\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;
use Modules\Core\Models\Deposit;

class DepositCreated implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * @var Deposit
     *
     */
    public $deposit;

    /**
     * Create a new event instance.
     *
     * @param  Deposit  $deposit
     */
    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('user.deposits.'.$this->deposit->user_id);
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'deposit.created';
    }
}
