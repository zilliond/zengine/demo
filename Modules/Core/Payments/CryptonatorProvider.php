<?php


namespace Modules\Core\Payments;

use Modules\Core\Exceptions;
use Modules\Core\Models\Operation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Log;

class CryptonatorProvider extends PaymentSystemProvider
{
    /* @inheritDoc */
    public $can_auto_withdraw = true;

    /* @inheritDoc */
    public $can_semi_auto_withdraw = true;

    private const SCI_ACTION = 'https://api.cryptonator.com/api/merchant/v1/startpayment';

    /** @inheritDoc */
    public function getName() : string
    {
        return 'Cryptonator';
    }
    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws Exceptions\OperationException
     */
    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['sci_id']);
        $request_data = [
            'merchant_id'       => $this->getCredential('sci_id'),
            'item_name'         => 'Refill to account '.$operation->user_id,
            'order_id'          => $operation->id,
            'invoice_currency'  => Str::lower($operation->currency->code_iso),
            'invoice_amount'    => $operation->amount,
            'success_url'       => route('cabinet.refill', ['status' => 'success']),
            'failed_url'        => route('cabinet.refill', ['status' => 'error']),
        ];
        $redirect_url = self::SCI_ACTION.'?'.http_build_query($request_data);
        Log::channel('payment_systems')->info("CryptonatorProvider|makeRefill|Operation {$operation->id}: payment_url: $redirect_url");
        $operation->payment_system_data = [
            'type'          => 'redirect',
            'request_data'  => $request_data,
            'redirect_url'  => $redirect_url
        ];
        $operation->save();

        return $operation;
    }

    /**
     * @param  Request  $request
     * @return bool
     * @throws Exceptions\OperationException
     */
    public function canHandle(Request $request) : bool
    {
        $this->checkCredentials(['sci_id']);
        Log::channel('payment_systems')->info("CryptonatorProvider|canHandle", ['request' => $request->all()]);
        return $request->filled('merchant_id') && $request->filled('invoice_id') && $this->getCredential('sci_id') === $request->get('merchant_id');
    }

    /**
     * @param  Request  $request
     * @throws Exceptions\OperationException
     * @throws Exceptions\BalanceException
     */
    public function handle(Request $request)
    {
        $this->checkCredentials(['sci_secret']);
        Log::channel('payment_systems')->info("CryptonatorProvider|handle request", ['request' => $request->all()]);
        $hash_data = $request->only([
           'merchant_id',
           'invoice_id',
           'invoice_created',
           'invoice_expires',
           'invoice_amount',
           'invoice_currency',
           'invoice_status',
           'invoice_url',
           'order_id',
           'checkout_address',
           'checkout_amount',
           'checkout_currency',
           'date_time',
        ]);
        $hash_data['secret'] = $this->getCredential('sci_secret');
        $hash = sha1(implode('&', array_values($hash_data)));
        Log::channel('payment_systems')->info("CryptonatorProvider|handle hash", ['hash' => $hash]);
        $operation = app('zengine')->model('Operation')->find((int) $request->input('order_id'));
        if (!$operation) {
            Log::channel('payment_systems')->info("CryptonatorProvider|Handler|Operation {$request->get('order_id')} not find - request");
            exit("Operation {$request->get('order_id')} not find");
        }
        if ($request->get('secret_hash') === $hash && $request->get('invoice_status') === 'paid') {
            Log::channel('payment_systems')->info("CryptonatorProvider|Handler|Operation {$request->get('order_id')} success");
            $this->operationService->typeByOperation($operation)->success($operation);
            exit("Operation {$request->get('order_id')} success");
        }
        Log::channel('payment_systems')->info("CryptonatorProvider|Handler|Operation {$request->get('order_id')} error - request");
        exit("Operation {$request->get('order_id')} error");
    }

    public function getCredentialsKeys() : array
    {
        return [
            'sci_id',
            'sci_secret',
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'sci_id'        => 'ID магазина',
            'sci_secret'    => 'Приватный ключ магазина',
        ];
    }
}
