<?php


namespace Modules\Core\OperationTypes;


use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\UserServiceInterface;

class UserBonusOperationType extends OperationType
{
    public function success(Operation $operation) : Operation
    {
        app(UserServiceInterface::class)->putMoney($operation);
        return parent::success($operation);
    }
}
