const mix = require('laravel-mix');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');

const webpack_config = require('./webpack.config');
require('laravel-mix-merge-manifest');
mix.webpackConfig(webpack_config);
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
/* Allow multiple Laravel Mix applications*/
mix.mergeManifest();
/*----------------------------------------*/


mix
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/auth.scss', 'public/css');
mix.version();

mix.webpackConfig({
	plugins: [
		new SVGSpritemapPlugin({
			src: 'resources/auth/*.svg',
			filename: 'auth/socials.svg',
			prefix: '',
			svgo: true,
		}),
	]
});
