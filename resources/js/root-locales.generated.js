export default {
    "en": [],
    "ru": {
        "ui": {
            "registration": "Регистрация",
            "sign_up": "Зарегистрироваться",
            "menu": {
                "home": "Главная",
                "cabinet": "Кабинет",
                "news": "Новости",
                "dashboard": "Админ-панель",
                "login": "Вход",
                "register": "Регистрация",
                "logout": "Выйти"
            }
        },
        "cabinet": {
            "ui": {
                "menu": {
                    "index": "Главная",
                    "profile": "Профиль",
                    "security": "Безопасность",
                    "wallets": "Кошельки",
                    "invest": "Инвестировать",
                    "deposits": "Депозиты",
                    "operations": "Операции",
                    "refill": "Пополнить",
                    "withdraw": "Вывести",
                    "transfer": "Перевести",
                    "support": "Поддержка"
                },
                "account_locked": "Аккаунт заблокирован, для вывода необходимо разблокировать.",
                "save": "Сохранить",
                "enable": "Включить",
                "disable": "Выключить",
                "unlock": "Разблокировать",
                "lock": "Заблокировать",
                "search": "Поиск"
            }
        }
    }
}
