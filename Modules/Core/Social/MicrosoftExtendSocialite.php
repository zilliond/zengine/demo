<?php


namespace Modules\Core\Social;

use SocialiteProviders\Manager\SocialiteWasCalled;

class MicrosoftExtendSocialite
{
    /**
     * Execute the provider.
     * @param  SocialiteWasCalled  $socialiteWasCalled
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('microsoft', MicrosoftProvider::class);
    }
}
