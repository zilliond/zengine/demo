<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class Migrate extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'zengine:migrate {--seed} {--fresh}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->hasOption('fresh')) {
            $this->call('migrate:fresh');
        } else {
            $this->call('migrate');
        }
        if ($this->hasOption('seed')) {
            $this->call('db:seed');
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['seed', null, InputOption::VALUE_OPTIONAL, 'Seed.', null],
            ['fresh', null, InputOption::VALUE_OPTIONAL, 'Fresh database before migrate.', null],
        ];
    }
}
