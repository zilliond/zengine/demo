@extends('layouts.cabinet')

@section('body')
    <h1>@lang('cabinet/deposits.title')</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">@lang('cabinet/deposits.plan')</th>
            @if(setting('balances_type') === 'divided')
                <th scope="col">@lang('cabinet/deposits.wallet')</th>
            @endif
            <th scope="col">@lang('cabinet/deposits.amount')</th>
            <th scope="col">@lang('cabinet/deposits.accruals')</th>
            <th scope="col">@lang('cabinet/deposits.accrued')</th>
            <th scope="col">@lang('cabinet/deposits.status')</th>
            <th scope="col">@lang('cabinet/deposits.next_accrual')</th>
            <th scope="col">@lang('cabinet/deposits.last_accrual')</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
            @php
                $currencyService = app('currencies');
                $defaultCurrency = $currencyService->getDefaultCurrency(); //TODO: Use User Currency
            @endphp
            @foreach($deposits as $deposit)
                @php /* @var $deposit Modules\Core\Models\Deposit */ @endphp
                <tr>
                    <th scope="row">{{ $deposit->id }}</th>
                    <td>{{ $deposit->plan->name }}</td>
                    @if(setting('balances_type') === 'divided')
                        <td>{{ $deposit->wallet->payment_system->name }}</td>
                    @endif
                    <td>{{ $deposit->amount }} {{ $deposit->currency->symbol }}
                        @if($deposit->currency_id !== $defaultCurrency->id) ( {{ $deposit->getAmountInCurrency($defaultCurrency) }} {{ $defaultCurrency->symbol }} ) @endif
                    </td>
                    <td>{{ $deposit->passed_accruals }} / {{ $deposit->plan->duration }}</td>
                    <td>{{ $deposit->profit }} {{ $deposit->currency->symbol }}
                        @if($deposit->currency_id !== $defaultCurrency->id) ( {{ $deposit->getProfitInCurrency($defaultCurrency) }} {{ $defaultCurrency->symbol }} ) @endif
                    </td>
                    <td>
                        @if($deposit->status === app('zengine')->modelClass('Deposit')::STATUS_OPEN)
                            <span class="text-success">@lang('deposits.statuses.'.app('zengine')->modelClass('Deposit')::STATUS_OPEN)</span>
                        @elseif($deposit->status === app('zengine')->modelClass('Deposit')::STATUS_CLOSED)
                            <span class="text-danger">@lang('deposits.statuses.'.app('zengine')->modelClass('Deposit')::STATUS_CLOSED)</span>
                        @endif
                    </td>
                    <td>{{ optional($deposit->next_accrual_at)->toDateTimeString() }}</td>
                    <td>{{ optional($deposit->last_accrual_at)->toDateTimeString() }}</td>
                    <td>
                        @can('close', $deposit)
                        <a href="{!! route('cabinet.deposits.close', [$deposit->id]) !!}" class='btn btn-danger btn-sm'><i class="fas fa-times"></i></a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $deposits->links() }}
@endsection
