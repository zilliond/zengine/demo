<div class="list-group">
    <a href="{{ route('cabinet.index') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.index')
    </a>
    <a href="{{ route('cabinet.profile') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/profile*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.profile')
    </a>
    <a href="{{ route('cabinet.referrals') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/referrals*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.referrals')
    </a>
    <a href="{{ route('cabinet.security') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/security*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.security')
    </a>
    <a href="{{ route('cabinet.deposits.create') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/invest*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.invest')
    </a>
    <a href="{{ route('cabinet.deposits') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/deposits*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.deposits')
    </a>
    <a href="{{ route('cabinet.operations') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/operations*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.operations')
    </a>
    <a href="{{ route('cabinet.refill') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/refill*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.refill')
    </a>
    <a href="{{ route('cabinet.withdraw') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/withdraw*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.withdraw')
    </a>
    <a href="{{ route('cabinet.transfer') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/transfer*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.transfer')
    </a>
    <a href="{{ route('cabinet.tickets.index') }}" class="list-group-item list-group-item-action {{ Request::is('cabinet/tickets*') ? 'active' : '' }}">
        @lang('cabinet/ui.menu.support')
    </a>
</div>
