<?php

return [
    'user_operations' => [
        'created' => [
            app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => 'A deposit operation has been created for the amount :amount :currency through the payment system :Payment',
            app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => 'A withdrawal operation has been created for the amount :amount :currency through the payment system :Payment',
            app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => 'A transfer operation has been created for the amount :amount :currency to user :user',
            app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Создано депозит
            app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // Создано начисление
            app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => 'Deposit :deposit closed, earned :profit :currency',
        ],
        'status_updated' => [
            app('zengine')->modelClass('Operation')::STATUS_SUCCESS => [
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => 'Accrual received on the deposit :deposit, for the amount :amount :currency, left :left accruals',
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => 'Received payment on the referral system, in the amount :amount :currency, per user :user',
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => 'Replenishment received, amounting to :amount :currency through the payment system :payment', // пополнение успешно завершен
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод успешно завершен
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Успешно переведено
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Создано депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Депозит успешно закончен
            ],
            app('zengine')->modelClass('Operation')::STATUS_CANCELED => [ // Отменено пользователем или автоматически
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // начисление по депозиту
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => '', // Выплата по реф системе
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => '', // Пополнение
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Перевод
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Открыт депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Закрыт депозит
            ],
            app('zengine')->modelClass('Operation')::STATUS_REJECTED => [ // Отменено админом
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // начисление по депозиту
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => '', // Выплата по реф системе
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => '', // Пополнение
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Перевод
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Открыт депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Закрыт депозит
            ],
            app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS => [ // Операция в процессе
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // начисление по депозиту
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => '', // Выплата по реф системе
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => '', // Пополнение
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Перевод
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Открыт депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Закрыт депозит
            ],
        ]
    ],
    'user_deposits' => [
        'created' => 'A new deposit was created for the amount :amount :currency, :accruals accruals'
    ]
];
