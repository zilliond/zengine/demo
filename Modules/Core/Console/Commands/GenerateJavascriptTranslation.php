<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use MartinLindhe\VueInternationalizationGenerator\Generator;

class GenerateJavascriptTranslation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zengine:js:generate-translations {--all} {--admin} {--root}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate json translation for js from php files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function handle()
    {
        if ($this->option('all') || $this->option('root')) {
            return $this->generateRoot();
        }
        if ($this->option('all') || $this->option('admin')) {
            return $this->generateAdmin();
        }
        $this->generateRoot();
        $this->generateAdmin();
        $this->info('all js-translations compiled');
    }

    /**
     * @throws \Exception
     */
    public function generateRoot()
    {
        $config = config('js-translations.root');
        $this->generate($config);
    }

    /**
     * @throws \Exception
     */
    public function generateAdmin()
    {
        $config = config('js-translations.admin');
        $this->generate($config);
    }

    /**
     * @param $config
     *
     * @throws \Exception
     */
    public function generate($config)
    {
        $data = (new Generator($config))
            ->generateFromPath($config['langPath'], 'es6', false, false);
        $this->info($data);
        $jsFile = base_path().$config['jsFile'];
        file_put_contents($jsFile, $data);
        $this->info("Generated $jsFile");
    }
}
