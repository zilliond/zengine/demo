<?php

namespace Modules\Core\Services\Contracts;

use Modules\Core\Models\Currency;

interface CurrencyServiceInterface
{
    /**
     * @return Currency
     */
    public function getDefaultCurrency() : Currency;

    /**
     * @return Currency
     */
    public function getSystemCurrency() : Currency;

    /**
     * @return Currency[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCurrencies();

    /**
     * @param  float  $amount
     * @param  string  $code_from
     * @param  string  $code_to
     * @return mixed
     */
    public function convertByCode(float $amount, string $code_from, string $code_to);

    /**
     * @param  float  $amount
     * @param  Currency  $from
     * @param  Currency  $to
     * @return float|int
     */
    public function convert(float $amount, Currency $from, Currency $to);

    /**
     * @param  float  $amount
     * @param  Currency  $from
     * @return float|int
     */
    public function convertToDefault(float $amount, Currency $from);

    /**
     * @param  float  $amount
     * @param  Currency  $from
     * @return float|int
     */
    public function convertToSystem(float $amount, Currency $from);
}
