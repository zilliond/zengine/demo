<?php

return [
    'wallets'   => 'Кошельки',
    'wallet'    => 'Кошелек',
    'balance'   => 'Баланс',
    'stats'     => 'Статистика',
];
