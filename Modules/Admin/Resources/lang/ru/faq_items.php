<?php

return [
    'title' => 'FAQ',
    'table' => [
        'question'  => 'Вопрос',
        'answer'    => 'Ответ',
    ],
    'form' => [
        'question'  => 'Вопрос:',
        'answer'    => 'Ответ:',
        'hidden'    => 'Не отображать:',
        'submit'    => 'Сохранить',
    ]
];
