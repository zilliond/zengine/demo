<?php


namespace Modules\Core\Payments;

use Illuminate\Http\Request;
use Log;
use Modules\Core\Exceptions;
use Modules\Core\Exceptions\OperationException;
use Modules\Core\Models\Operation;
use Symfony\Component\DomCrawler\Crawler;

class PerfectMoneyProvider extends PaymentSystemProvider
{
    /* @inheritDoc */
    public $can_auto_withdraw = true;

    /* @inheritDoc */
    public $can_semi_auto_withdraw = true;

    public const SCI_ACTION = 'https://perfectmoney.is/api/step1.asp';

    /** @inheritDoc */
    public function getName() : string
    {
        return 'Perfect Money';
    }
    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws OperationException
     */
    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['sci_account', 'sci_name']);
        $form_data = [
            'PAYEE_ACCOUNT'         => $this->getCredential('sci_account'),
            'PAYEE_NAME'            => $this->getCredential('sci_name'),
            'PAYMENT_AMOUNT'        => number_format($operation->amount, 2, '.', ''),
            'PAYMENT_UNITS'         => $operation->currency->code_iso,
            'SUGGESTED_MEMO'        => '',
            'PAYMENT_ID'            => $operation->id,
            'STATUS_URL'            => route('payment_systems.ipn'),
            'PAYMENT_URL'           => route('cabinet.refill', ['status' => 'success']),
            'PAYMENT_URL_METHOD'    => 'GET',
            'NOPAYMENT_URL'         => route('cabinet.refill', ['status' => 'error']),
            'NOPAYMENT_URL_METHOD'  => 'GET',
            'BAGGAGE_FIELDS'        => 'itspm',
            'itspm'                 => 1,
        ];
        $operation->payment_system_data = [
            'action'    => self::SCI_ACTION,
            'method'    => 'POST',
            'type'      => 'form',
            'form_data' => $form_data
        ];
        $operation->save();

        return $operation;
    }

    /**
     * @param  Request  $request
     * @return bool
     */
    public function canHandle(Request $request) : bool
    {
        Log::channel('payment_systems')->info("PerfectMoneyProvider|canHandle", ['request' => $request->all()]);
        return $request->filled('PAYMENT_ID') && $request->filled('V2_HASH') && $request->filled('itspm');
    }

    /**
     * @param  Request  $request
     * @throws OperationException
     * @throws Exceptions\BalanceException
     */
    public function handle(Request $request)
    {
        $this->checkCredentials(['sci_key']);
        Log::channel('payment_systems')->info("PerfectMoneyProvider|Handler|start", ['request' => $request->all()]);
        $operation = app('zengine')->model('Operation')->find($request->get('PAYMENT_ID'));
        if (!$operation) {
            Log::channel('payment_systems')->info("PerfectMoneyProvider|Handler|Operation {$request->get('PAYMENT_ID')} not find - request", ['request' => $request->all()]);
            exit;
        }
        $hashData = [
            $operation->id,
            $request->get('PAYEE_ACCOUNT'),
            $request->get('PAYMENT_AMOUNT'),
            $request->get('PAYMENT_UNITS'),
            (int) $request->get('PAYMENT_BATCH_NUM'),
            $request->get('PAYER_ACCOUNT'),
            strtoupper(md5($this->getCredential('sci_key'))),
            $request->get('TIMESTAMPGMT')
        ];
        $hash = implode(':', $hashData);
        $hash = strtoupper(md5($hash));
        Log::channel('payment_systems')->info("PerfectMoneyProvider|handle|Operation {$operation->id} | Generated hash $hash");
        $operation->payment_system_data = $operation->payment_system_data + ['hash' => $hash];
        $operation->save();
        if ($request->get('V2_HASH') === $hash) {
            Log::channel('payment_systems')->info("PerfectMoneyProvider|Handler|Operation {$operation->id} success");
            $this->operationService->typeByOperation($operation)->success($operation);
            exit;
        }
        Log::channel('payment_systems')->info("PerfectMoneyProvider|Handler|Operation {$operation->id} error");
        exit;
    }

    /**
     * @return float
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance() : float
    {
        try {
            $this->checkCredentials(['api_acc', 'api_pass', 'sci_account']);
            $response = $this->guzzle->request('GET', 'https://perfectmoney.is/acct/balance.asp', [
                'query' => [
                    'AccountID'  => $this->getCredential('api_acc'),
                    'PassPhrase' => $this->getCredential('api_pass')
                ]
            ]);
            $crawler = new Crawler((string) $response->getBody());
            $a = $crawler->filter("input[name={$this->getCredential('sci_account')}]");
            return (float) $a->attr('value');
        } catch (\Exception $exception) {
            return 0.00;
        }
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws OperationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws Exceptions\BalanceException
     */
    public function makeWithdraw(Operation $operation) : Operation
    {
        $this->checkCredentials(['api_acc', 'api_pass', 'sci_account']);
        if (!isset($operation->payment_system_data['target'])) {
            throw new OperationException('PerfectMoneyProvider|makeWithdraw|Not set target account');
        }
        $response = $this->guzzle->request('POST', 'https://perfectmoney.is/acct/confirm.asp', [
            'form_params' => [
                'AccountID'     => $this->getCredential('api_acc'),
                'PassPhrase'    => $this->getCredential('api_pass'),
                'Payer_Account' => $this->getCredential('sci_account'),
                'Payee_Account' => $operation->payment_system_data['target'],
                'Amount'        => $operation->amount,
                'Memo'          => 'Withdraw',
                'PAYMENT_ID'    => $operation->id,
            ]
        ]);
        $body = (string) $response->getBody();
        if (!preg_match_all("|<input name='(.*)' type='hidden' value='(.*)'>|", $body, $matches, PREG_SET_ORDER)) {
            throw new OperationException("Operation {$operation->id}|Withdraw response not has a fields");
        }
        $params = [];
        foreach ($matches as $r) {
            $params[$r[1]] = $r[2];
        }
        if (isset($params['PAYMENT_BATCH_NUM']) && $params['PAYMENT_BATCH_NUM']) {
            $operation->payment_system_data += ['transaction_id' => $params['PAYMENT_BATCH_NUM']];
            $operation = $this->operationService->typeByOperation($operation)->success($operation);
        } elseif (isset($params['ERROR']) && $params['ERROR']) {
            $operation->payment_system_data += ['error' => $params['ERROR']];
            Log::channel('payment_systems')->info("PerfectMoneyProvider|makeWithdraw|Operation {$operation->id} error", ['body' => $body, 'params' => $params]);
        } else {
            $operation = $this->operationService->typeByOperation($operation)->inProgress($operation);
        }
        $operation->save();
        return $operation;
    }

    public function getCredentialsKeys() : array
    {
        return [
            'sci_account',
            'sci_name',
            'sci_key',
            'api_acc',
            'api_pass',
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'sci_account'   => 'Кошелек',
            'sci_name'      => 'Название магазина',
            'sci_key'       => 'Приватный ключ маназина',
            'api_acc'       => 'Аккаунт',
            'api_pass'      => 'Пароль',
        ];
    }
}
