<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Deposit;

class DepositsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $depositService = app('deposits');
        $deposit = factory(app('zengine')->modelClass('Deposit'))->create(['created_at' => now()]);
        $depositService->create($deposit, true);
        $deposits = factory(app('zengine')->modelClass('Deposit'), 200)->create();
        $deposits->each(function (Deposit $deposit) use ($depositService) {
            $depositService->create($deposit, true);
        });
        $this->command->info($deposits->count().' Deposits created.');
    }
}
