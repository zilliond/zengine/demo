<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Api\CreatePlanRequest;
use Modules\Admin\Http\Requests\Api\UpdatePlanRequest;
use Modules\Core\Models\Plan;

class PlanController extends Controller
{
    /**
     * Display a listing of the Plan.
     *
     * @param  Request  $request
     *
     * @return \Illuminate\Database\Eloquent\Collection|Plan[]
     */
    public function index(Request $request)
    {
        return app('zengine')->model('Plan')->with('currency')->get();
    }

    public function show($id)
    {
        return app('zengine')->model('Plan')->find($id);
    }

    /**
     * Store a newly created Plan in storage.
     *
     * @param  \Modules\Admin\Http\Requests\Api\CreatePlanRequest  $request
     *
     * @return array
     */
    public function store(CreatePlanRequest $request)
    {
        $input = $request->all();

        $plan = app('zengine')->model('Plan')->create($input);

        return [
            'status'  => 'success',
            'message' => 'Plan saved successfully.',
            'plan'    => $plan,
        ];
    }

    /**
     * Update the specified Plan in storage.
     *
     * @param $id
     * @param  UpdatePlanRequest  $request
     *
     * @return array
     */
    public function update($id, UpdatePlanRequest $request)
    {
        $plan = app('zengine')->model('Plan')->find($id);
        $plan->fill($request->all());
        $plan->save();

        return [
            'status'  => 'success',
            'message' => 'Plan updated successfully.',
            'plan'    => $plan,
        ];
    }

    /**
     * Remove the specified Plan from storage.
     *
     * @param  int  $id
     *
     * @return array
     * @throws \Exception
     */
    public function destroy($id)
    {
        $plan = app('zengine')->model('Plan')->findOrFail($id);

        if(empty($plan)) {
            return [
                'status'  => 'error',
                'message' => 'Plan not found',
            ];
        }

        return [
            'status'  => 'success',
            'message' => 'Plan deleted successfully.',
            'plan'    => $plan->delete(),
        ];
    }

    public function toggleVisible($id)
    {
        $plan = app('zengine')->model('Plan')->with('currency')->findOrFail($id);
        /** @var Plan $plan */
        $plan->is_hidden = !$plan->is_hidden;
        return [
            'status'  => $plan->save() ? 'success' : 'error',
            'message' => $plan->is_hidden ? 'План скрыт' : 'План теперь отображается',
            'plan'    => $plan,
        ];

    }
}
