<?php

use Modules\Core\Http\Controllers\ZEngineController;

Route::prefix('engine')->group(function () {
    Route::get('', [ZEngineController::class, 'engine']);
    Route::get('version', [ZEngineController::class, 'version']);
    Route::get('licence', [ZEngineController::class, 'licence']);
});


Route::middleware('can:is-admin')->group(function () {
    Route::post('currencies/update_rates', [ZEngineController::class, 'update_all']);
    Route::post('currencies/{currency}/update_rate', [ZEngineController::class, 'update']);
});
Route::get('currencies', [ZEngineController::class, 'currencies']);
