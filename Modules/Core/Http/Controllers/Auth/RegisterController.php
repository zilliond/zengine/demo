<?php

namespace Modules\Core\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Core\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        $inviter = app('zengine')->model('User')->find(Cookie::get('inviter'));
        return view('auth.register', [
            'inviter' => $inviter,
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \Modules\Core\Models\User
     */
    protected function create(array $data)
    {
        $inviter = app('zengine')->model('User')->find(Cookie::get('inviter', null));
        $user_data = [
            'login'         => $data['login'],
            'name'          => $data['name'],
            'email'         => $data['email'],
            'password'      => Hash::make($data['password']),
            'inviter_id'    => optional($inviter)->id,
            'ref_level'     => $inviter ? $inviter->ref_level + 1 : 0,
            'last_login_at' => now()->toDateTimeString(),
        ];
        $user_data[config('zengine.auth.username')] = $data[config('zengine.auth.username')];
        $custom_fields = config('zengine.auth.custom_register_fields');
        if(is_array($custom_fields) && count($custom_fields)) {
            $user_data = array_merge($user_data, collect($data)->only($custom_fields)->toArray());
        }
        $user = app('zengine')->model('User')->create($user_data);
        if(isset($data['provider'], $data['provider_user_id']) && $data['provider'] && $data['provider_user_id']) {
            //Create social account
            $user->social_accounts()->create([
                'provider_user_id' => $data['provider_user_id'],
                'provider'         => $data['provider'],
            ]);
        }
        if((bool) setting('general.log_ip', true)) {
            $location = \GeoIP::getLocation(request()->getClientIp());
            $user->last_login_ip = request()->getClientIp();
            $user->country_code = $user->last_login_country_code;
            $user->auth_logs()->create([
                'ip'           => $user->last_login_ip,
                'country_code' => $user->country_code,
            ]);
            if($currency = app('zengine')->model('Currency')->where('code_iso', $location['currency'])->first()) {
                $user->currency()->associate($currency);
            }
        }elseif($currency = app('zengine')->model('Currency')->find(setting('currencies.default'))) {
            $user->currency()->associate($currency);
        }
        $userService = app('users');
        $userService->onCreate($user);
        $user->save();
        return $user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'login'    => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
        if(config('zengine.auth.username') === 'email') {
            $rules['email'] = ['required', 'string', 'email', 'max:255', 'unique:users'];
        }else {
            $rules[config('zengine.auth.username')] = ['required', 'string', 'max:255', 'unique:users'];
        }
        $custom_rules = config('zengine.auth.custom_register_fields_validation');
        if(is_array($custom_rules) && count($custom_rules)) {
            $rules = array_merge($rules, $custom_rules);
        }
        return Validator::make($data, $rules);
    }
}
