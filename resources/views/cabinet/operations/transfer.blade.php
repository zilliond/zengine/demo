@extends('layouts.cabinet')

@section('body')
    @include('flash::message')

    <div class="clearfix"></div>
    <h1>@lang('cabinet/transfer.title')</h1>
    <form action="{{ route('cabinet.transfer.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="login_field">@lang('cabinet/transfer.receiver_login')</label>
            <input type="text" class="form-control" name="login" id="login_field">
        </div>
        <div class="form-group">
            <label for="amount_field">@lang('cabinet/transfer.amount')</label>
            <input type="number" class="form-control" name="amount" id="amount_field" step="0.00001">
        </div>
        <div class="form-group">
            <label for="currency_id_field">@lang('cabinet/transfer.currency')</label>
            <select name="currency_id" id="currency_id_field" class="form-control">
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}" @if($currency->id === (int) old('currency_id'))selected @endif>{{ $currency->code_iso }}</option>
                @endforeach
            </select>
        </div>
        @if(setting('balances_type') === 'divided')
            <div class="form-group">
                <label for="wallet_id_field">@lang('cabinet/transfer.wallet')</label>
                <select name="wallet_id" id="wallet_id_field" class="form-control">
                    @foreach($wallets as $wallet)
                        <option value="{{ $wallet->id }}" @if($wallet->id === (int) old('wallet_id'))selected @endif>{{ $wallet->payment_system->name }} {{ $wallet->balance }}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <button class="btn btn-primary" type="submit">@lang('cabinet/transfer.submit')</button>
    </form>
@endsection
